class GeraltDopplerResources
{
	public var isLoaded : bool; default isLoaded = false;

	public var appearComp : CAppearanceComponent;
	public var Head : GeraltDopplerHead;
	public var Hair : GeraltDopplerHair;
	public var Body : GeraltDopplerBody;
	public var SandBox : GeraltDopplerSandBox;
	public var isSandBoxActive : bool; default isSandBoxActive = false;

	public var currentHair : int; default currentHair = 0;
	public var currentHead : int; default currentHead = 0;
	public var currentBody : int; default currentBody = 0;

	public var CFG_CURRENT : name;
	public var CFG_CURRENT_HEAD : name;
	public var CFG_CURRENT_HAIR : name;
	public var CFG_CURRENT_BODY : name;

	default CFG_CURRENT = 'Hidden';
	default CFG_CURRENT_HEAD = 'GDCurrentHead';
	default CFG_CURRENT_HAIR = 'GDCurrentHair';
	default CFG_CURRENT_BODY = 'GDCurrentBody';

    public function Init()
    {
    	Head = new GeraltDopplerHead in this;
    	Head.Init();

    	Hair = new GeraltDopplerHair in this;
    	Hair.Init();

    	Body = new GeraltDopplerBody in this;
    	Body.Init();

    	SandBox = new GeraltDopplerSandBox in this;
    	SandBox.Init();

    	isLoaded = true;
    }

    public function SetSandBox(active: bool)
    {
    	if ( active ) {
    		UnloadCurrentBody();
			SandBox.LoadAll();
    	}
    	else {
    		SandBox.UnloadAll();
			SetCurrentBody();
    	}

    	GetGeraltDoppler().configWrapper.SetVarValue(CFG_CURRENT, 'GDIsSandBoxActive', active);

    	isSandBoxActive = active;
    }

    public function IsValid() : bool
    {
        return Head.count > 0 && Hair.count > 0 && Body.count > 0 && SandBox.count > 0 && SandBox.categoryCount > 0;
    }

	public function LoadFromConfig()
	{
		var cfgCurrentHead, cfgCurrentHair, cfgCurrentBody : int;
		
		cfgCurrentHead = StringToInt(GetGeraltDoppler().configWrapper.GetVarValue(CFG_CURRENT, CFG_CURRENT_HEAD));
		if ( Head.IsValid(cfgCurrentHead) ) currentHead = cfgCurrentHead;

		cfgCurrentHair = StringToInt(GetGeraltDoppler().configWrapper.GetVarValue(CFG_CURRENT, CFG_CURRENT_HAIR));
		if ( Hair.IsValid(cfgCurrentHair) ) currentHair = cfgCurrentHair;

		cfgCurrentBody = StringToInt(GetGeraltDoppler().configWrapper.GetVarValue(CFG_CURRENT, CFG_CURRENT_BODY));
		if ( Body.IsValid(cfgCurrentBody) ) currentBody = cfgCurrentBody;

		isSandBoxActive = GetGeraltDoppler().configWrapper.GetVarValue(CFG_CURRENT, 'GDIsSandBoxActive');
	}

	public function SaveToConfig(configName : name, value : int)
	{
		GetGeraltDoppler().configWrapper.SetVarValue(CFG_CURRENT, configName, IntToString(value));
	}

	public function GetCurrentDescription(showHead : bool) : string
	{
		var text : string;

		text  = "<font size='15'>";
		if ( showHead ) {
			text += "Head: " + Head.names[ currentHead ] + "<br />";
		}
		text += "Hair: " + Hair.names[ currentHair ] + "<br />";
		text += "Body: " + Body.names[ currentBody ] + "<br /></font>";

		return text;
	}

	public function SetCurrent()
	{
		SetCurrentHead();
		SetCurrentHair();
		SetCurrentBody();
	}

    // HEADS ------------------------
    public function SetCurrentHead() {
		SetHead(false, false);
	}

	public function SetNextHead() {
		SetHead(true, false);
	}

	public function SetPreviousHead() {
		SetHead(false, true);
	}

	private function SetHead(next: bool, previous : bool)
	{
		if ( next ) {
			currentHead = currentHead + 1;
			if ( currentHead >= Head.count ) {
				currentHead = 0;
			}
		}

		if ( previous ) {
			if ( currentHead == 0 ) {
				currentHead = Head.count - 1;
			}
			else {
				currentHead = currentHead - 1;
			}
		}

		if ( currentHead >= 0 && currentHead < Head.count ) {
			SetCustomHead(currentHead);
			SaveToConfig(CFG_CURRENT_HEAD, currentHead);
		}
	}


	// HAIRS ------------------------
	public function UnloadCurrentHair() {
		UnloadResources(Hair.list[currentHair]);
	}

	public function SetCurrentHair() {
		SetHair(false, false);
	}

	public function SetNextHair() {
		SetHair(true, false);
	}

	public function SetPreviousHair() {
		SetHair(false, true);
	}

	public function SetHair(next: bool, previous : bool)
	{
		UnloadCurrentHair();

		if ( next ) {
			currentHair = currentHair + 1;

			if ( currentHair >= Hair.count ) {
				currentHair = 0;
			}
		}

		if ( previous ) {
			if ( currentHair == 0 ) {
				currentHair = Hair.count - 1;
			}
			else {
				currentHair = currentHair - 1;
			}
		}

		if ( currentHair >= 0 && currentHair < Hair.count ) {
			LoadResources(Hair.list[currentHair]);
			SaveToConfig(CFG_CURRENT_HAIR, currentHair);
		}
	}


	// BODIES ------------------------
	public function UnloadCurrentBody()	{
		UnloadResources(Body.list[currentBody]);
	}

	public function SetCurrentBody() {
		SetBody(false, false);
	}

	public function SetNextBody() {
		SetBody(true, false);
	}

	public function SetPreviousBody() {
		SetBody(false, true);
	}

	public function SetBody(next: bool, previous : bool)
	{
		UnloadCurrentBody();

		if ( next ) {
			currentBody = currentBody + 1;

			if ( currentBody >= Body.count ) {
				currentBody = 0;
			}
		}

		if ( previous ) {
			if ( currentBody == 0 ) {
				currentBody = Body.count - 1;
			}
			else {
				currentBody = currentBody - 1;
			}
		}

		if ( currentBody >= 0 && currentBody < Body.count ) {
			LoadResources(Body.list[currentBody]);
			SaveToConfig(CFG_CURRENT_BODY, currentBody);
		}
	}


	public function UnloadResources(templateList : array<CEntityTemplate>) {
		SetResource(templateList, false);
	}

	public function LoadResources(templateList : array<CEntityTemplate>) {
		SetResource(templateList, true);
	}

	public function SetResource(templateList : array<CEntityTemplate>, load : bool)
	{
		var i, listSize : int;

		appearComp = (CAppearanceComponent)thePlayer.GetComponentByClassName('CAppearanceComponent');
		listSize = templateList.Size();

		for (i = 0; i < listSize; i += 1)
		{
			if (load) {
				appearComp.IncludeAppearanceTemplate(templateList[i]);
			}
			else {
				appearComp.ExcludeAppearanceTemplate(templateList[i]);
			}
		}
	}

	private function SetCustomHead(index : int)
	{
		var cHead : CHeadManagerComponent;
		cHead = (CHeadManagerComponent)thePlayer.GetComponentByClassName('CHeadManagerComponent');
		cHead.SetCustomHead(Head.list[index]);
	}

	public function ApplyWomanModelFix()
	{
		var entities : array<CEntityTemplate>;
		entities.PushBack((CEntityTemplate)LoadResource("characters\player_entities\ciri\ciri_phantom_fx.w2ent", true));
		UnloadResources(entities);
		LoadResources(entities);
	}
}