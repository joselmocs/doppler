class GeraltDoppler
{
    public var isInitialized : bool;
    public var isHalted : bool;
    public var isLocked : bool;
    public var isWoman : bool;
    public var isSandBox : bool;

    var configWrapper : CInGameConfigWrapper;
    var resources : GeraltDopplerResources;
    var menu : GeraltDopplerMenu;
    var favorites : GeraltDopplerFavorites;

    default isInitialized = false;
    default isHalted = false;
    default isLocked = false;
    default isWoman = true;
    default isSandBox = false;

    public function Init()
    {
        configWrapper = theGame.GetInGameConfigWrapper();

        resources = GetGeraltDopplerResources();

        if ( !resources.IsValid() ) {
            Halt("invalid resources");
            return;
        }

        if (isWoman) {
            resources.ApplyWomanModelFix();
        }
        
        resources.LoadFromConfig();
        resources.SandBox.LoadFromConfig();

        resources.SetCurrentHead();
        resources.SetCurrentHair();

        if ( resources.isSandBoxActive ) {
            resources.SandBox.LoadAll();
        }
        else {
            resources.SetCurrentBody();
        }

        favorites = new GeraltDopplerFavorites in this;
        favorites.Init();

        menu = new GeraltDopplerMenu in this;
        menu.SetRoot();
        menu.Init();

        RegisterListeners();
        
        isInitialized = true;
    }

    public function Halt( reason : string )
    {
        isHalted = true;
        GDNotify(reason, 10000);
    }

    function RegisterListeners()
    {
        theInput.RegisterListener(this, 'OnUp', 'GeraltDopplerOnUp');
        theInput.RegisterListener(this, 'OnDown', 'GeraltDopplerOnDown');
        theInput.RegisterListener(this, 'OnLeft', 'GeraltDopplerOnLeft');
        theInput.RegisterListener(this, 'OnRight', 'GeraltDopplerOnRight');
    }

    public final function OnUp(action : SInputAction)
    {
        if ( !IsPressed(action) ) return;
        if ( !isInitialized || isLocked ) return;

        isLocked = true;
        menu.OnUp();
        isLocked = false;
    }

    public final function OnDown(action : SInputAction)
    {
        if ( !IsPressed(action) ) return;
        if ( !isInitialized || isLocked ) return;

        isLocked = true;
        menu.OnDown();
        isLocked = false;
    }

    public final function OnLeft(action : SInputAction)
    {
        if ( !IsPressed(action) ) return;
        if ( !isInitialized || isLocked ) return;

        isLocked = true;
        menu.OnLeft();
        isLocked = false;
    }

    public final function OnRight(action : SInputAction)
    {
        if ( !IsPressed(action) ) return;
        if ( !isInitialized || isLocked ) return;

        isLocked = true;
        menu.OnRight();
        isLocked = false;
    }
}


function GetGeraltDoppler(): GeraltDoppler
{
    return GetWitcherPlayer().geraltDoppler;
}

function GDNotify(message : string, duration : float)
{
    theGame.GetGuiManager().ShowNotification(message, duration);
}

function GetGeraltDopplerResources(): GeraltDopplerResources
{
    return theGame.geraltDopplerResources;
}