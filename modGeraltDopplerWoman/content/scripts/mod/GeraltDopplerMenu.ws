class GeraltDopplerMenu extends NotifyMenuSubMenus
{
	public var doppler : GeraltDoppler;

	public function Init()
	{
		super.Init();
		SetTitle("GeraltDoppler");
	}

	public function PreRefreshContent()
	{
		doppler = GetGeraltDoppler();
		ClearOptions();

		AddSubMenu(new GeraltDopplerPresetsMenu in this);
		AddSubMenu(new GeraltDopplerSandBoxMenu in this);
	}

	public function RefreshContent()
	{
		var i, optionsSize : int;

		Close();
		ClearContent();
		PreRefreshContent();

		optionsSize = options.Size();

		for ( i = 0; i < optionsSize; i += 1)
		{
			if ( GetHighlighted() == i ) {
				AddContent( GetConfig().highlightFont + "# " + options[ i ] + "</font>");
			}
			else {
				AddContent( options[ i ] );
			}

			AddContent( menus[i].GetDescription() );

			if ( i < optionsSize - 1 ) {
				AddContent( "<br />" );
			}
		}

		Open();
	}
}

class GeraltDopplerPresetsMenu extends NotifyMenuSubMenus
{
	public var doppler : GeraltDoppler;

	public function Init()
	{
		super.Init();
		SetTitle("Presets");
	}

	public function GetDescription() : string
	{
		doppler = GetGeraltDoppler();
		if ( !doppler.resources.isSandBoxActive ) {
			return " [active]";
		}

		return "";
	}

	public function RefreshContent()
	{
		var i, optionsSize : int;
		var newContent : string;

		Close();
		ClearContent();

		if ( !doppler.resources.isSandBoxActive ) {
			
			PreRefreshContent();

			optionsSize = options.Size();
			for ( i = 0; i < optionsSize; i += 1 )
			{
				if ( GetHighlighted() == i ) {
					AddContent( GetConfig().highlightFont + "# " + StrUpper(options[ i ]) + "</font>");
				}
				else {
					AddContent( StrUpper(options[ i ]) );
				}

				if ( menus[ i ].GetSubTitle() != "") {
					AddContent( GetConfig().alternativeFont + ": " + menus[ i ].GetSubTitle() + "</font>" );
				}
				else {
					AddContent( "<br />" );
				}

				if ( i < optionsSize - 1 ) {
					AddContent("<br />");
				}
			}
		}
		else {
			newContent  = GetConfig().highlightFont + "ATTENTION:</font><br />";
			newContent += "Entering Presets will reset your current<br />SandBox settings.<br />";

			AddContent( newContent );
		}
		Open();
	}

	public function PreRefreshContent()
	{
		doppler = GetGeraltDoppler();
		ClearOptions();

		AddSubMenu(new GeraltDopplerFavoritesMenu in this);
		AddSubMenu(new GeraltDopplerHairMenu in this);
		AddSubMenu(new GeraltDopplerHeadMenu in this);
		AddSubMenu(new GeraltDopplerBodyMenu in this);

		super.PreRefreshContent();
	}

	public function GetActionsLabel() : string
	{
		var actionsLabel : string;

		if ( !doppler.resources.isSandBoxActive ) {
			return super.GetActionsLabel();
		}
		else {
			actionsLabel = "";
			actionsLabel += GetConfig().highlightFont + "RIGHT</font> to enable ";
			actionsLabel += GetConfig().highlightFont + "Presets</font><br />";
			actionsLabel += GetConfig().highlightFont + "LEFT</font> to back<br />";

			return actionsLabel;
		}
	}

	public function OnRight()
	{
		if ( doppler.resources.isSandBoxActive ) {
			doppler.resources.SetSandBox(false);
			RefreshContent();
		}
		else {
			super.OnRight();
		}
	}
}

class GeraltDopplerHeadMenu extends NotifyMenuOptions
{
	public function Init()
	{
		super.Init();
		SetTitle("Head");
	}

	public function GetSubTitle() : string
	{
		var doppler : GeraltDoppler; doppler = GetGeraltDoppler();
		return doppler.resources.Head.names[doppler.resources.currentHead];
	}

	public function PreRefreshContent()
	{
		var i, index : int;
		var doppler : GeraltDoppler;

		doppler = GetGeraltDoppler();
		
		ClearOptions();

		for ( i = 5; i >= 1; i -= 1) {
			index = doppler.resources.currentHead - i;

			if ( index >= 0 && index <= doppler.resources.Head.count - 1 ) {
				AddOption( doppler.resources.Head.names[ index ] );
			}
		}

		SetHighlightedIndex(GetOptionsSize());
		AddOption( doppler.resources.Head.names[ doppler.resources.currentHead ] );

		for ( i = 1; i <= 5; i += 1) {
			index = doppler.resources.currentHead + i;

			if ( index >= 0 && index <= doppler.resources.Head.count - 1 ) {
				AddOption( doppler.resources.Head.names[ index ] );
			}
		}
	}

	public function OnUp()
	{
		GetGeraltDopplerResources().SetPreviousHead();
		RefreshContent();
	}

	public function OnDown()
	{
		GetGeraltDopplerResources().SetNextHead();
		RefreshContent();
	}
}

class GeraltDopplerHairMenu extends NotifyMenuOptions
{
	public function Init()
	{
		super.Init();
		SetTitle("Hair");
	}

	public function GetSubTitle() : string
	{
		var doppler : GeraltDoppler; doppler = GetGeraltDoppler();
		return doppler.resources.Hair.names[doppler.resources.currentHair];
	}

	public function PreRefreshContent()
	{
		var i, index : int;
		var doppler : GeraltDoppler;

		doppler = GetGeraltDoppler();
		
		ClearOptions();

		for ( i = 5; i >= 1; i -= 1) {
			index = doppler.resources.currentHair - i;

			if ( index >= 0 && index <= doppler.resources.Hair.count - 1 ) {
				AddOption( doppler.resources.Hair.names[ index ] );
			}
		}

		SetHighlightedIndex( GetOptionsSize() );
		AddOption( doppler.resources.Hair.names[ doppler.resources.currentHair ] );

		for ( i = 1; i <= 5; i += 1) {
			index = doppler.resources.currentHair + i;

			if ( index >= 0 && index <= doppler.resources.Hair.count - 1 ) {
				AddOption( doppler.resources.Hair.names[ index ] );
			}
		}
	}

	public function OnUp()
	{
		GetGeraltDopplerResources().SetPreviousHair();
		RefreshContent();
	}

	public function OnDown()
	{
		GetGeraltDopplerResources().SetNextHair();
		RefreshContent();
	}
}

class GeraltDopplerBodyMenu extends NotifyMenuOptions
{
	var doppler : GeraltDoppler;

	public function Init()
	{
		doppler = GetGeraltDoppler();

		super.Init();
		SetTitle("Body");
	}

	public function GetSubTitle() : string
	{
		var doppler : GeraltDoppler; doppler = GetGeraltDoppler();
		return doppler.resources.Body.names[doppler.resources.currentBody];
	}

	public function PreRefreshContent()
	{
		var i, index : int;
		var doppler : GeraltDoppler;

		doppler = GetGeraltDoppler();
		
		ClearOptions();

		for ( i = 5; i >= 1; i -= 1) {
			index = doppler.resources.currentBody - i;

			if ( index >= 0 && index <= doppler.resources.Body.count - 1 ) {
				AddOption( doppler.resources.Body.names[ index ] );
			}
		}

		SetHighlightedIndex( GetOptionsSize() );
		AddOption( doppler.resources.Body.names[ doppler.resources.currentBody ] );

		for ( i = 1; i <= 5; i += 1) {
			index = doppler.resources.currentBody + i;

			if ( index >= 0 && index <= doppler.resources.Body.count - 1 ) {
				AddOption( doppler.resources.Body.names[ index ] );
			}
		}
	}

	public function OnUp()
	{
		GetGeraltDopplerResources().SetPreviousBody();
		RefreshContent();
	}

	public function OnDown()
	{
		GetGeraltDopplerResources().SetNextBody();
		RefreshContent();
	}
}

class GeraltDopplerFavoritesMenu extends NotifyMenuSubMenus
{
	var doppler : GeraltDoppler;
	var favorites : GeraltDopplerFavorites;

	public function Init()
	{
		super.Init();
		SetTitle("Favorites");
	}

	public function PreRefreshContent()
	{
		var i, size : int;
		var text : string;
		var slot : GeraltDopplerFavoriteSlotMenu;

		doppler = GetGeraltDoppler();
		favorites = doppler.favorites;

		ClearOptions();

		size = favorites.slots.Size();

		for ( i = 0; i < size; i += 1 )
		{
			text = favorites.GetSlotDescription(i, false);

			slot = new GeraltDopplerFavoriteSlotMenu in this;
			slot.SetTitle( "SLOT " + IntToString( i + 1 ));
			slot.SetSubTitle( text );

			AddSubMenu( slot );
		}
	}

	public function RefreshContent()
	{
		var i, size : int;
		var newContent : string;

		Close();
		ClearContent();
		PreRefreshContent();

		size = options.Size();

		for ( i = 0; i < size; i += 1)
		{
			if ( GetHighlighted() == i ) {
				AddContent( GetConfig().highlightFont + "# " + options[i] );
			}
			else {
				AddContent( options[ i ] );
			}

			AddContent( ": " + favorites.GetSlotHeadName(i) + "<br />");
			AddContent( "    " + favorites.GetSlotHairName(i) + "<br />");
			AddContent( "    " + favorites.GetSlotBodyName(i));

			if ( GetHighlighted() == i ) {
				AddContent( "</font>" );
			}

			if ( i < size - 1 ) {
				AddContent( "<br />" );
			}
		}

		Open();
	}

	public function GetActionsLabel() : string
	{
		var actionsLabel : string;

		actionsLabel = "";
		actionsLabel += GetConfig().highlightFont + "RIGHT</font> to enter ";
		actionsLabel += GetConfig().highlightFont + "Slot "+ IntToString(GetHighlighted() + 1) +"</font><br />";
		actionsLabel += GetConfig().highlightFont + "LEFT</font> to back<br />";

		return actionsLabel;
	}
}

class GeraltDopplerFavoriteSlotMenu extends NotifyMenu
{
	var doppler : GeraltDoppler;
	var parentM : GeraltDopplerFavoritesMenu;
	var favorites : GeraltDopplerFavorites;

	public function RefreshContent()
	{
		var newContent : string;

		parentM = (GeraltDopplerFavoritesMenu)parentMenu;
		doppler = GetGeraltDoppler();
		favorites = doppler.favorites;

		Close();
		ClearContent();

		newContent  = GetConfig().highlightFont + "Currently Favorited</font><br />";
		newContent += favorites.GetSlotDescription(parentM.openedMenuIndex, true) + "<br />";
		newContent += GetConfig().highlightFont + "Currently in Use</font><br />";
		newContent += doppler.resources.GetCurrentDescription(true);

		AddContent( newContent );

		Open();
	}

	public function Open()
	{
		var notificationText : string;

		notificationText  = GetConfig().globalFont;
		notificationText += GetFullTitle() + "<br /><br />";
		notificationText += content;
		notificationText += "<br />" + GetConfig().legendSeparatorText + "<br />";
		notificationText += GetConfig().highlightFont + "RIGHT</font> to ";
		notificationText += GetConfig().highlightFont + "LOAD</font> favorited<br />";
		notificationText += GetConfig().highlightFont + "DOWN</font> to ";
		notificationText += GetConfig().highlightFont + "SAVE</font> currently in use<br />";
		notificationText += GetActionsLabel();
		notificationText += "</font>";

		theGame.GetGuiManager().ShowNotification( notificationText, GetConfig().oppenedMenuDuration );
		isActive = true;
	}

	public function OnRight()
	{
		var slot : array<int>;
		var oppenedIndex : int;
		
		if ( !favorites ) {
			return;
		}

		oppenedIndex = parentM.openedMenuIndex;

		slot = favorites.GetSlot( oppenedIndex );

		doppler.resources.UnloadCurrentHair();
		doppler.resources.UnloadCurrentBody();

		if ( doppler.resources.Head.IsValid(slot[0]) ) {
			doppler.resources.currentHead = slot[0];
		}
		if ( doppler.resources.Hair.IsValid(slot[1]) ) {
			doppler.resources.currentHair = slot[1];
		}
		if ( doppler.resources.Body.IsValid(slot[2]) ) {
			doppler.resources.currentBody = slot[2];
		}

		doppler.resources.SetCurrentHead();
		doppler.resources.SetCurrentHair();
		doppler.resources.SetCurrentBody();

		CloseAll();
	}

	public function OnDown()
	{
		var slot : array<int>;
		var oppenedIndex : int;

		oppenedIndex = parentM.openedMenuIndex;

		slot.PushBack( doppler.resources.currentHead );
		slot.PushBack( doppler.resources.currentHair );
		slot.PushBack( doppler.resources.currentBody );

		favorites.SaveToConfig( oppenedIndex, slot );

		CloseAll();
		theGame.GetGuiManager().ShowNotification( "SLOT " + IntToString( oppenedIndex + 1 ) + " saved", 3000 );
	}
}


class GeraltDopplerSandBoxMenu extends NotifyMenuSubMenus
{
	public var doppler : GeraltDoppler;

	public function Init()
	{
		super.Init();
		SetTitle("SandBox");
	}

	public function GetDescription() : string
	{
		doppler = GetGeraltDoppler();
		if ( doppler.resources.isSandBoxActive ) {
			return " [active]";
		}

		return "";
	}

	public function RefreshContent()
	{
		var i, optionsSize : int;
		var newContent : string;

		Close();
		ClearContent();

		if ( doppler.resources.isSandBoxActive ) {
			
			PreRefreshContent();

			optionsSize = options.Size();
			for ( i = 0; i < optionsSize; i += 1 )
			{
				if ( GetHighlighted() == i ) {
					AddContent( GetConfig().highlightFont + "# " + StrUpper(options[ i ]) + "</font>");
				}
				else {
					AddContent( StrUpper(options[ i ]) );
				}

				if ( menus[ i ].GetSubTitle() != "") {
					AddContent( GetConfig().alternativeFont + ": " + menus[ i ].GetSubTitle() + "</font>" );
				}
				else {
					AddContent( "<br />" );
				}

				if ( i < optionsSize - 1 ) {
					AddContent("<br />");
				}
			}
		}
		else {
			newContent  = GetConfig().highlightFont + "ATTENTION:</font><br />";
			newContent += "Entering SandBox will reset your current<br />Presets settings.<br />";

			AddContent( newContent );
		}

		Open();
	}

	public function PreRefreshContent()
	{
		doppler = GetGeraltDoppler();
		ClearOptions();

		AddSubMenu(new GeraltDopplerSandBoxFavoritesMenu in this);
		AddSubMenu(new GeraltDopplerHairMenu in this);
		AddSubMenu(new GeraltDopplerHeadMenu in this);
		AddSubMenu(new GeraltDopplerSandBoxCategoriesMenu in this);

		super.PreRefreshContent();
	}

	public function GetActionsLabel() : string
	{
		var actionsLabel : string;

		if ( doppler.resources.isSandBoxActive ) {
			return super.GetActionsLabel();
		}
		else {
			actionsLabel = "";
			actionsLabel += GetConfig().highlightFont + "RIGHT</font> to enable ";
			actionsLabel += GetConfig().highlightFont + "SandBox</font><br />";
			actionsLabel += GetConfig().highlightFont + "LEFT</font> to back<br />";

			return actionsLabel;
		}
	}

	public function OnRight()
	{
		if ( !doppler.resources.isSandBoxActive ) {
			doppler.resources.SetSandBox(true);
			RefreshContent();
		}
		else {
			super.OnRight();
		}
	}
}


class GeraltDopplerSandBoxCategoriesMenu extends NotifyMenuSubMenus
{
	public var doppler : GeraltDoppler;

	public function Init()
	{
		var i : int;
		var sandBoxCategory : GeraltDopplerSandBoxCategoryMenu;

		doppler = GetGeraltDoppler();

		super.Init();
		SetTitle("Body");

		for (i = 0; i < GetGeraltDopplerResources().SandBox.categoryCount; i += 1) {
			sandBoxCategory = new GeraltDopplerSandBoxCategoryMenu in this;
			sandBoxCategory.SetCategory(i);
			AddSubMenu(sandBoxCategory);
		}
	}

	public function GetSubTitle() : string
	{
		var i, ix, current : int;
		var text : string;
		var sandBox : GeraltDopplerSandBox;

		sandBox = GetGeraltDopplerResources().SandBox;
		text = "<br />";
		text += sandBox.GetCurrentBodyDescription();

		return text;
	}
}


class GeraltDopplerSandBoxCategoryMenu extends NotifyMenuSubMenus
{
	public var indexCategory: int;
	public var resources : GeraltDopplerResources;

	public function Init()
	{
		var i, totalSlots : int;
		var slotMenu : GeraltDopplerSandBoxSlotMenu;

		super.Init();
		SetTitle("Category");

		resources = GetGeraltDopplerResources();

		for (i = 0; i < resources.SandBox.categories[indexCategory].slotsCount; i += 1) {
			slotMenu = new GeraltDopplerSandBoxSlotMenu in this;
			slotMenu.SetCurrent(indexCategory, i);
			AddSubMenu(slotMenu);
		}
	}

	public function SetCategory(index: int)
	{
		indexCategory = index;
	}

	public function GetTitle(): string
	{
		resources = GetGeraltDopplerResources();
		return resources.SandBox.categories[indexCategory].categoryName;
	}

	public function RefreshContent()
	{
		var i, size : int;

		Close();
		ClearContent();
		PreRefreshContent();

		size = options.Size();

		for ( i = 0; i < size; i += 1)
		{
			if ( GetHighlighted() == i ) {
				AddContent( GetConfig().highlightFont + "# Slot " + (i + 1) + ": " + menus[ i ].GetSubTitle() + "</font>");
			}
			else {
				AddContent( "Slot " + (i + 1) + ": " + menus[ i ].GetSubTitle());
			}

			if ( i < size - 1 ) {
				AddContent("<br />");
			}
		}

		Open();
	}
}


class GeraltDopplerSandBoxSlotMenu extends NotifyMenuOptions
{
	public var indexCurrent : int;
	public var indexCategory: int;
	public var resources : GeraltDopplerResources;

	public function Init()
	{
		super.Init();
		SetTitle("Slot");
	}

	public function GetTitle(): string
	{
		return "Slot " + (indexCurrent + 1);
	}

	public function SetCurrent(category: int, index : int)
	{
		indexCategory = category;
		indexCurrent = index;
	}

	public function GetSubTitle() : string
	{
		resources = GetGeraltDopplerResources();
		return resources.SandBox.categories[indexCategory].GetNameFromIndex(indexCurrent);
	}

	public function PreRefreshContent()
	{
		var i, index : int;

		resources = GetGeraltDopplerResources();

		if (resources.SandBox.categories[indexCategory].count == 0) {
			return;
		}
		
		ClearOptions();

		for ( i = 10; i >= 1; i -= 1) {
			index = resources.SandBox.categories[indexCategory].currents[indexCurrent] - i;

			if ( index >= 0 && index <= resources.SandBox.categories[indexCategory].count - 1 ) {
				AddOption( resources.SandBox.categories[indexCategory].names[index] );
			}
		}

		SetHighlightedIndex( GetOptionsSize() );
		AddOption( resources.SandBox.categories[indexCategory].GetNameFromIndex(indexCurrent) );

		for ( i = 1; i <= 10; i += 1) {
			index = resources.SandBox.categories[indexCategory].currents[indexCurrent] + i;

			if ( index >= 0 && index <= resources.SandBox.categories[indexCategory].count - 1 ) {
				AddOption( resources.SandBox.categories[indexCategory].names[index] );
			}
		}
	}

	public function GetActionsLabel() : string
	{
		var actionsLabel : string;

		actionsLabel = "";

		actionsLabel += GetConfig().highlightFont + "RIGHT</font> to UNLOAD selected<br />";
		actionsLabel += super.GetActionsLabel();

		return actionsLabel;
	}

	public function OnRight()
	{
		resources = GetGeraltDopplerResources();
		resources.SandBox.categories[indexCategory].UnloadPartFromIndex(indexCurrent);
		resources.SandBox.categories[indexCategory].currents[indexCurrent] = 0;
		resources.SandBox.categories[indexCategory].SaveToConfig(indexCurrent);
		Close();
		parentMenu.RefreshContent();
	}

	public function OnUp()
	{
		resources = GetGeraltDopplerResources();
		resources.SandBox.categories[indexCategory].SetPrevious(indexCurrent);
		RefreshContent();
	}

	public function OnDown()
	{
		resources = GetGeraltDopplerResources();
		resources.SandBox.categories[indexCategory].SetNext(indexCurrent);
		RefreshContent();
	}
}






class GeraltDopplerSandBoxFavoritesMenu extends NotifyMenuSubMenus
{
	var doppler : GeraltDoppler;
	var sandBox : GeraltDopplerSandBox;
	var resources : GeraltDopplerResources;
	var favorites : GeraltDopplerFavorites;

	public function Init()
	{
		super.Init();
		SetTitle("Favorites");
	}

	public function PreRefreshContent()
	{
		var i, size : int;
		var text : string;
		var slot : GeraltDopplerSandBoxFavoriteSlotMenu;

		doppler = GetGeraltDoppler();
		resources = GetGeraltDopplerResources();
		sandBox = doppler.resources.SandBox;

		ClearOptions();

		for ( i = 0; i < sandBox.favoriteSlotsCount; i += 1 )
		{
			slot = new GeraltDopplerSandBoxFavoriteSlotMenu in this;
			slot.SetTitle( "SLOT " + IntToString( i + 1 ));

			AddSubMenu( slot );
		}
	}

	public function RefreshContent()
	{
		var i, size : int;
		var newContent : string;

		Close();
		ClearContent();
		PreRefreshContent();

		size = options.Size();

		for ( i = 0; i < size; i += 1)
		{
			if ( GetHighlighted() == i ) {
				AddContent( GetConfig().highlightFont + "# " + options[i] + "<br />" );
			}
			else {
				AddContent( options[ i ] + "<br />" );
			}

			AddContent( "    " + resources.Head.GetName(sandBox.slots[i][0]) + "<br />");
			AddContent( "    " + resources.Hair.GetName(sandBox.slots[i][1]));

			if ( GetHighlighted() == i ) {
				AddContent( "</font>" );
			}

			if ( i < size - 1 ) {
				AddContent( "<br />" );
			}
		}

		Open();
	}

	public function GetActionsLabel() : string
	{
		var actionsLabel : string;

		actionsLabel = "";
		actionsLabel += GetConfig().highlightFont + "RIGHT</font> to enter ";
		actionsLabel += GetConfig().highlightFont + "Slot "+ IntToString(GetHighlighted() + 1) +"</font><br />";
		actionsLabel += GetConfig().highlightFont + "LEFT</font> to back<br />";

		return actionsLabel;
	}
}

class GeraltDopplerSandBoxFavoriteSlotMenu extends NotifyMenu
{
	var doppler : GeraltDoppler;
	var resources : GeraltDopplerResources;
	var parentM : GeraltDopplerSandBoxFavoritesMenu;

	public function Init()
	{
		super.Init();
	}

	public function Open()
	{
		var notificationText : string;

		notificationText  = GetConfig().globalFont;
		notificationText += GetFullTitle() + "<br /><br />";
		notificationText += content;
		notificationText += "<br />" + GetConfig().legendSeparatorText + "<br />";
		notificationText += GetConfig().highlightFont + "RIGHT</font> to ";
		notificationText += GetConfig().highlightFont + "LOAD</font> favorited<br />";
		notificationText += GetConfig().highlightFont + "DOWN</font> to ";
		notificationText += GetConfig().highlightFont + "SAVE</font> currently in use<br />";
		notificationText += GetActionsLabel();
		notificationText += "</font>";

		theGame.GetGuiManager().ShowNotification( notificationText, GetConfig().oppenedMenuDuration );
		isActive = true;
	}

	public function RefreshContent()
	{
		var newContent : string;

		parentM = (GeraltDopplerSandBoxFavoritesMenu)parentMenu;
		doppler = GetGeraltDoppler();
		resources = GetGeraltDopplerResources();

		Close();
		ClearContent();

		newContent  = GetConfig().highlightFont + "Currently Favorited</font><br />";
		newContent += resources.SandBox.GetFavoriteSlotDescription(parentM.openedMenuIndex) + "<br />";
		newContent += GetConfig().highlightFont + "Currently in Use</font><br />";

		newContent += "<font size='15'>";
		newContent += "Head: " + resources.Head.GetName(resources.currentHead) + "<br />";
		newContent += "Hair: " + resources.Hair.GetName(resources.currentHair) + "<br />";
		newContent += "Body:<br />";
		newContent += resources.SandBox.GetCurrentBodyDescription();
		newContent += "</font>";

		AddContent( newContent );

		Open();
	}

	public function OnRight()
	{
		resources.SandBox.LoadSlotToCurrent(parentM.openedMenuIndex);

		CloseAll();
	}

	public function OnDown()
	{
		var index : int;
		index = parentM.openedMenuIndex;

		resources.SandBox.SaveCurrentToSlot(index);

		CloseAll();
		theGame.GetGuiManager().ShowNotification( "SLOT " + IntToString( index + 1 ) + " saved", 3000 );
	}
}