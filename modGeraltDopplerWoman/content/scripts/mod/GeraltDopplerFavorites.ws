class GeraltDopplerFavorites
{
	var slotsNames : array<array<name>>;
	var slots : array<array<int>>;
	var categoryConfigName : name;
	var doppler : GeraltDoppler;

	public function Init()
	{
		var slotNames : array<name>;
		var slot0, slot1, slot2, slot3, slot4, slot5 : array<int>;

		categoryConfigName = 'Hidden';
		doppler = GetGeraltDoppler();

		slotNames.PushBack( 'GDFavorite1Head' );
		slotNames.PushBack( 'GDFavorite1Hair' );
		slotNames.PushBack( 'GDFavorite1Body' );
		slotsNames.PushBack( slotNames );
		slotNames.Clear();

		slotNames.PushBack( 'GDFavorite2Head' );
		slotNames.PushBack( 'GDFavorite2Hair' );
		slotNames.PushBack( 'GDFavorite2Body' );
		slotsNames.PushBack( slotNames );
		slotNames.Clear();

		slotNames.PushBack( 'GDFavorite3Head' );
		slotNames.PushBack( 'GDFavorite3Hair' );
		slotNames.PushBack( 'GDFavorite3Body' );
		slotsNames.PushBack( slotNames );
		slotNames.Clear();
		
		slotNames.PushBack( 'GDFavorite4Head' );
		slotNames.PushBack( 'GDFavorite4Hair' );
		slotNames.PushBack( 'GDFavorite4Body' );
		slotsNames.PushBack( slotNames );
		slotNames.Clear();
		
		slotNames.PushBack( 'GDFavorite5Head' );
		slotNames.PushBack( 'GDFavorite5Hair' );
		slotNames.PushBack( 'GDFavorite5Body' );
		slotsNames.PushBack( slotNames );
		slotNames.Clear();
		
		slotNames.PushBack( 'GDFavorite6Head' );
		slotNames.PushBack( 'GDFavorite6Hair' );
		slotNames.PushBack( 'GDFavorite6Body' );
		slotsNames.PushBack( slotNames );
		slotNames.Clear();


		slot0.PushBack( StringToInt(doppler.configWrapper.GetVarValue( categoryConfigName, 'GDFavorite1Head' )) );
		slot0.PushBack( StringToInt(doppler.configWrapper.GetVarValue( categoryConfigName, 'GDFavorite1Hair' )) );
		slot0.PushBack( StringToInt(doppler.configWrapper.GetVarValue( categoryConfigName, 'GDFavorite1Body' )) );
		slots.PushBack( slot0 );

		slot1.PushBack( StringToInt(doppler.configWrapper.GetVarValue( categoryConfigName, 'GDFavorite2Head' )) );
		slot1.PushBack( StringToInt(doppler.configWrapper.GetVarValue( categoryConfigName, 'GDFavorite2Hair' )) );
		slot1.PushBack( StringToInt(doppler.configWrapper.GetVarValue( categoryConfigName, 'GDFavorite2Body' )) );
		slots.PushBack( slot1 );

		slot2.PushBack( StringToInt(doppler.configWrapper.GetVarValue( categoryConfigName, 'GDFavorite3Head' )) );
		slot2.PushBack( StringToInt(doppler.configWrapper.GetVarValue( categoryConfigName, 'GDFavorite3Hair' )) );
		slot2.PushBack( StringToInt(doppler.configWrapper.GetVarValue( categoryConfigName, 'GDFavorite3Body' )) );
		slots.PushBack( slot2 );
		
		slot3.PushBack( StringToInt(doppler.configWrapper.GetVarValue( categoryConfigName, 'GDFavorite4Head' )) );
		slot3.PushBack( StringToInt(doppler.configWrapper.GetVarValue( categoryConfigName, 'GDFavorite4Hair' )) );
		slot3.PushBack( StringToInt(doppler.configWrapper.GetVarValue( categoryConfigName, 'GDFavorite4Body' )) );
		slots.PushBack( slot3 );

		slot4.PushBack( StringToInt(doppler.configWrapper.GetVarValue( categoryConfigName, 'GDFavorite5Head' )) );
		slot4.PushBack( StringToInt(doppler.configWrapper.GetVarValue( categoryConfigName, 'GDFavorite5Hair' )) );
		slot4.PushBack( StringToInt(doppler.configWrapper.GetVarValue( categoryConfigName, 'GDFavorite5Body' )) );
		slots.PushBack( slot4 );

		slot5.PushBack( StringToInt(doppler.configWrapper.GetVarValue( categoryConfigName, 'GDFavorite6Head' )) );
		slot5.PushBack( StringToInt(doppler.configWrapper.GetVarValue( categoryConfigName, 'GDFavorite6Hair' )) );
		slot5.PushBack( StringToInt(doppler.configWrapper.GetVarValue( categoryConfigName, 'GDFavorite6Body' )) );
		slots.PushBack( slot5 );
	}

	public function GetSlot( slot : int ) : array<int>
	{
		return slots[ slot ];
	}

	public function GetSlotHeadName( slot : int ) : string
	{
		if ( doppler.resources.Head.IsValid(slots[ slot ][0]) ) {
			return doppler.resources.Head.names[ slots[ slot ][0] ];
		}
		
		return "Unknown Head";
	}

	public function GetSlotHairName( slot : int ) : string
	{
		if ( doppler.resources.Hair.IsValid(slots[ slot ][1]) ) {
			return doppler.resources.Hair.names[ slots[ slot ][1] ];
		}
		
		return "Unknown Hair";
	}

	public function GetSlotBodyName( slot : int ) : string
	{
		if ( doppler.resources.Body.IsValid(slots[ slot ][2]) ) {
			return doppler.resources.Body.names[ slots[ slot ][2] ];
		}
		
		return "Unknown Body";
	}

	public function GetSlotDescription( slot : int, showHead : bool ) : string
	{

		var text : string;

		text  = "<font size='15'>";
		if ( showHead ) {
			text += "Head: " + doppler.resources.Head.names[ slots[ slot ][0] ] + "<br />";
		}
		text += "Hair: " + doppler.resources.Hair.names[ slots[ slot ][1] ] + "<br />";
		text += "Body: " + doppler.resources.Body.names[ slots[ slot ][2] ] + "<br /></font>";

		return text;
	}

	public function SaveToConfig( index: int, slot : array<int> )
	{
		var i : int;

		slots[ index ][0] = slot[0];
		slots[ index ][1] = slot[1];
		slots[ index ][2] = slot[2];

		for ( i = 0; i < 3; i += 1 )
		{
			doppler.configWrapper.SetVarValue( categoryConfigName, slotsNames[ index ][ i ], IntToString( slot[ i ] ) );
		}
	}
}