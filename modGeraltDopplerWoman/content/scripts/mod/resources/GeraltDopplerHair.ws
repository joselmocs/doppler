class GeraltDopplerHair
{
	public var list : array<array<CEntityTemplate>>;
	public var names : array<string>;
	public var count : int;
	private var resources : array<CEntityTemplate>;

	public function IncludeHair(hairName : string, hair : array<CEntityTemplate>)
	{
		list.PushBack(hair);
		names.PushBack(hairName);
		resources.Clear();
	}

	public function IsValid(index : int) : bool
	{
		return index >= 0 && index < count;
	}

	public function GetName(index: int) : string
	{
		return names[index];
	}
	
	public function Init()
	{
		Hair_Nohair();
		Hair_TrissHair();
		Hair_TrissHairHood();
		Hair_TrissHairDLC();
		Hair_CiriHair();
		Hair_CiriHairHood();
		Hair_VesHair();
		Hair_WomanHerbalistHair();
		Hair_TamaraHair();
		Hair_ShaniHair();
		Hair_YenneferHair();
		Hair_YenneferHairHood();
		Hair_KeiraHair();
		Hair_ShealaHair();
		Hair_FilippaHair();
		Hair_LuizaHair();
		Hair_PriscillaCaps1Red();
		Hair_PriscillaCaps2Red();
		Hair_WomanAaverageHairYellow();
		Hair_WomanAverageHairDarkBrown();
		Hair_WomanAverageHairBrown();
		Hair_WomanAverageHair1DarkBrown();
		Hair_WomanAverageHair1Grey();
		Hair_WomanAverageHair1ReddishBrown();
		Hair_WomanAverageHair1LightBrown();
		Hair_WomanAverageHairLightBrown();
		Hair_WomanAverageHairGreyBrown();
		Hair_WomanAverageHairRed();
		Hair_WomanAverageHair2LightBrown();
		Hair_WomanAverageHair2ReddishBrown();
		Hair_WomanAverageHair2DarkBrown();
		Hair_WomanAverageHair2Brown();
		Hair_BrideHairYellowBrown();
		Hair_WomanAverageHair2Black();
		Hair_EdnaHair();
		Hair_WomanAverageHair3Brown();
		Hair_VespulaHair();
		Hair_MargarittaHair();
		Hair_WomanAverageHair4ReddishBrown();
		Hair_WomanBrewessHairReddishBrown();
		Hair_WomanWhispessHairRed();
		Hair_WomanWhispessHairDarkBrown();
		Hair_WomanAverageHair5Golden();
		Hair_WomanAverageHair5ReddishBrown();
		Hair_IrinaHair();
		Hair_WomanAverageHair6Orange();
		Hair_WomanAverageHair6LightYellow();
		Hair_WomanAverageHair7Brown();
		Hair_WomanAverageHair8Brown();
		Hair_CorineHair();
		Hair_FringillaHair();
		Hair_EvelineHair();
		Hair_WomanElfHairReddishBrown();
		Hair_WomanElfHairLightBrown();
		Hair_WomanElfHairReddishBrown1();
		Hair_WomanWhispessHairGolden();
		Hair_WomanWhispessHairLightGolden();
		Hair_WomanAverageHair9White();
		Hair_IrisHair();
		Hair_CerysHair();
		Hair_ShaniHairWithRowan();
		Hair_SorceressHair();
		Hair_FeliciaCaps1Red();
		Hair_AnnaHair();
		Hair_NovigradCitizenCaps1Red();
		Hair_WomanAverageHairBrown1();
		Hair_NovigradCitizenCaps2Red();
		Hair_NovigradCitizenCaps3Red();
		Hair_NovigradCitizenCaps4Red();
		Hair_NovigradCitizenCaps5Red();
		Hair_NovigradCitizenCaps6Red();
		Hair_NovigradCitizenCaps7();
		Hair_NovigradCitizenCaps8Red();
		Hair_IdaCapsRed();
		Hair_WeavessHair();
		Hair_AnnaHenriettaHair1();
		Hair_AnnaHenriettaHood();
		Hair_Syanna();
		Hair_VivienneHair();
		Hair_OrianaHair();

		Hair_c_01_wa__bob();
		Hair_c_02_wa__bob();
		Hair_c_03_wa__bob();
		Hair_c_01__sq701_forest_nymph();
		Hair_c_01_wa__hair_olgierd();
		Hair_c_02_wa__hair_dlc();
		Hair_c_01_wa__hair_dlc();
		Hair_c_01_wa__novigrad_sorceress();
		Hair_c_03_wa__novigrad_sorceress();
		Hair_c_04_wa__novigrad_sorceress();
		Hair_c_06_wa__novigrad_sorceress();
		Hair_c_07_wa__novigrad_sorceress();
		Hair_c_02_wa__skellige_villager();
		Hair_c_03_wa__skellige_villager();
		Hair_c_04_wa__skellige_villager();
		Hair_c_05_wa__skellige_villager();
		Hair_c_09_wa__skellige_villager();
		Hair_c_06_wa__skellige_village();
		Hair_c_08_wa__skellige_village();

		count = list.Size();
	}

	public function Hair_Nohair()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\main_npc\triss\nonewomhair.w2ent", true));
		IncludeHair("Nohair", resources);
	}

	public function Hair_TrissHair()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\main_npc\triss\c_01_wa__triss.w2ent", true));
		IncludeHair("Triss Hair", resources);
	}

	public function Hair_TrissHairHood()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\main_npc\triss\c_02_wa__triss.w2ent", true));
		IncludeHair("Triss Hair Hood", resources);
	}

	public function Hair_TrissHairDLC()
	{
		resources.PushBack((CEntityTemplate)LoadResource("dlc\dlc6\data\characters\models\main_npc\triss\c_01_wa__triss_dlc.w2ent", true));
		IncludeHair("Triss Hair (DLC)", resources);
	}

	public function Hair_VesHair()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\hair\c_18_wa__ves.w2ent", true));
		IncludeHair("Ves Hair", resources);
	}

	public function Hair_WomanHerbalistHair()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\hair\c_12_wa__hair_herbalist.w2ent", true));
		IncludeHair("Woman Herbalist Hair", resources);
	}

	public function Hair_TamaraHair()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\hair\c_18_wa__tamara.w2ent", true));
		IncludeHair("Tamara Hair", resources);
	}

	public function Hair_ShaniHair()
	{
		resources.PushBack((CEntityTemplate)LoadResource("dlc\ep1\data\characters\models\secondary_npc\shani\c_01_wa__shani_hair.w2ent", true));
		IncludeHair("Shani Hair", resources);
	}

	public function Hair_YenneferHair()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\main_npc\yennefer\c_01_wa__yennefer.w2ent", true));
		IncludeHair("Yennefer Hair", resources);
	}

	public function Hair_YenneferHairHood()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\main_npc\yennefer\c_05_wa__yennefer.w2ent", true));
		IncludeHair("Yennefer Hair Hood", resources);
	}

	public function Hair_KeiraHair()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\main_npc\keira\c_01_wa__keira.w2ent", true));
		IncludeHair("Keira Hair", resources);
	}

	public function Hair_ShealaHair()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\main_npc\sheala\c_07_ma__sheala_hair.w2ent", true));
		IncludeHair("Sheala Hair", resources);
	}

	public function Hair_CiriHairHood()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\main_npc\ciri\c_01_wa__ciri.w2ent", true));
		IncludeHair("Ciri Hair", resources);
	}

	public function Hair_CiriHair()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\main_npc\ciri\c_06_wa__ciri.w2ent", true));
		IncludeHair("Ciri Hair Hood", resources);
	}

	public function Hair_FilippaHair()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\secondary_npc\filippa\c_01_wa__filippa.w2ent", true));
		IncludeHair("Filippa Hair", resources);
	}

	public function Hair_LuizaHair()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\secondary_npc\luiza\c_01_wa__luiza.w2ent", true));
		IncludeHair("Luiza Hair", resources);
	}

	public function Hair_PriscillaCaps1Red()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\secondary_npc\priscilla\c_01_wa__priscilla.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\secondary_npc\priscilla\hh_01_wa__priscilla.w2ent", true));
		IncludeHair("Priscilla Caps 1 (Red)", resources);
	}

	public function Hair_PriscillaCaps2Red()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\secondary_npc\priscilla\c_02_wa__priscilla.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\secondary_npc\priscilla\hh_01_wa__priscilla.w2ent", true));
		IncludeHair("Priscilla Caps 2 (Red)", resources);
	}

	public function Hair_WomanAaverageHairYellow()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\hair\c_01_wa__hair_01.w2ent", true));
		IncludeHair("Woman Aaverage Hair Yellow", resources);
	}

	public function Hair_WomanAverageHairDarkBrown()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\hair\c_01_wa__hair_02.w2ent", true));
		IncludeHair("Woman Average Hair Dark Brown", resources);
	}

	public function Hair_WomanAverageHairBrown()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\hair\c_01_wa__hair_03.w2ent", true));
		IncludeHair("Woman Average Hair Brown", resources);
	}

	public function Hair_WomanAverageHair1DarkBrown()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\hair\c_02_wa__hair.w2ent", true));
		IncludeHair("Woman Average Hair 1 Dark Brown", resources);
	}

	public function Hair_WomanAverageHair1Grey()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\hair\c_03_wa__hair.w2ent", true));
		IncludeHair("Woman Average Hair 1 Grey", resources);
	}

	public function Hair_WomanAverageHair1ReddishBrown()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\hair\c_04_wa__hair.w2ent", true));
		IncludeHair("Woman Average Hair 1 Reddish Brown", resources);
	}

	public function Hair_WomanAverageHair1LightBrown()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\hair\c_05_wa__hair.w2ent", true));
		IncludeHair("Woman Average Hair 1 Light Brown", resources);
	}

	public function Hair_WomanAverageHairLightBrown()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\hair\c_06_wa__hair_02.w2ent", true));
		IncludeHair("Woman Average Hair Light Brown", resources);
	}

	public function Hair_WomanAverageHairGreyBrown()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\hair\c_06_wa__hair_03.w2ent", true));
		IncludeHair("Woman Average Hair Grey Brown", resources);
	}

	public function Hair_WomanAverageHairRed()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\hair\c_06_wa__hair_04.w2ent", true));
		IncludeHair("Woman Average Hair Red", resources);
	}

	public function Hair_WomanAverageHair2LightBrown()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\hair\c_07_wa__hair_01.w2ent", true));
		IncludeHair("Woman Average Hair 2 Light Brown", resources);
	}

	public function Hair_WomanAverageHair2ReddishBrown()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\hair\c_07_wa__hair_02.w2ent", true));
		IncludeHair("Woman Average Hair 2 Reddish Brown", resources);
	}

	public function Hair_WomanAverageHair2DarkBrown()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\hair\c_07_wa__hair_03.w2ent", true));
		IncludeHair("Woman Average Hair 2 Dark Brown", resources);
	}

	public function Hair_WomanAverageHair2Brown()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\hair\c_08_wa__hair_01.w2ent", true));
		IncludeHair("Woman Average Hair 2 Brown", resources);
	}

	public function Hair_BrideHairYellowBrown()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\hair\c_08_wa__hair_02.w2ent", true));
		IncludeHair("Bride Hair Yellow Brown", resources);
	}

	public function Hair_WomanAverageHair2Black()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\hair\c_08_wa__hair_03.w2ent", true));
		IncludeHair("Woman Average Hair 2 Black", resources);
	}

	public function Hair_EdnaHair()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\hair\c_09_wa__hair_01.w2ent", true));
		IncludeHair("Edna Hair", resources);
	}

	public function Hair_WomanAverageHair3Brown()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\hair\c_09_wa__hair_02.w2ent", true));
		IncludeHair("Woman Average Hair 3 Brown", resources);
	}

	public function Hair_VespulaHair()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\hair\c_09_wa__hair_03.w2ent", true));
		IncludeHair("Vespula Hair", resources);
	}

	public function Hair_MargarittaHair()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\hair\c_10_wa__hair_01.w2ent", true));
		IncludeHair("Margaritta Hair", resources);
	}

	public function Hair_WomanAverageHair4ReddishBrown()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\hair\c_10_wa__hair_03.w2ent", true));
		IncludeHair("Woman Average Hair 4 Reddish Brown", resources);
	}

	public function Hair_WomanBrewessHairReddishBrown()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\hair\c_12_wa__hair_brewess.w2ent", true));
		IncludeHair("Woman Brewess Hair Reddish Brown", resources);
	}

	public function Hair_WomanWhispessHairRed()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\hair\c_14_wa__hair_whispess.w2ent", true));
		IncludeHair("Woman Whispess Hair Red", resources);
	}

	public function Hair_WomanWhispessHairDarkBrown()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\hair\c_14_wa__hair_whispess02.w2ent", true));
		IncludeHair("Woman Whispess Hair Dark Brown", resources);
	}

	public function Hair_WomanAverageHair5Golden()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\hair\c_15_wa__hair_02.w2ent", true));
		IncludeHair("Woman Average Hair 5 Golden", resources);
	}

	public function Hair_WomanAverageHair5ReddishBrown()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\hair\c_15_wa__hair_03.w2ent", true));
		IncludeHair("Woman Average Hair 5 Reddish Brown", resources);
	}

	public function Hair_IrinaHair()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\hair\c_15_wa__hair_04.w2ent", true));
		IncludeHair("Irina Hair", resources);
	}

	public function Hair_WomanAverageHair6Orange()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\hair\c_16_wa__hair_cerys.w2ent", true));
		IncludeHair("Woman Average Hair 6 Orange", resources);
	}

	public function Hair_WomanAverageHair6LightYellow()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\hair\c_17_wa__hair_02.w2ent", true));
		IncludeHair("Woman Average Hair 6 Light Yellow", resources);
	}

	public function Hair_WomanAverageHair7Brown()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\hair\c_17_wa__hair_03.w2ent", true));
		IncludeHair("Woman Average Hair 7 Brown", resources);
	}

	public function Hair_WomanAverageHair8Brown()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\hair\c_17_wa__hair_04.w2ent", true));
		IncludeHair("Woman Average Hair 7 Brown", resources);
	}

	public function Hair_CorineHair()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\hair\c_17_wa__hair_corine.w2ent", true));
		IncludeHair("Corine Hair", resources);
	}

	public function Hair_FringillaHair()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\hair\c_18_wa__fringilla.w2ent", true));
		IncludeHair("Fringilla Hair", resources);
	}

	public function Hair_EvelineHair()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\hair\c_19_wa_hair_01.w2ent", true));
		IncludeHair("Eveline Hair", resources);
	}

	public function Hair_WomanElfHairReddishBrown()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\hair\c_20_wa__elf_03.w2ent", true));
		IncludeHair("Woman Elf Hair Reddish Brown", resources);
	}

	public function Hair_WomanElfHairLightBrown()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\hair\c_20_wa__elf_04.w2ent", true));
		IncludeHair("Woman Elf Hair Light Brown", resources);
	}

	public function Hair_WomanElfHairReddishBrown1()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\hair\c_21_wa__elf_01.w2ent", true));
		IncludeHair("Woman Elf Hair Reddish Brown 1", resources);
	}

	public function Hair_WomanWhispessHairGolden()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\hair\c_21_wa__elf_03.w2ent", true));
		IncludeHair("Woman Whispess Hair Golden", resources);
	}

	public function Hair_WomanWhispessHairLightGolden()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\hair\c_21_wa__elf_03_naked.w2ent", true));
		IncludeHair("Woman Whispess Hair Light Golden", resources);
	}

	public function Hair_WomanAverageHair9White()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\hair\c_22_wa__hair_02.w2ent", true));
		IncludeHair("Woman Average Hair 9 White", resources);
	}

	public function Hair_IrisHair()
	{
		resources.PushBack((CEntityTemplate)LoadResource("dlc\ep1\data\characters\models\main_npc\iris\c_01_wa__iris.w2ent", true));
		IncludeHair("Iris Hair", resources);
	}

	public function Hair_CerysHair()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\main_npc\cerys\c_16_wa__cerys02.w2ent", true));
		IncludeHair("Cerys Hair", resources);
	}

	public function Hair_ShaniHairWithRowan()
	{
		resources.PushBack((CEntityTemplate)LoadResource("dlc\ep1\data\characters\models\secondary_npc\shani\c_01_wa__shani_hair.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("dlc\ep1\data\characters\models\secondary_npc\shani\c_01_wa__shani.w2ent", true));
		IncludeHair("Shani Hair (With Rowan)", resources);
	}

	public function Hair_SorceressHair()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_sorceress_woman\caps\c_05_wa__novigrad_sorceress.w2ent", true));
		IncludeHair("Sorceress Hair ", resources);
	}

	public function Hair_FeliciaCaps1Red()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_sorceress_woman\caps\c_02_wa__novigrad_sorceress.w2ent", true));
		IncludeHair("Felicia Caps 1 (Red)", resources);
	}

	public function Hair_AnnaHair()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\secondary_npc\anna\hh_01_wa__anna.w2ent", true));
		IncludeHair("Anna Hair", resources);
	}

	public function Hair_NovigradCitizenCaps1Red()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\caps\c_02_wa__novigrad_citizen.w2ent", true));
		IncludeHair("Novigrad Citizen Caps 1 (Red)", resources);
	}

	public function Hair_WomanAverageHairBrown1()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\hair\c_06_wa__hair_01.w2ent", true));
		IncludeHair("Woman Average Hair Brown 1", resources);
	}

	public function Hair_NovigradCitizenCaps2Red()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\caps\c_03_wa__novigrad_citizen.w2ent", true));
		IncludeHair("Novigrad Citizen Caps 2 (Red)", resources);
	}

	public function Hair_NovigradCitizenCaps3Red()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\caps\c_01_wa__novigrad_citizen.w2ent", true));
		IncludeHair("Novigrad Citizen Caps 3 (Red)", resources);
	}

	public function Hair_NovigradCitizenCaps4Red()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\caps\c_04_wa__novigrad_citizen.w2ent", true));
		IncludeHair("Novigrad Citizen Caps 4 (Red)", resources);
	}

	public function Hair_NovigradCitizenCaps5Red()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\caps\c_09_wa__novigrad_citizen.w2ent", true));
		IncludeHair("Novigrad Citizen Caps 5 (Red)", resources);
	}

	public function Hair_NovigradCitizenCaps6Red()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\caps\c_06_wa__novigrad_citizen.w2ent", true));
		IncludeHair("Novigrad Citizen Caps 6 (Red)", resources);
	}

	public function Hair_NovigradCitizenCaps7()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\caps\c_07_wa__novigrad_citizen.w2ent", true));
		IncludeHair("Novigrad Citizen Caps 7", resources);
	}

	public function Hair_NovigradCitizenCaps8Red()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\caps\c_08_wa__novigrad_citizen.w2ent", true));
		IncludeHair("Novigrad Citizen Caps 8 (Red)", resources);
	}

	public function Hair_IdaCapsRed()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\secondary_npc\ida_emean_aep_sivney\c_13_wa__ida_emean.w2ent", true));
		IncludeHair("Ida Caps (Red)", resources);
	}

	public function Hair_WeavessHair()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\hair\c_13_wa__hair_weavess.w2ent", true));
		IncludeHair("Weavess Hair", resources);
	}

	public function Hair_AnnaHenriettaHair1()
	{
		resources.PushBack((CEntityTemplate)LoadResource("dlc\bob\data\characters\models\main_npc\anna_henrietta\c_01_wa__anna_henrietta.w2ent", true));
		IncludeHair("Anna Henrietta Hair 1", resources);
	}

	public function Hair_AnnaHenriettaHood()
	{
		resources.PushBack((CEntityTemplate)LoadResource("dlc\bob\data\characters\models\main_npc\anna_henrietta\c_04_wa__anna_henrietta_travel.w2ent", true));
		IncludeHair("Anna Henrietta Hood", resources);
	}

	public function Hair_Syanna()
	{
		resources.PushBack((CEntityTemplate)LoadResource("dlc\bob\data\characters\models\main_npc\syanna\c_01_wa__syanna.w2ent", true));
		IncludeHair("Syanna", resources);
	}

	public function Hair_VivienneHair()
	{
		resources.PushBack((CEntityTemplate)LoadResource("dlc\bob\data\characters\models\main_npc\vivienne_de_tabris\c_01_wa__vivienne.w2ent", true));
		IncludeHair("Vivienne Hair", resources);
	}

	public function Hair_OrianaHair()
	{
		resources.PushBack((CEntityTemplate)LoadResource("dlc\bob\data\characters\models\main_npc\oriana\c_01_wa__oriana.w2ent", true));
		IncludeHair("Oriana Hair", resources);
	}


	public function Hair_c_01_wa__bob()
	{
		resources.PushBack((CEntityTemplate)LoadResource("dlc\bob\data\characters\models\crowd_npc\bob_citizen_woman\caps\c_01_wa__bob.w2ent", true));
		IncludeHair("c_01_wa__bob", resources);
	}

	public function Hair_c_02_wa__bob()
	{
		resources.PushBack((CEntityTemplate)LoadResource("dlc\bob\data\characters\models\crowd_npc\bob_citizen_woman\caps\c_02_wa__bob.w2ent", true));
		IncludeHair("c_02_wa__bob", resources);
	}

	public function Hair_c_03_wa__bob()
	{
		resources.PushBack((CEntityTemplate)LoadResource("dlc\bob\data\characters\models\crowd_npc\bob_citizen_woman\caps\c_03_wa__bob.w2ent", true));
		IncludeHair("c_03_wa__bob", resources);
	}

	public function Hair_c_01__sq701_forest_nymph()
	{
		resources.PushBack((CEntityTemplate)LoadResource("dlc\bob\data\characters\models\secondary_npc\sq701_forest_nymph\c_01__sq701_forest_nymph.w2ent", true));
		IncludeHair("c_01__sq701_forest_nymph", resources);
	}

	public function Hair_c_01_wa__hair_olgierd()
	{
		resources.PushBack((CEntityTemplate)LoadResource("dlc\ep1\data\characters\models\common\woman_average\hair\c_01_wa__hair_olgierd.w2ent", true));
		IncludeHair("c_01_wa__hair_olgierd", resources);
	}

	public function Hair_c_02_wa__hair_dlc()
	{
		resources.PushBack((CEntityTemplate)LoadResource("dlc\bob\data\characters\models\common\woman_average\hair\c_02_wa__hair_dlc.w2ent", true));
		IncludeHair("c_02_wa__hair_dlc", resources);
	}

	public function Hair_c_01_wa__hair_dlc()
	{
		resources.PushBack((CEntityTemplate)LoadResource("dlc\bob\data\characters\models\common\woman_average\hair\c_01_wa__hair_dlc.w2ent", true));
		IncludeHair("c_01_wa__hair_dlc", resources);
	}

	public function Hair_c_01_wa__novigrad_sorceress()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_sorceress_woman\caps\c_01_wa__novigrad_sorceress.w2ent", true));
		IncludeHair("c_01_wa__novigrad_sorceress", resources);
	}

	public function Hair_c_03_wa__novigrad_sorceress()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_sorceress_woman\caps\c_03_wa__novigrad_sorceress.w2ent", true));
		IncludeHair("c_03_wa__novigrad_sorceress", resources);
	}

	public function Hair_c_04_wa__novigrad_sorceress()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_sorceress_woman\caps\c_04_wa__novigrad_sorceress.w2ent", true));
		IncludeHair("c_04_wa__novigrad_sorceress", resources);
	}

	public function Hair_c_06_wa__novigrad_sorceress()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_sorceress_woman\caps\c_06_wa__novigrad_sorceress.w2ent", true));
		IncludeHair("c_06_wa__novigrad_sorceress", resources);
	}

	public function Hair_c_07_wa__novigrad_sorceress()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_sorceress_woman\caps\c_07_wa__novigrad_sorceress.w2ent", true));
		IncludeHair("c_07_wa__novigrad_sorceress", resources);
	}

	public function Hair_c_02_wa__skellige_villager()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\skellige_villager_woman\caps\c_02_wa__skellige_villager.w2ent", true));
		IncludeHair("c_02_wa__skellige_villager", resources);
	}

	public function Hair_c_03_wa__skellige_villager()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\skellige_villager_woman\caps\c_03_wa__skellige_villager.w2ent", true));
		IncludeHair("c_03_wa__skellige_villager", resources);
	}

	public function Hair_c_04_wa__skellige_villager()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\skellige_villager_woman\caps\c_04_wa__skellige_villager.w2ent", true));
		IncludeHair("c_04_wa__skellige_villager", resources);
	}

	public function Hair_c_05_wa__skellige_villager()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\skellige_villager_woman\caps\c_05_wa__skellige_villager.w2ent", true));
		IncludeHair("c_05_wa__skellige_villager", resources);
	}

	public function Hair_c_09_wa__skellige_villager()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\skellige_villager_woman\caps\c_09_wa__skellige_villager.w2ent", true));
		IncludeHair("c_09_wa__skellige_villager", resources);
	}

	public function Hair_c_06_wa__skellige_village()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\skellige_villager_woman\caps\c_06_wa__skellige_village.w2ent", true));
		IncludeHair("c_06_wa__skellige_village", resources);
	}

	public function Hair_c_08_wa__skellige_village()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\skellige_villager_woman\caps\c_08_wa__skellige_village.w2ent", true));
		IncludeHair("c_08_wa__skellige_village", resources);
	}

}