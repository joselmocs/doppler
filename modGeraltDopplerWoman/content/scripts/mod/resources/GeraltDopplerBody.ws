class GeraltDopplerBody
{
	public var list : array<array<CEntityTemplate>>;
	public var names : array<string>;
	public var count : int;
	private var resources : array<CEntityTemplate>;

	public function IncludeBody(bodyName : string, body : array<CEntityTemplate>)
	{
		list.PushBack(body);
		names.PushBack(bodyName);
		resources.Clear();
	}

	public function IsValid(index : int) : bool
	{
		return index >= 0 && index < count;
	}

	public function Init()
	{
		Body_TrissClothesCasual();
		Body_TrissclothesHoodUp();
		Body_TrissclothesHoodDown();
		Body_TrissclothesHoodDownTortured();
		Body_TrissClothesLingerieWithSandals();
		Body_TrissClothesElegantGown();
		Body_CiriClothesFur();
		Body_CiriClothesBandages();
		Body_CiriClothesCasual();
		Body_CiriClothesBandagesNudeWithSandals();
		Body_CiriClothesCasualDirty();
		Body_CiriClothesHoodUp();
		Body_CiriBodyNudeWithSandals();
		Body_CiriClothesBathTowelWithSandalsRed();
		Body_KeiraClothesDress1();
		Body_KeiraClothesDress2();
		Body_KeiraClothesDress2andScroll();
		Body_KeiraBodyNudeWetWithSandals();
		Body_KeiraBodyNudeDryWithSandals();
		Body_ShealaClothesRobe();
		Body_YenneferClothesLingerieWithSandals();
		Body_YenneferBodyNudeWithSandals();
		Body_YenneferClothesDress();
		Body_YenneferClothesDressCloaked();
		Body_YenneferClothesTrousers();
		Body_VesClothesCasual();
		Body_AnabelleClothesCasualRed();
		Body_AnnaClothesCasual();
		Body_FilippaClothesCasual();
		Body_LadyBranClothesCasual();
		Body_LuizaClothesCasual();
		Body_TamaraClothesCasual();
		Body_ShaniClothesCasualHeartsofStoneDLC();
		Body_ShaniClothesDressHeartsofStoneDLC();
		Body_IrisClothesDressVeilHeartsofStoneDLC();
		Body_IrisClothesDressHeartsofStoneDLC();
		Body_YenneferClothesDressDLC();
		Body_YenneferClothesDressScarfDLC();
		Body_YenneferClothesDressCloakedDLC();
		Body_YenneferClothesDressHoodUpDLC();
		Body_TrissClothesDressDLC();
		Body_TrissClothesDressHoodUpDLC();
		Body_TrissClothesDressCloakedDLC();
		Body_TrissClothesLingerieWithSandalsDLC();
		Body_CiriClothesPantsnDressDLC();
		Body_CiriClothesPantsnDressFurDLC();
		Body_CiriClothesPantsnDressHoodUpDLC();
		Body_CiriClothesPantsnDressCloakedDLC();
		Body_CiriClothesPantsnDressBloodyDLC();
		Body_TrissBodyNudeWithSandals();
		Body_BathRobeRed();
		Body_EvelineClothesCasualMasked();
		Body_FringillaClothesCasualRed();
		Body_TrissClothesLingerie();
		Body_YenneferClothesLingerie();
		Body_CiriBodyNude();
		Body_KeiraBodyNudeWet();
		Body_KeiraBodyNudeDry();
		Body_YenneferBodyNude();
		Body_TrissBodyNude();
		Body_BrideClothes();
		Body_BrideVillagerClothesRed();
		Body_CerysClothesCasualRed();
		Body_EdnaRosaClothesDressRed();
		Body_RosaClothesCasualRed();
		Body_FeliciaClothesCasualRed();
		Body_IrinaClothesDressRed();
		Body_IrinaClothesCasualRed();
		Body_MargarittaClothesDress();
		Body_PriscillaClothesSceneRed();
		Body_PriscillaClothesCasualRed();
		Body_VespulaClothesCasualRed();
		Body_EvelineClothesCasual();
		Body_CorineClothesCasual();
		Body_YenneferClothesLingerieTopWithoutPantieswSandals();
		Body_YenneferClothesLingeriePantiesWithoutTopwSandals();
		Body_ProstituteClothes1Red();
		Body_ProstituteClothes2Red();
		Body_ProstituteClothes3PartiallyRed();
		Body_ProstituteClothes4Red();
		Body_ProstituteClothes5Red();
		Body_ProstituteClothes6Red();
		Body_ProstituteClothes7Red();
		Body_ProstituteClothes8Red();
		Body_ProstituteClothes9Red();
		Body_ProstituteClothes10Red();
		Body_ProstituteClothes11Red();
		Body_ProstituteClothes12Red();
		Body_IrismotherClothesCasualRed();
		Body_NoblemansdaughterClothesCasualRed();
		Body_NudebijouterieTrissAnnaHenriettaothermix();
		Body_AnnaHenriettapantscorset();
		Body_AnnaHenriettatravel();
		Body_Syannacasualribbon();
		Body_Syannanaked();

		count = list.Size();
	}

	public function Body_TrissClothesCasual()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\main_npc\triss\body_01_wa__triss.w2ent", true));
		IncludeBody("Triss Clothes Casual", resources);
	}

	public function Body_TrissclothesHoodUp()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\main_npc\triss\body_01_wa__triss_hooded_01.w2ent", true));
		IncludeBody("Triss clothes Hood Up", resources);
	}

	public function Body_TrissclothesHoodDown()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\main_npc\triss\body_01_wa__triss_hooded_02.w2ent", true));
		IncludeBody("Triss clothes Hood Down", resources);
	}

	public function Body_TrissclothesHoodDownTortured()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\main_npc\triss\body_01_wa__triss_tortured.w2ent", true));
		IncludeBody("Triss clothes Hood Down Tortured", resources);
	}

	public function Body_TrissClothesLingerieWithSandals()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\main_npc\triss\body_02_wa__triss.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_prostitute\shoes\s_01_wa__novigrad_prostitute.w2ent", true));
		IncludeBody("Triss Clothes Lingerie With Sandals", resources);
	}

	public function Body_TrissClothesElegantGown()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\main_npc\triss\body_03_wa__triss.w2ent", true));
		IncludeBody("Triss Clothes Elegant Gown", resources);
	}

	public function Body_CiriClothesFur()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\main_npc\ciri\body_01_wa__ciri_fur.w2ent", true));
		IncludeBody("Ciri Clothes Fur", resources);
	}

	public function Body_CiriClothesBandages()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\main_npc\ciri\body_01_wa__ciri_bandaged.w2ent", true));
		IncludeBody("Ciri Clothes Bandages", resources);
	}

	public function Body_CiriClothesCasual()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\main_npc\ciri\body_01_wa__ciri.w2ent", true));
		IncludeBody("Ciri Clothes Casual", resources);
	}

	public function Body_CiriClothesBandagesNudeWithSandals()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\main_npc\ciri\body_01_wa__ciri_bandaged_naked.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_prostitute\shoes\s_01_wa__novigrad_prostitute.w2ent", true));
		IncludeBody("Ciri Clothes Bandages Nude With Sandals", resources);
	}

	public function Body_CiriClothesCasualDirty()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\main_npc\ciri\body_01_wa__ciri_dirty.w2ent", true));
		IncludeBody("Ciri Clothes Casual Dirty", resources);
	}

	public function Body_CiriClothesHoodUp()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\main_npc\ciri\body_01_wa__ciri_hooded_02.w2ent", true));
		IncludeBody("Ciri Clothes Hood Up", resources);
	}

	public function Body_CiriBodyNudeWithSandals()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\main_npc\ciri\body_03_wa__ciri.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_prostitute\shoes\s_01_wa__novigrad_prostitute.w2ent", true));
		IncludeBody("Ciri Body Nude With Sandals", resources);
	}

	public function Body_CiriClothesBathTowelWithSandalsRed()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\main_npc\ciri\body_04_wa__ciri.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_prostitute\shoes\s_01_wa__novigrad_prostitute.w2ent", true));
		IncludeBody("Ciri Clothes Bath Towel With Sandals (Red)", resources);
	}

	public function Body_KeiraClothesDress1()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\main_npc\keira\body_01_wa__nml_keira.w2ent", true));
		IncludeBody("Keira Clothes Dress 1", resources);
	}

	public function Body_KeiraClothesDress2()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\main_npc\keira\body_03_wa__nml_keira.w2ent", true));
		IncludeBody("Keira Clothes Dress 2", resources);
	}

	public function Body_KeiraClothesDress2andScroll()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\main_npc\keira\body_05_wa__nml_keira.w2ent", true));
		IncludeBody("Keira Clothes Dress 2 and Scroll", resources);
	}

	public function Body_KeiraBodyNudeWetWithSandals()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\main_npc\keira\body_06_wa__body_keira_wet.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_prostitute\shoes\s_01_wa__novigrad_prostitute.w2ent", true));
		IncludeBody("Keira Body Nude Wet With Sandals", resources);
	}

	public function Body_KeiraBodyNudeDryWithSandals()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\main_npc\keira\body_07_wa__body_keira.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_prostitute\shoes\s_01_wa__novigrad_prostitute.w2ent", true));
		IncludeBody("Keira Body Nude Dry With Sandals", resources);
	}

	public function Body_ShealaClothesRobe()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\main_npc\sheala\body_01__sheala.w2ent", true));
		IncludeBody("Sheala Clothes Robe", resources);
	}

	public function Body_YenneferClothesLingerieWithSandals()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\main_npc\yennefer\body_yennefer__lingerie.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_prostitute\shoes\s_01_wa__novigrad_prostitute.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\main_npc\yennefer\pendant_03_wa__yennefer.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\lingerie\legs\l_01a_wa__lingerie_slim.w2ent", true));
		IncludeBody("Yennefer Clothes Lingerie With Sandals", resources);
	}

	public function Body_YenneferBodyNudeWithSandals()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\main_npc\yennefer\twals_01_wa__body_notcensored.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_prostitute\shoes\s_01_wa__novigrad_prostitute.w2ent", true));
		IncludeBody("Yennefer Body Nude With Sandals", resources);
	}

	public function Body_YenneferClothesDress()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\main_npc\yennefer\b_01_wa__yennefer.w2ent", true));
		IncludeBody("Yennefer Clothes Dress ", resources);
	}

	public function Body_YenneferClothesDressCloaked()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\main_npc\yennefer\b_01_wa__yennefer_hooded_02.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\main_npc\yennefer\l_02_wa__yennefer.w2ent", true));
		IncludeBody("Yennefer Clothes Dress Cloaked", resources);
	}

	public function Body_YenneferClothesTrousers()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\main_npc\yennefer\b_03_wa_yennefer.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\main_npc\yennefer\l_02_wa__yennefer.w2ent", true));
		IncludeBody("Yennefer Clothes Trousers ", resources);
	}

	public function Body_VesClothesCasual()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\secondary_npc\ves\body_01_wa__ves.w2ent", true));
		IncludeBody("Ves Clothes Casual ", resources);
	}

	public function Body_AnabelleClothesCasualRed()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\secondary_npc\anabelle\body_01_wa__anabelle.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\shadowmesh\woman_dress_shadowmesh_01.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\body\g_01_wa__body.w2ent", true));
		IncludeBody("Anabelle Clothes Casual (Red)", resources);
	}

	public function Body_AnnaClothesCasual()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\secondary_npc\anna\body_02_wa__anna.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\shoes\s_02_wa__novigrad_citizen.w2ent", true));
		IncludeBody("Anna Clothes Casual", resources);
	}

	public function Body_FilippaClothesCasual()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\secondary_npc\filippa\body_01__filippa.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\secondary_npc\filippa\ag2_01_wa__body_filippa.w2ent", true));
		IncludeBody("Filippa Clothes Casual", resources);
	}

	public function Body_LadyBranClothesCasual()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\secondary_npc\lady_bran\body_01_wa__lady_bran.w2ent", true));
		IncludeBody("Lady Bran Clothes Casual", resources);
	}

	public function Body_LuizaClothesCasual()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\secondary_npc\luiza\t_01_wa__luiza.w2ent", true));
		IncludeBody("Luiza Clothes Casual", resources);
	}

	public function Body_TamaraClothesCasual()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\secondary_npc\tamara\body_01_wa__tamara.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\body\a2g_01_wa__body.w2ent", true));
		IncludeBody("Tamara Clothes Casual", resources);
	}

	public function Body_ShaniClothesCasualHeartsofStoneDLC()
	{
		resources.PushBack((CEntityTemplate)LoadResource("dlc\ep1\data\characters\models\secondary_npc\shani\b_01_wa__shani.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\body\a2g_01_wa__body.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("dlc\ep1\data\characters\models\secondary_npc\shani\i_01_wa__shani.w2ent", true));
		IncludeBody("Shani Clothes Casual (Hearts of Stone DLC)", resources);
	}

	public function Body_ShaniClothesDressHeartsofStoneDLC()
	{
		resources.PushBack((CEntityTemplate)LoadResource("dlc\ep1\data\characters\models\secondary_npc\shani\b_02_wa__shani.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\body\a2g_01_wa__body.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("dlc\ep1\data\characters\models\secondary_npc\shani\i_01_wa__shani.w2ent", true));
		IncludeBody("Shani Clothes Dress (Hearts of Stone DLC)", resources);
	}

	public function Body_IrisClothesDressVeilHeartsofStoneDLC()
	{
		resources.PushBack((CEntityTemplate)LoadResource("dlc\ep1\data\characters\models\main_npc\iris\body_01_wa__iris.w2ent", true));
		IncludeBody("Iris Clothes Dress Veil (Hearts of Stone DLC)", resources);
	}

	public function Body_IrisClothesDressHeartsofStoneDLC()
	{
		resources.PushBack((CEntityTemplate)LoadResource("dlc\ep1\data\characters\models\main_npc\iris\body_03_wa__iris.w2ent", true));
		IncludeBody("Iris Clothes Dress (Hearts of Stone DLC)", resources);
	}

	public function Body_YenneferClothesDressDLC()
	{
		resources.PushBack((CEntityTemplate)LoadResource("dlc\dlc4\data\characters\models\main_npc\yennefer\body_05__yennefer.w2ent", true));
		IncludeBody("Yennefer Clothes Dress (DLC)", resources);
	}

	public function Body_YenneferClothesDressScarfDLC()
	{
		resources.PushBack((CEntityTemplate)LoadResource("dlc\dlc4\data\characters\models\main_npc\yennefer\body_05__yennefer_travel.w2ent", true));
		IncludeBody("Yennefer Clothes Dress Scarf (DLC)", resources);
	}

	public function Body_YenneferClothesDressCloakedDLC()
	{
		resources.PushBack((CEntityTemplate)LoadResource("dlc\dlc4\data\characters\models\main_npc\yennefer\body_05__yennefer_hooded_02.w2ent", true));
		IncludeBody("Yennefer Clothes Dress Cloaked (DLC)", resources);
	}

	public function Body_YenneferClothesDressHoodUpDLC()
	{
		resources.PushBack((CEntityTemplate)LoadResource("dlc\dlc4\data\characters\models\main_npc\yennefer\body_05__yennefer_hooded.w2ent", true));
		IncludeBody("Yennefer Clothes Dress Hood Up (DLC)", resources);
	}

	public function Body_TrissClothesDressDLC()
	{
		resources.PushBack((CEntityTemplate)LoadResource("dlc\dlc6\data\characters\models\main_npc\triss\b_01_wa__triss_dlc.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\items\i_31_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_prostitute\shoes\s_01_wa__novigrad_prostitute.w2ent", true));
		IncludeBody("Triss Clothes Dress (DLC)", resources);
	}

	public function Body_TrissClothesDressHoodUpDLC()
	{
		resources.PushBack((CEntityTemplate)LoadResource("dlc\dlc6\data\characters\models\main_npc\triss\b_01_wa__triss_dlc_hooded_01.w2ent", true));
		IncludeBody("Triss Clothes Dress Hood Up (DLC)", resources);
	}

	public function Body_TrissClothesDressCloakedDLC()
	{
		resources.PushBack((CEntityTemplate)LoadResource("dlc\dlc6\data\characters\models\main_npc\triss\b_01_wa__triss_dlc_hooded_02.w2ent", true));
		IncludeBody("Triss Clothes Dress Cloaked (DLC)", resources);
	}

	public function Body_TrissClothesLingerieWithSandalsDLC()
	{
		resources.PushBack((CEntityTemplate)LoadResource("dlc\dlc6\data\characters\models\main_npc\triss\body_02_wa__triss_dlc.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_prostitute\shoes\s_01_wa__novigrad_prostitute.w2ent", true));
		IncludeBody("Triss Clothes Lingerie With Sandals (DLC)", resources);
	}

	public function Body_CiriClothesPantsnDressDLC()
	{
		resources.PushBack((CEntityTemplate)LoadResource("dlc\dlc11\data\characters\models\main_npc\ciri\body_01_wa__ciri_dlc_dirty.w2ent", true));
		IncludeBody("Ciri Clothes Pants n Dress (DLC)", resources);
	}

	public function Body_CiriClothesPantsnDressFurDLC()
	{
		resources.PushBack((CEntityTemplate)LoadResource("dlc\dlc11\data\characters\models\main_npc\ciri\body_01_wa__ciri_dlc_fur.w2ent", true));
		IncludeBody("Ciri Clothes Pants n Dress Fur (DLC)", resources);
	}

	public function Body_CiriClothesPantsnDressHoodUpDLC()
	{
		resources.PushBack((CEntityTemplate)LoadResource("dlc\dlc11\data\characters\models\main_npc\ciri\body_01_wa__ciri_dlc_hooded.w2ent", true));
		IncludeBody("Ciri Clothes Pants n Dress Hood Up (DLC)", resources);
	}

	public function Body_CiriClothesPantsnDressCloakedDLC()
	{
		resources.PushBack((CEntityTemplate)LoadResource("dlc\dlc11\data\characters\models\main_npc\ciri\body_01_wa__ciri_dlc_hooded_02.w2ent", true));
		IncludeBody("Ciri Clothes Pants n Dress Cloaked (DLC)", resources);
	}

	public function Body_CiriClothesPantsnDressBloodyDLC()
	{
		resources.PushBack((CEntityTemplate)LoadResource("dlc\dlc11\data\characters\models\main_npc\ciri\body_01_wa__ciri_dlc_wounded.w2ent", true));
		IncludeBody("Ciri Clothes Pants n Dress Bloody (DLC)", resources);
	}

	public function Body_TrissBodyNudeWithSandals()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\naked_bodies\torso\twals_02_wa__body.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_prostitute\shoes\s_01_wa__novigrad_prostitute.w2ent", true));
		IncludeBody("Triss Body Nude With Sandals", resources);
	}

	public function Body_BathRobeRed()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\bath_robes\dress\t2las_0_wa__bath_robe.w2ent", true));
		IncludeBody("Bath Robe (Red)", resources);
	}

	public function Body_EvelineClothesCasualMasked()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\legs\l2_05_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\body\l_01_wa__body.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\shoes\s_05_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_sorceress_woman\arms\a_01_wa__novigrad_sorceress.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\items\i_35_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("dlc\ep1\data\characters\models\secondary_npc\eveline_gallo\i_01_wa__eveline_gallo.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("dlc\ep1\data\characters\models\secondary_npc\eveline_gallo\i_02_wa__eveline_gallo.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\body\a2g_01_wa__body.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("dlc\ep1\data\characters\models\secondary_npc\eveline_gallo\t2_01_wa__eveline_gallo.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("dlc\ep1\data\items\quest_items\q603\q603_item__hanselt_mask_eveline.w2ent", true));
		IncludeBody("Eveline Clothes Casual Masked", resources);
	}

	public function Body_FringillaClothesCasualRed()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_sorceress_woman\dress\d_01_wa__novigrad_sorceress.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_sorceress_woman\items\i_02_wa__novigrad_sorceress.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_sorceress_woman\arms\a_03_wa__novigrad_sorceress.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\body\l_01_wa__body.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_sorceress_woman\torso\t_02_wa__novigrad_sorceress.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_prostitute\shoes\s_01_wa__body_prostitute.w2ent", true));
		IncludeBody("Fringilla Clothes Casual (Red)", resources);
	}

	public function Body_TrissClothesLingerie()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\main_npc\triss\body_02_wa__triss.w2ent", true));
		IncludeBody("Triss Clothes Lingerie", resources);
	}

	public function Body_YenneferClothesLingerie()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\main_npc\yennefer\body_yennefer__lingerie.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_prostitute\shoes\s_01_wa__novigrad_prostitute.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\main_npc\yennefer\pendant_03_wa__yennefer.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("dlc\ep1\data\characters\models\secondary_npc\shani\i_01_wa__shani.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\items\i_09_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\items\i_08_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("dlc\dlc6\data\characters\models\main_npc\triss\i_01_wa__triss_dlc.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\items\i_31_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\lingerie\legs\l_01a_wa__lingerie_slim.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\skellige_villager_woman\items\i_02_wa__skellige_villager.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\items\i_19_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_prostitute\items\i_06_wa__novigrad_prostitute.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\main_npc\triss\triss_necklace.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\main_npc\triss\corine_torso.w2ent", true));
		IncludeBody("Yennefer Clothes Lingerie", resources);
	}

	public function Body_CiriBodyNude()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\main_npc\ciri\body_03_wa__ciri.w2ent", true));
		IncludeBody("Ciri Body Nude", resources);
	}

	public function Body_KeiraBodyNudeWet()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\main_npc\keira\body_06_wa__body_keira_wet.w2ent", true));
		IncludeBody("Keira Body Nude Wet", resources);
	}

	public function Body_KeiraBodyNudeDry()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\main_npc\keira\body_07_wa__body_keira.w2ent", true));
		IncludeBody("Keira Body Nude Dry", resources);
	}

	public function Body_YenneferBodyNude()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\naked_bodies\torso\twals_01_wa__body.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_prostitute\shoes\s_01_wa__novigrad_prostitute.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\main_npc\yennefer\pendant_03_wa__yennefer.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("dlc\ep1\data\characters\models\secondary_npc\shani\i_01_wa__shani.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\items\i_09_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\items\i_08_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("dlc\dlc6\data\characters\models\main_npc\triss\i_01_wa__triss_dlc.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\items\i_31_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\skellige_villager_woman\items\i_02_wa__skellige_villager.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\items\i_19_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_prostitute\items\i_06_wa__novigrad_prostitute.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\main_npc\triss\triss_necklace.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\main_npc\triss\corine_torso.w2ent", true));
		IncludeBody("Yennefer Body Nude", resources);
	}

	public function Body_TrissBodyNude()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\naked_bodies\torso\twals_02_wa__body.w2ent", true));
		IncludeBody("Triss Body Nude", resources);
	}

	public function Body_BrideClothes()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\body\ag_01_wa__body.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\body\ls_04_wa__body.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("dlc\ep1\data\characters\models\secondary_npc\bride\t2_01_wa__bride.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("dlc\ep1\data\characters\models\secondary_npc\bride\d_01_wa__bride.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("dlc\ep1\data\characters\models\secondary_npc\bride\a_01_wa__bride.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("dlc\ep1\data\characters\models\secondary_npc\bride\i_02_wa__bride.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("dlc\ep1\data\characters\models\secondary_npc\bride\i_01_wa__bride.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("dlc\ep1\data\characters\models\secondary_npc\bride\i_03_wa__bride.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\shoes\s_01_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("dlc\ep1\data\characters\models\secondary_npc\bride\i_01_wa__bride.w2ent", true));
		IncludeBody("Bride Clothes ", resources);
	}

	public function Body_BrideVillagerClothesRed()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\body\ag_01_wa__body.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\skellige_villager_woman\dress\d_02_wa__old_skellige_villager.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\skellige_villager_woman\arms\a_06_wa__skellige_villager.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\body\ls_04_wa__body.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\skellige_villager_woman\shoes\s_03_wa__skellige_villager.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\skellige_villager_woman\torso\t3_04_wa__skellige_villager.w2ent", true));
		IncludeBody("Bride Villager Clothes (Red) ", resources);
	}

	public function Body_CerysClothesCasualRed()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\main_npc\cerys\t3d_01_wa__cerys.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\main_npc\cerys\i_01_wa__cerys.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\main_npc\cerys\i_02_wa__cerys.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\main_npc\cerys\fur_01_wa__cerys.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\main_npc\cerys\a_01_wa__cerys.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\body\l_01_wa__body.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\shoes\s_05_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\skellige_villager_woman\items\i_04_wa__skellige_villager.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\skellige_warrior_woman\arms\ag_02_wa__skellige_warrior_woman.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("items\remains\remains__human_01\remains_human_inv.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\body\g_01_wa__body.w2ent", true));
		IncludeBody("Cerys Clothes Casual (Red) ", resources);
	}

	public function Body_EdnaRosaClothesDressRed()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\secondary_npc\rosa\t3_08_wa__rosa.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\shoes\s_01_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_sorceress_woman\arms\a_04_wa__novigrad_sorceress.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\items\i_24_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\items\i_14_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\items\i_15_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\secondary_npc\rosa\d_01_wa__rosa.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_sorceress_woman\items\i_10_wa__novigrad_sorceress.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\body\g_01_wa__body.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\body\a0_03_wa__body.w2ent", true));
		IncludeBody("Edna Rosa Clothes Dress (Red)", resources);
	}

	public function Body_RosaClothesCasualRed()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\skellige_warrior_woman\torso\t3d_02_wa__skellige_warrior_woman.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\shoes\s_05_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\items\i_08_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\items\i_10_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\legs\l2_06_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\body\a2g_02_wa__body.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\skellige_warrior_woman\arms\a_01_wa__skellige_warrior_woman.w2ent", true));
		IncludeBody("Rosa Clothes Casual (Red)", resources);
	}

	public function Body_FeliciaClothesCasualRed()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\body\a2g_01_wa__body.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_sorceress_woman\dress\d_01_wa__novigrad_sorceress.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_sorceress_woman\torso\t_02_wa__novigrad_sorceress.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_sorceress_woman\items\i_02_wa__novigrad_sorceress.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_sorceress_woman\items\i_06_wa__novigrad_sorceress.w2ent", true));
		IncludeBody("Felicia Clothes Casual (Red)", resources);
	}

	public function Body_IrinaClothesDressRed()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_sorceress_woman\dress\d_01_wa__novigrad_sorceress.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_sorceress_woman\torso\t_01_wa__novigrad_sorceress.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\arms\a_08_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\body\a2g_01_wa__body.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\body\l_01_wa__body.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_prostitute\shoes\s_01_wa__novigrad_prostitute.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\skellige_villager_woman\items\i_03_wa__skellige_villager.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_sorceress_woman\items\i_10_wa__novigrad_sorceress.w2ent", true));
		IncludeBody("Irina Clothes Dress (Red)", resources);
	}

	public function Body_IrinaClothesCasualRed()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\torso\t3_08_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\arms\a_06_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\shoes\s_05_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\body\g_01_wa__body.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\legs\l2_05_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\secondary_npc\irina\d_03_wa__irina_renarde.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\items\i_02_wa__novigrad_citizen.w2ent", true));
		IncludeBody("Irina Clothes Casual (Red)", resources);
	}

	public function Body_MargarittaClothesDress()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\secondary_npc\margaritta\d_01_wa__margaritta.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\secondary_npc\margaritta\body_01_wa__margaritta.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\items\i_04_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\items\i_08_wa__novigrad_citizen.w2ent", true));
		IncludeBody("Margaritta Clothes Dress", resources);
	}

	public function Body_PriscillaClothesSceneRed()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\body\g_01_wa__body.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\arms\a_04_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\body\a2_01_wa__body.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\skellige_villager_woman\dress\d_07_wa__skellige_villager.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\torso\t2_01_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\shoes\s_02_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\items\i_08_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\body\ls_03_wa__body.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\body\a0_03_wa__body.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\items\i_11_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\items\i_16_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\items\i_23_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\items\i_07_wa__novigrad_citizen.w2ent", true));
		IncludeBody("Priscilla Clothes Scene (Red)", resources);
	}

	public function Body_PriscillaClothesCasualRed()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\body\g_01_wa__body.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\body\n_01_wa__body.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\arms\a_01_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\items\i_01_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\shoes\s_01_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\torso\t2_01_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\secondary_npc\priscilla\l1_01_wa__priscilla.w2ent", true));
		IncludeBody("Priscilla Clothes Casual (Red)", resources);
	}

	public function Body_VespulaClothesCasualRed()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\dress\d_02_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\body\l_01_wa__body.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\shoes\s_01_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\torso\t3_07_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\arms\a_07_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\body\ag_01_wa__body.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\skellige_villager_woman\items\i_02_wa__skellige_villager.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\skellige_villager_woman\items\i_04_wa__skellige_villager.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\skellige_villager_woman\items\i_03_wa__skellige_villager.w2ent", true));
		IncludeBody("Vespula Clothes Casual (Red)", resources);
	}

	public function Body_EvelineClothesCasual()
	{
		resources.PushBack((CEntityTemplate)LoadResource("dlc\ep1\data\characters\models\secondary_npc\eveline_gallo\t2_01_wa__eveline_gallo.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\shoes\s_05_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_sorceress_woman\arms\a_01_wa__novigrad_sorceress.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\items\i_35_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("dlc\ep1\data\characters\models\secondary_npc\eveline_gallo\i_01_wa__eveline_gallo.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("dlc\ep1\data\characters\models\secondary_npc\eveline_gallo\i_02_wa__eveline_gallo.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\body\a2g_01_wa__body.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\legs\l2_05_wa__novigrad_citizen.w2ent", true));
		IncludeBody("Eveline Clothes Casual", resources);
	}

	public function Body_CorineClothesCasual()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\secondary_npc\corine_dreamer\body_01_wa__corine.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\body\s_01_wa__body.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_prostitute\legs\l2_02_wa__novigrad_prostitute.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\secondary_npc\corine_dreamer\d_01_wa__corine.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\body\l_01_wa__body.w2ent", true));
		IncludeBody("Corine Clothes Casual", resources);
	}

	public function Body_YenneferClothesLingerieTopWithoutPantieswSandals()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\main_npc\yennefer\body_yennefer__lingerie.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_prostitute\shoes\s_01_wa__novigrad_prostitute.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\main_npc\yennefer\pendant_03_wa__yennefer.w2ent", true));
		IncludeBody("Yennefer Clothes Lingerie (Top Without Panties) w Sandals", resources);
	}

	public function Body_YenneferClothesLingeriePantiesWithoutTopwSandals()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\naked_bodies\torso\twals_01_wa__body.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_prostitute\shoes\s_01_wa__novigrad_prostitute.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("dlc\ep1\data\characters\models\secondary_npc\shani\i_01_wa__shani.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\items\i_09_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\items\i_08_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\items\i_31_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\skellige_villager_woman\items\i_02_wa__skellige_villager.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\items\i_19_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_prostitute\items\i_06_wa__novigrad_prostitute.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\main_npc\triss\triss_necklace.w2ent", true));
		IncludeBody("Yennefer Clothes Lingerie (Panties Without Top) w Sandals", resources);
	}

	public function Body_ProstituteClothes1Red()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\lingerie\legs\l_09_wa__lingerie_twals03.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\naked_bodies\torso\twals_02_wa__body.w2ent", true));
		IncludeBody("Prostitute Clothes 1 (Red)", resources);
	}

	public function Body_ProstituteClothes2Red()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\body\ls_02_wa__body_barefoot.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_prostitute\dress\d_03_wa__novigrad_prostitute.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\torso\t0a_01_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\skellige_villager_woman\items\i_03_wa__skellige_villager.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\body\w_01_wa__body.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\torso\t0a_01_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_prostitute\dress\d_02_wa__novigrad_prostitute_px.w2ent", true));
		IncludeBody("Prostitute Clothes 2 (Red)", resources);
	}

	public function Body_ProstituteClothes3PartiallyRed()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_prostitute\items\i_02_wa__novigrad_prostitute.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_prostitute\items\i_07_wa__novigrad_prostitute.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\body\twa0_01_wa__body.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\body\a2g_01_wa__body.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\lingerie\legs\l_01a_wa__lingerie.w2ent", true));
		IncludeBody("Prostitute Clothes 3 (Partially Red)", resources);
	}

	public function Body_ProstituteClothes4Red()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_prostitute\items\i_01_wa__novigrad_prostitute.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\torso\t3a_04_wa__novigrad_citizen_clean.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_prostitute\legs\l2_02_wa__novigrad_prostitute.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_prostitute\dress\d_02_wa__novigrad_prostitute_px.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\body\w_01_wa__body.w2ent", true));
		IncludeBody("Prostitute Clothes 4 (Red)", resources);
	}

	public function Body_ProstituteClothes5Red()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_prostitute\items\i_07_wa__novigrad_prostitute.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_prostitute\torso\t_04_wa__novigrad_prostitute.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\lingerie\legs\l_02_wa__lingerie.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_prostitute\items\i_02_wa__novigrad_prostitute_clean.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\items\i_09_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\items\i_08_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_prostitute\shoes\s_02_wa__novigrad_prostitute.w2ent", true));
		IncludeBody("Prostitute Clothes 5 (Red)", resources);
	}

	public function Body_ProstituteClothes6Red()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\body\a2g_01_wa__body.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_prostitute\torso\t2_01_wa__novigrad_prostitute.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_prostitute\arms\a_01_wa__novigrad_prostitute.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_prostitute\legs\l2_02_wa__novigrad_prostitute.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_prostitute\items\i_01_wa__novigrad_prostitute.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\items\i_04_wa__novigrad_citizen.w2ent", true));
		IncludeBody("Prostitute Clothes 6 (Red)", resources);
	}

	public function Body_ProstituteClothes7Red()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_prostitute\shoes\s_01_wa__novigrad_prostitute.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\body\l_01_wa__body.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_prostitute\legs\l2_02_wa__novigrad_prostitute.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_prostitute\dress\d_01_wa__novigrad_prostitute.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_prostitute\dress\d_03_wa__novigrad_prostitute.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\lingerie\torso\t_01_wa__lingerie.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_prostitute\arms\a_01_wa__novigrad_prostitute.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\body\a2g_01_wa__body.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_prostitute\items\i_06_wa__novigrad_prostitute.w2ent", true));
		IncludeBody("Prostitute Clothes 7 (Red)", resources);
	}

	public function Body_ProstituteClothes8Red()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\items\i_04_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_prostitute\shoes\s_01_wa__novigrad_prostitute.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\body\l_01_wa__body.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\body\w_01_wa__body.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\body\twa0_01_wa__body.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\body\a2g_01_wa__body.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\lingerie\legs\l_08_wa__lingerie_twals02.w2ent", true));
		IncludeBody("Prostitute Clothes 8 (Red)", resources);
	}

	public function Body_ProstituteClothes9Red()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_prostitute\torso\t_04_wa__novigrad_prostitute.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_sorceress_woman\items\i_11_wa__novigrad_sorceress.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_sorceress_woman\arms\a_01_wa__novigrad_sorceress.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_prostitute\legs\l2_01_wa__novigrad_prostitute.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\body\l_01_wa__body.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\shoes\s_05_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_prostitute\items\i_06_wa__novigrad_prostitute.w2ent", true));
		IncludeBody("Prostitute Clothes 9 (Red)", resources);
	}

	public function Body_ProstituteClothes10Red()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\body\a2g_01_wa__body.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_elf_woman\torso\t_01_wa__novigrad_elf.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\skellige_villager_woman\items\i_02_wa__skellige_villager.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\legs\l2_03_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\body\l_02_wa__old_body.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\shoes\s_02_wa__novigrad_citizen.w2ent", true));
		IncludeBody("Prostitute Clothes 10 (Red)", resources);
	}

	public function Body_ProstituteClothes11Red()
	{
		resources.PushBack((CEntityTemplate)LoadResource("dlc\bob\data\characters\models\crowd_npc\bob_citizen_woman\arms\a_082_wa__bob_woman_noble.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_prostitute\items\i_01_wa__novigrad_prostitute.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_prostitute\items\i_07_wa__novigrad_prostitute.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\body\twa0_01_wa__body.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\body\a2g_01_wa__body.w2ent", true));
		IncludeBody("Prostitute Clothes 11 (Red)", resources);
	}

	public function Body_ProstituteClothes12Red()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\body\a2g_01_wa__body.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\body\twa0_01_wa__body.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_prostitute\items\i_01_wa__novigrad_prostitute.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\main_npc\yennefer\pendant_03_wa__yennefer.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\items\i_08_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\items\i_31_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\skellige_villager_woman\items\i_02_wa__skellige_villager.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\items\i_19_wa__novigrad_citizen.w2ent", true));
		IncludeBody("Prostitute Clothes 12 (Red)", resources);
	}

	public function Body_IrismotherClothesCasualRed()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\skellige_villager_woman\dress\d_01_wa__skellige_villager.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\arms\a_11_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\body\g_01_wa__old_body.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("dlc\ep1\data\characters\models\secondary_npc\iris's_mother\t4_02_wa__iriss_mother.w2ent", true));
		IncludeBody("Irismother Clothes Casual (Red)", resources);
	}

	public function Body_NoblemansdaughterClothesCasualRed()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\body\a0_03_wa__body.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\common\woman_average\body\a2g_01_wa__body.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\arms\a_04_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\dress\d_01_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\shoes\s_02_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\torso\t2_01_wa__novigrad_citizen.w2ent", true));
		IncludeBody("Noblemansdaughter Clothes Casual (Red)", resources);
	}

	public function Body_NudebijouterieTrissAnnaHenriettaothermix()
	{
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\naked_bodies\torso\twals_01_wa__body.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_prostitute\shoes\s_01_wa__novigrad_prostitute.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("dlc\ep1\data\characters\models\secondary_npc\shani\i_01_wa__shani.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\items\i_09_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\items\i_08_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\items\i_31_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\skellige_villager_woman\items\i_02_wa__skellige_villager.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_citizen_woman\items\i_19_wa__novigrad_citizen.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\crowd_npc\novigrad_prostitute\items\i_06_wa__novigrad_prostitute.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\main_npc\triss\triss_necklace.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("dlc\bob\data\characters\models\main_npc\anna_henrietta\earrings_01_wa__anna_henrietta.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("dlc\bob\data\characters\models\main_npc\anna_henrietta\hairpin_01_wa__anna_henrietta.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("dlc\bob\data\characters\models\main_npc\anna_henrietta\crown_01_wa__anna_henrietta.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("characters\models\main_npc\triss\NeckringHenrietta.w2ent", true));
		IncludeBody("Nude bijouterie (Triss Anna Henrietta  other mix)", resources);
	}

	public function Body_AnnaHenriettapantscorset()
	{
		resources.PushBack((CEntityTemplate)LoadResource("dlc\bob\data\characters\models\main_npc\anna_henrietta\body_02_wa__anna_henrietta.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("dlc\bob\data\characters\models\main_npc\anna_henrietta\earrings_01_wa__anna_henrietta.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("dlc\bob\data\characters\models\main_npc\anna_henrietta\hairpin_01_wa__anna_henrietta.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("dlc\bob\data\characters\models\main_npc\anna_henrietta\crown_01_wa__anna_henrietta.w2ent", true));
		IncludeBody("Anna Henrietta pants corset ", resources);
	}

	public function Body_AnnaHenriettatravel()
	{
		resources.PushBack((CEntityTemplate)LoadResource("dlc\bob\data\characters\models\main_npc\anna_henrietta\body_03_wa__anna_henrietta_travel.w2ent", true));
		IncludeBody("Anna Henrietta travel", resources);
	}

	public function Body_Syannacasualribbon()
	{
		resources.PushBack((CEntityTemplate)LoadResource("dlc\bob\data\characters\models\main_npc\syanna\t_01__syanna.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("dlc\bob\data\characters\models\main_npc\syanna\i_05_wa__syanna.w2ent", true));
		resources.PushBack((CEntityTemplate)LoadResource("dlc\bob\data\characters\models\main_npc\syanna\t_01__syanna_px.w2ent", true));
		IncludeBody("Syanna casual ribbon", resources);
	}

	public function Body_Syannanaked()
	{
		resources.PushBack((CEntityTemplate)LoadResource("dlc\bob\data\characters\models\main_npc\syanna\body_01_wa__syanna.w2ent", true));
		IncludeBody("Syanna naked", resources);
	}
}