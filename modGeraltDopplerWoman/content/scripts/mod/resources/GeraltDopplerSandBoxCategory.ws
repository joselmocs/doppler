class GeraltDopplerSandBoxCategory
{
	public var resources : GeraltDopplerResources;
	public var doppler : GeraltDoppler;
	public var categoryName: string;
	public var list: array<CEntityTemplate>;
	public var names: array<string>;
	public var currents: array<int>;
	public var beginsWith: array<string>;
	public var slotsNames : array<name>;
	public var count : int;
	public var slotsCount : int;
	public var favorites : array<array<name>>;
	public var favoriteSlots : array<array<int>>;
	public var categoryConfigName : name;

	public function Init(_categoryName: string, _beginsWith: array<string>, _slotsNames: array<name>, isMisc : bool)
	{
		var i : int;
		doppler = GetGeraltDoppler();
		resources = GetGeraltDopplerResources();
		categoryName = _categoryName;
		beginsWith = _beginsWith;
		slotsCount = _slotsNames.Size();
		slotsNames = _slotsNames;

		categoryConfigName = 'Hidden';

		for (i = 0; i < slotsCount; i += 1) {
			currents.PushBack(0);
		}

		list.PushBack(resources.SandBox.list[0]);
		names.PushBack(resources.SandBox.names[0]);

		if ( isMisc ) {
			CheckMissingParts();
		}
		else {
			LoadPartsFromSandBox();
		}

		count = list.Size();
	}

	public function SaveCurrentToSlot(index: int)
	{
		var i : int;

		doppler = GetGeraltDoppler();

		for (i = 0; i < slotsCount; i += 1)
		{
			favoriteSlots[index][i] = currents[i];
			doppler.configWrapper.SetVarValue( categoryConfigName, favorites[ index ][ i ], IntToString( currents[i] ) );
		}
	}

	public function LoadSlotToCurrent(index: int)
	{
		var i : int;

		for (i = 0; i < slotsCount; i += 1) {
			currents[i] = favoriteSlots[index][i];
			SaveToConfig(i);
		}
	}

	public function AddFavorites(favoriteNames : array<name>)
	{
		var i : int;
		var favSlots : array<int>;

		for (i = 0; i < favoriteNames.Size(); i += 1) {
			favSlots.PushBack( StringToInt(theGame.GetInGameConfigWrapper().GetVarValue( categoryConfigName, favoriteNames[i] )) );
		}

		favoriteSlots.PushBack(favSlots);
		favorites.PushBack(favoriteNames);
	}

	public function UnloadAll()
	{
		var i : int;
		var res : array<CEntityTemplate>;

		for (i = 0; i < slotsCount; i += 1) {
			if (currents[i] == 0) {
				continue;
			}
			
			res.Clear();
			res.PushBack(list[currents[i]]);
			resources.UnloadResources(res);
		}
	}

	public function LoadAll()
	{
		var i : int;
		var res : array<CEntityTemplate>;

		for (i = 0; i < slotsCount; i += 1) {
			if (currents[i] == 0) {
				continue;
			}
			
			res.Clear();
			res.PushBack(list[currents[i]]);
			resources.UnloadResources(res);
			resources.LoadResources(res);
		}
	}

	public function CheckMissingParts()
	{
		var i, c, n, totalNames, totalParts, catSize : int;
		var found : bool;

		totalParts = resources.SandBox.list.Size();
		catSize = resources.SandBox.categories.Size();

		for (i = 0; i < totalParts; i += 1)
		{
			found = false;
			for (c = 0; c < catSize; c += 1)
			{
				totalNames = resources.SandBox.categories[c].names.Size();
				for (n = 0; n < totalNames; n += 1)
				{
					if ( resources.SandBox.categories[c].names[n] == resources.SandBox.names[i] ) {
						found = true;
						break;
					}
				}

				if ( found ) {
					break;
				}
			}


			if ( !found ) {
				list.PushBack(resources.SandBox.list[i]);
				names.PushBack(resources.SandBox.names[i]);
			}
		}
	}

	public function LoadPartsFromSandBox()
	{
		var i, bw, totalBegins, totalParts : int;

		totalParts = resources.SandBox.list.Size();

		for (i = 1; i < totalParts; i += 1)
		{
			for (bw = 0; bw < beginsWith.Size(); bw += 1)
			{
				if (StrBeginsWith(resources.SandBox.names[i], beginsWith[bw])) {
					list.PushBack(resources.SandBox.list[i]);
					names.PushBack(resources.SandBox.names[i]);
					break;
				}
			}
		}
	}

	public function GetNameFromIndex(currentIndex: int) : string
	{
		return names[currents[currentIndex]];
	}

	public function UnloadPartFromIndex(index : int) {
		var res : array<CEntityTemplate>;

		if (ExitsInAnotherCurrent(index)) {
			return;
		}

		res.PushBack(list[currents[index]]);

		resources.UnloadResources(res);
	}

	public function SetNext(index : int)
	{
		SetPart(index, true, false);
	}

	public function SetPrevious(index : int)
	{
		SetPart(index, false, true);
	}

	public function SetPart(index : int, next: bool, previous : bool)
	{
		var res : array<CEntityTemplate>;

		if ( index >= 0 ) {
			UnloadPartFromIndex(index);
		}

		if ( next ) {
			currents[index] = currents[index] + 1;

			if ( currents[index] >= count ) {
				currents[index] = 0;
			}
		}

		if ( previous ) {
			if ( currents[index] == 0 ) {
				currents[index] = count - 1;
			}
			else {
				currents[index] = currents[index] - 1;
			}
		}

		if ( currents[index] >= 0 && currents[index] < count ) {
			SaveToConfig(index);

			if (ExitsInAnotherCurrent(index)) {
				return;
			}
			
			res.PushBack(list[currents[index]]);
			resources.LoadResources(res);
		}
	}

	public function LoadFromConfig()
	{
		var i : int;
		for (i = 0; i < slotsCount; i += 1) {
			currents[i] = StringToInt(GetGeraltDoppler().configWrapper.GetVarValue(resources.CFG_CURRENT, slotsNames[i]));
		}
	}

	public function SaveToConfig(index : int)
	{
		GetGeraltDoppler().configWrapper.SetVarValue(resources.CFG_CURRENT, slotsNames[index], IntToString(currents[index]));
	}

	public function ExitsInAnotherCurrent(index : int) : bool
	{
		var i, currentSize : int;
		var alreadLoaded : bool;

		currentSize = currents.Size();
		for (i = 0; i < currentSize; i += 1) {
			if (i == index) {
				continue;
			}

			if (currents[index] == currents[i]) {
				alreadLoaded = true;
				break;
			}
		}

		return alreadLoaded;
	}
}
