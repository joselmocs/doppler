class GeraltDopplerSandBox
{
	public var doppler : GeraltDoppler;
	public var categories : array<GeraltDopplerSandBoxCategory>;
	public var list : array<CEntityTemplate>;
	public var names : array<string>;
	public var categoryCount : int;
	public var count : int;
	public var favoriteSlotsCount : int;
	public var categoryConfigName : name;
	public var slotsNames : array<array<name>>;
	public var slots : array<array<int>>;

	public function LoadParts_1()
	{
		AddPart("Empty", "");
		
		//[body]
		AddPart("body_05_wa__anna_henrietta_black","dlc\bob\data\characters\models\main_npc\anna_henrietta\body_05_wa__anna_henrietta_black.w2ent");
		AddPart("body_01_wa__shani","dlc\ep1\data\characters\models\secondary_npc\shani\b_01_wa__shani.w2ent");
		AddPart("body_01_wa__syanna","dlc\bob\data\characters\models\main_npc\syanna\body_01_wa__syanna.w2ent");
		AddPart("body_wa__sq701_forest_nymph","dlc\bob\data\characters\models\secondary_npc\sq701_forest_nymph\body_wa__sq701_forest_nymph.w2ent");
		AddPart("body_n_04_wa__old_body","characters\models\common\woman_average\body\n_04_wa__old_body.w2ent");
		AddPart("body_n_05_wa__body","characters\models\common\woman_average\body\n_05_wa__body.w2ent");
		AddPart("body_twa0_02_wa__body","dlc\bob\data\characters\models\common\woman_average\body\twa0_02_wa__body.w2ent");
		AddPart("body_01_wa__triss","characters\models\main_npc\triss\body_01_wa__triss.w2ent");
		AddPart("body_01_wa__triss_hooded_01","characters\models\main_npc\triss\body_01_wa__triss_hooded_01.w2ent");
		AddPart("body_01_wa__triss_hooded_02","characters\models\main_npc\triss\body_01_wa__triss_hooded_02.w2ent");
		AddPart("body_01_wa__triss_tortured","characters\models\main_npc\triss\body_01_wa__triss_tortured.w2ent");
		AddPart("body_02_wa__triss","characters\models\main_npc\triss\body_02_wa__triss.w2ent");
		AddPart("body_03_wa__triss","characters\models\main_npc\triss\body_03_wa__triss.w2ent");
		AddPart("body_01_wa__ciri_fur","characters\models\main_npc\ciri\body_01_wa__ciri_fur.w2ent");
		AddPart("body_01_wa__ciri_bandaged","characters\models\main_npc\ciri\body_01_wa__ciri_bandaged.w2ent");
		AddPart("body_01_wa__ciri","characters\models\main_npc\ciri\body_01_wa__ciri.w2ent");
		AddPart("body_01_wa__ciri_bandaged_naked","characters\models\main_npc\ciri\body_01_wa__ciri_bandaged_naked.w2ent");
		AddPart("body_01_wa__ciri_dirty","characters\models\main_npc\ciri\body_01_wa__ciri_dirty.w2ent");
		AddPart("body_01_wa__ciri_hooded_02","characters\models\main_npc\ciri\body_01_wa__ciri_hooded_02.w2ent");
		AddPart("body_03_wa__ciri","characters\models\main_npc\ciri\body_03_wa__ciri.w2ent");
		AddPart("body_04_wa__ciri","characters\models\main_npc\ciri\body_04_wa__ciri.w2ent");
		AddPart("body_01_wa__nml_keira","characters\models\main_npc\keira\body_01_wa__nml_keira.w2ent");
		AddPart("body_03_wa__nml_keira","characters\models\main_npc\keira\body_03_wa__nml_keira.w2ent");
		AddPart("body_05_wa__nml_keira","characters\models\main_npc\keira\body_05_wa__nml_keira.w2ent");
		AddPart("body_06_wa__body_keira_wet","characters\models\main_npc\keira\body_06_wa__body_keira_wet.w2ent");
		AddPart("body_07_wa__body_keira","characters\models\main_npc\keira\body_07_wa__body_keira.w2ent");
		AddPart("body_01__sheala","characters\models\main_npc\sheala\body_01__sheala.w2ent");
		AddPart("body_yennefer__lingerie","characters\models\main_npc\yennefer\body_yennefer__lingerie.w2ent");
		AddPart("body_twals_01_wa__body_notcensored","characters\models\main_npc\yennefer\twals_01_wa__body_notcensored.w2ent");
		AddPart("body_b_01_wa__yennefer","characters\models\main_npc\yennefer\b_01_wa__yennefer.w2ent");
		AddPart("body_b_01_wa__yennefer_hooded_02","characters\models\main_npc\yennefer\b_01_wa__yennefer_hooded_02.w2ent");
		AddPart("body_b_03_wa_yennefer","characters\models\main_npc\yennefer\b_03_wa_yennefer.w2ent");
		AddPart("body_01_wa__ves","characters\models\secondary_npc\ves\body_01_wa__ves.w2ent");
		AddPart("body_01_wa__anabelle","characters\models\secondary_npc\anabelle\body_01_wa__anabelle.w2ent");
		AddPart("body_b_02_wa__shani","dlc\ep1\data\characters\models\secondary_npc\shani\b_02_wa__shani.w2ent");
		AddPart("body_01_wa__tamara","characters\models\secondary_npc\tamara\body_01_wa__tamara.w2ent");
		AddPart("body_01_wa__lady_bran","characters\models\secondary_npc\lady_bran\body_01_wa__lady_bran.w2ent");
		AddPart("body_01__filippa","characters\models\secondary_npc\filippa\body_01__filippa.w2ent");
		AddPart("body_02_wa__anna","characters\models\secondary_npc\anna\body_02_wa__anna.w2ent");
		AddPart("body_01_wa__iris","dlc\ep1\data\characters\models\main_npc\iris\body_01_wa__iris.w2ent");
		AddPart("body_03_wa__iris","dlc\ep1\data\characters\models\main_npc\iris\body_03_wa__iris.w2ent");
		AddPart("body_05__yennefer","dlc\dlc4\data\characters\models\main_npc\yennefer\body_05__yennefer.w2ent");
		AddPart("body_05__yennefer_travel","dlc\dlc4\data\characters\models\main_npc\yennefer\body_05__yennefer_travel.w2ent");
		AddPart("body_05__yennefer_hooded_02","dlc\dlc4\data\characters\models\main_npc\yennefer\body_05__yennefer_hooded_02.w2ent");
		AddPart("body_05__yennefer_hooded","dlc\dlc4\data\characters\models\main_npc\yennefer\body_05__yennefer_hooded.w2ent");
		AddPart("body_b_01_wa__triss_dlc","dlc\dlc6\data\characters\models\main_npc\triss\b_01_wa__triss_dlc.w2ent");
		AddPart("body_b_01_wa__triss_dlc_hooded_01","dlc\dlc6\data\characters\models\main_npc\triss\b_01_wa__triss_dlc_hooded_01.w2ent");
		AddPart("body_b_01_wa__triss_dlc_hooded_02","dlc\dlc6\data\characters\models\main_npc\triss\b_01_wa__triss_dlc_hooded_02.w2ent");
		AddPart("body_02_wa__triss_dlc","dlc\dlc6\data\characters\models\main_npc\triss\body_02_wa__triss_dlc.w2ent");
		AddPart("body_01_wa__ciri_dlc_dirty","dlc\dlc11\data\characters\models\main_npc\ciri\body_01_wa__ciri_dlc_dirty.w2ent");
		AddPart("body_01_wa__ciri_dlc_fur","dlc\dlc11\data\characters\models\main_npc\ciri\body_01_wa__ciri_dlc_fur.w2ent");
		AddPart("body_01_wa__ciri_dlc_hooded","dlc\dlc11\data\characters\models\main_npc\ciri\body_01_wa__ciri_dlc_hooded.w2ent");
		AddPart("body_01_wa__ciri_dlc_hooded_02","dlc\dlc11\data\characters\models\main_npc\ciri\body_01_wa__ciri_dlc_hooded_02.w2ent");
		AddPart("body_01_wa__ciri_dlc_wounded","dlc\dlc11\data\characters\models\main_npc\ciri\body_01_wa__ciri_dlc_wounded.w2ent");
		AddPart("body_twals_02_wa__body","characters\models\crowd_npc\naked_bodies\torso\twals_02_wa__body.w2ent");
		AddPart("body_twals_01_wa__body","characters\models\crowd_npc\naked_bodies\torso\twals_01_wa__body.w2ent");
		AddPart("body_01_wa__margaritta","characters\models\secondary_npc\margaritta\body_01_wa__margaritta.w2ent");
		AddPart("body_n_01_wa__body","characters\models\common\woman_average\body\n_01_wa__body.w2ent");
		AddPart("body_01_wa__corine","characters\models\secondary_npc\corine_dreamer\body_01_wa__corine.w2ent");
		AddPart("body_w_01_wa__body","characters\models\common\woman_average\body\w_01_wa__body.w2ent");
		AddPart("body_twa0_01_wa__body","characters\models\common\woman_average\body\twa0_01_wa__body.w2ent");
		AddPart("body_02_wa__anna_henrietta","dlc\bob\data\characters\models\main_npc\anna_henrietta\body_02_wa__anna_henrietta.w2ent");
		AddPart("body_03_wa__anna_henrietta_travel","dlc\bob\data\characters\models\main_npc\anna_henrietta\body_03_wa__anna_henrietta_travel.w2ent");
		AddPart("body_t_01_wa_vivienne","dlc\bob\data\characters\models\main_npc\vivienne_de_tabris\t_01_wa_vivienne.w2ent");
		AddPart("body_01_wa__ciri_hooded","characters\models\main_npc\ciri\body_01_wa__ciri_hooded.w2ent");
		AddPart("body_01_wa__ciri_wounded","characters\models\main_npc\ciri\body_01_wa__ciri_wounded.w2ent");
		AddPart("body_t_01_wa__succubus","characters\models\crowd_npc\succubus\torso\t_01_wa__succubus.w2ent");
		AddPart("body_t_01__syanna","dlc\bob\data\characters\models\main_npc\syanna\t_01__syanna.w2ent");
		AddPart("body_t_01__syanna_hood","dlc\bob\data\characters\models\main_npc\syanna\t_01__syanna_hood.w2ent");
		AddPart("body_twals_02_wa__body","characters\models\crowd_npc\naked_bodies\torso\twals_02_wa__body.w2ent");

		//[legs]
		AddPart("legs_l_02_wa__body","characters\models\common\woman_average\body\l_02_wa__body.w2ent");
		AddPart("legs_l_02_wa__body_4skirts","characters\models\common\woman_average\body\l_02_wa__body_4skirts.w2ent");
		AddPart("legs_ls_01_wa__body","characters\models\common\woman_average\body\ls_01_wa__body.w2ent");
		AddPart("legs_ls_02_wa__body_barefoot_4skirts","characters\models\common\woman_average\body\ls_02_wa__body_barefoot_4skirts.w2ent");
		AddPart("legs_l_01_wa__body","characters\models\common\woman_average\body\l_01_wa__body.w2ent");
		AddPart("legs_ls_04_wa__body","characters\models\common\woman_average\body\ls_04_wa__body.w2ent");
		AddPart("legs_ls_03_wa__body","characters\models\common\woman_average\body\ls_03_wa__body.w2ent");
		AddPart("legs_ls_02_wa__body_barefoot","characters\models\common\woman_average\body\ls_02_wa__body_barefoot.w2ent");
		AddPart("legs_l_02_wa__old_body","characters\models\common\woman_average\body\l_02_wa__old_body.w2ent");
		AddPart("legs_l_03_wa__lingerie","characters\models\crowd_npc\lingerie\legs\l_03_wa__lingerie.w2ent");
		AddPart("legs_l_05_wa__lingerie_old","characters\models\crowd_npc\lingerie\legs\l_05_wa__lingerie_old.w2ent");
		AddPart("legs_l_01_wa__lingerie","characters\models\crowd_npc\lingerie\legs\l_01_wa__lingerie.w2ent");
		AddPart("legs_l_04_wa__lingerie","characters\models\crowd_npc\lingerie\legs\l_04_wa__lingerie.w2ent");
		AddPart("legs_l_06_wa__lingerie_slim","characters\models\crowd_npc\lingerie\legs\l_06_wa__lingerie_slim.w2ent");
		AddPart("legs_l2_04_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\legs\l2_04_wa__novigrad_citizen.w2ent");
		AddPart("legs_l1_01_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\legs\l1_01_wa__novigrad_citizen.w2ent");
		AddPart("legs_l_01_wa__novigrad_elf","characters\models\crowd_npc\novigrad_elf_woman\legs\l_01_wa__novigrad_elf.w2ent");
		AddPart("legs_i_01_wa__novigrad_prostitute","characters\models\crowd_npc\novigrad_prostitute\items\i_01_wa__novigrad_prostitute.w2ent");
		AddPart("legs_l_01a_wa__lingerie_slim","characters\models\crowd_npc\lingerie\legs\l_01a_wa__lingerie_slim.w2ent");
		AddPart("legs_l_02_wa__yennefer","characters\models\main_npc\yennefer\l_02_wa__yennefer.w2ent");
		AddPart("legs_l2_05_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\legs\l2_05_wa__novigrad_citizen.w2ent");
		AddPart("legs_l1_01_wa__priscilla","characters\models\secondary_npc\priscilla\l1_01_wa__priscilla.w2ent");
		AddPart("legs_l2_02_wa__novigrad_prostitute","characters\models\crowd_npc\novigrad_prostitute\legs\l2_02_wa__novigrad_prostitute.w2ent");
		AddPart("legs_l_09_wa__lingerie_twals03","characters\models\crowd_npc\lingerie\legs\l_09_wa__lingerie_twals03.w2ent");
		AddPart("legs_l_01a_wa__lingerie","characters\models\crowd_npc\lingerie\legs\l_01a_wa__lingerie.w2ent");
		AddPart("legs_l_02_wa__lingerie","characters\models\crowd_npc\lingerie\legs\l_02_wa__lingerie.w2ent");
		AddPart("legs_l_08_wa__lingerie_twals02","characters\models\crowd_npc\lingerie\legs\l_08_wa__lingerie_twals02.w2ent");
		AddPart("legs_l2_01_wa__novigrad_prostitute","characters\models\crowd_npc\novigrad_prostitute\legs\l2_01_wa__novigrad_prostitute.w2ent");
		AddPart("legs_bl_01_wa__olgierds_gang_member_woman","dlc\ep1\data\characters\models\crowd_npc\olgierds_gang_member_woman\legs\l_01_wa__olgierds_gang_member_woman.w2ent");
		AddPart("legs_l_i_01_wa__novigrad_prostitute","characters\models\crowd_npc\novigrad_prostitute\items\i_01_wa__novigrad_prostitute.w2ent");

		//[shoes]
		AddPart("shoes_s_01_wa__body","characters\models\common\woman_average\body\s_01_wa__body.w2ent");
		AddPart("shoes_s_03_wa__bob","dlc\bob\data\characters\models\crowd_npc\bob_citizen_woman\shoes\s_03_wa__bob.w2ent");
		AddPart("shoes_s_01_wa__bob","dlc\bob\data\characters\models\crowd_npc\bob_citizen_woman\shoes\s_01_wa__bob.w2ent");
		AddPart("shoes_s_03_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\shoes\s_03_wa__novigrad_citizen.w2ent");
		AddPart("shoes_s_04_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\shoes\s_04_wa__novigrad_citizen.w2ent");
		AddPart("shoes_s_01_wa__skellige_villager","characters\models\crowd_npc\skellige_villager_woman\shoes\s_01_wa__skellige_villager.w2ent");
		AddPart("shoes_s_02_wa__skellige_villager","characters\models\crowd_npc\skellige_villager_woman\shoes\s_02_wa__skellige_villager.w2ent");
		AddPart("shoes_s_02_wa__skellige_villager_4skirts","characters\models\crowd_npc\skellige_villager_woman\shoes\s_02_wa__skellige_villager_4skirts.w2ent");
		AddPart("shoes_s_01_wa__nml_villager","characters\models\crowd_npc\nml_villager_woman\shoes\s_01_wa__nml_villager.w2ent");
		AddPart("shoes_s_02_wa__nml_villager","characters\models\crowd_npc\nml_villager_woman\shoes\s_02_wa__nml_villager.w2ent");
		AddPart("shoes_s_02_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\shoes\s_02_wa__novigrad_citizen.w2ent");
		AddPart("shoes_s_01_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\shoes\s_01_wa__novigrad_citizen.w2ent");
		AddPart("shoes_s_03_wa__skellige_villager","characters\models\crowd_npc\skellige_villager_woman\shoes\s_03_wa__skellige_villager.w2ent");
		AddPart("shoes_s_02_wa__novigrad_prostitute","characters\models\crowd_npc\novigrad_prostitute\shoes\s_02_wa__novigrad_prostitute.w2ent");
		AddPart("shoes_s_05_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\shoes\s_05_wa__novigrad_citizen.w2ent");
		AddPart("shoes_s_01_wa__novigrad_prostitute","characters\models\crowd_npc\novigrad_prostitute\shoes\s_01_wa__novigrad_prostitute.w2ent");

		//[torso]
		AddPart("torso_t2_01_wa__bob_woman_noble","dlc\bob\data\characters\models\crowd_npc\bob_citizen_woman\torso\t2_01_wa__bob_woman_noble.w2ent");
		AddPart("torso_t2_01_wa__bob_woman_noble_p01","dlc\bob\data\characters\models\crowd_npc\bob_citizen_woman\torso\t2_01_wa__bob_woman_noble_p01.w2ent");
		AddPart("torso_t2_02_wa__bob_woman_noble","dlc\bob\data\characters\models\crowd_npc\bob_citizen_woman\torso\t2_02_wa__bob_woman_noble.w2ent");
		AddPart("torso_t2_02_wa__bob_woman_noble_p01","dlc\bob\data\characters\models\crowd_npc\bob_citizen_woman\torso\t2_02_wa__bob_woman_noble_p01.w2ent");
		AddPart("torso_t2_02_wa__bob_woman_noble_p02","dlc\bob\data\characters\models\crowd_npc\bob_citizen_woman\torso\t2_02_wa__bob_woman_noble_p02.w2ent");
		AddPart("torso_t2_03_wa__bob_woman_noble","dlc\bob\data\characters\models\crowd_npc\bob_citizen_woman\torso\t2_03_wa__bob_woman_noble.w2ent");
		AddPart("torso_t2_04_wa__bob_woman_noble","dlc\bob\data\characters\models\crowd_npc\bob_citizen_woman\torso\t2_04_wa__bob_woman_noble.w2ent");
		AddPart("torso_t2_05_wa__bob_woman_noble","dlc\bob\data\characters\models\crowd_npc\bob_citizen_woman\torso\t2_05_wa__bob_woman_noble.w2ent");
		AddPart("torso_t1a_11_wa__bob_woman","dlc\bob\data\characters\models\crowd_npc\bob_citizen_woman\torso\t1a_11_wa__bob_woman.w2ent");
		AddPart("torso_t1a_12_wa__bob_woman","dlc\bob\data\characters\models\crowd_npc\bob_citizen_woman\torso\t1a_12_wa__bob_woman.w2ent");
		AddPart("torso_t_10_wa__bob_woman","dlc\bob\data\characters\models\crowd_npc\bob_citizen_woman\torso\t_10_wa__bob_woman.w2ent");
		AddPart("torso_t3_01_wa__olgierds_gang_member_woman","dlc\ep1\data\characters\models\crowd_npc\olgierds_gang_member_woman\torso\t3_01_wa__olgierds_gang_member_woman.w2ent");
		AddPart("torso_t3_02_wa__olgierds_gang_member_woman","dlc\ep1\data\characters\models\crowd_npc\olgierds_gang_member_woman\torso\t3_02_wa__olgierds_gang_member_woman.w2ent");
		AddPart("torso_t2_01_wa__old_novigrad_citizen01","characters\models\crowd_npc\novigrad_citizen_woman\torso\t2_01_wa__old_novigrad_citizen01.w2ent");
		AddPart("torso_t3_03_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\torso\t3_03_wa__novigrad_citizen.w2ent");
		AddPart("torso_t3_08_wa__old_novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\torso\t3_08_wa__old_novigrad_citizen.w2ent");
		AddPart("torso_t3_10_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\torso\t3_10_wa__novigrad_citizen.w2ent");
		AddPart("torso_t0a_01_wa__novigrad_citizen_wounded","characters\models\crowd_npc\novigrad_citizen_woman\torso\t0a_01_wa__novigrad_citizen_wounded.w2ent");
		AddPart("torso_t3_03_wa__old_novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\torso\t3_03_wa__old_novigrad_citizen.w2ent");
		AddPart("torso_t3_07_wa__novigrad_citizen_p03","characters\models\crowd_npc\novigrad_citizen_woman\torso\t3_07_wa__novigrad_citizen_p03.w2ent");
		AddPart("torso_t3_07_wa__novigrad_citizen_p08","characters\models\crowd_npc\novigrad_citizen_woman\torso\t3_07_wa__novigrad_citizen_p08.w2ent");
		AddPart("torso_t3_07_wa__old_novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\torso\t3_07_wa__old_novigrad_citizen.w2ent");
		AddPart("torso_t3_08_wa__novigrad_citizen_p03","characters\models\crowd_npc\novigrad_citizen_woman\torso\t3_08_wa__novigrad_citizen_p03.w2ent");
		AddPart("torso_t3_09_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\torso\t3_09_wa__novigrad_citizen.w2ent");
		AddPart("torso_t3a_04_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\torso\t3a_04_wa__novigrad_citizen.w2ent");
		AddPart("torso_t3a_06_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\torso\t3a_06_wa__novigrad_citizen.w2ent");
		AddPart("torso_t4_01_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\torso\t4_01_wa__novigrad_citizen.w2ent");
		AddPart("torso_t3_07_wa__old_novigrad_citizen_p01","characters\models\crowd_npc\novigrad_citizen_woman\torso\t3_07_wa__old_novigrad_citizen_p01.w2ent");
		AddPart("torso_t_02_wa__novigrad_elf","characters\models\crowd_npc\novigrad_elf_woman\torso\t_02_wa__novigrad_elf.w2ent");
		AddPart("torso_t_06_wa__novigrad_elf","characters\models\crowd_npc\novigrad_elf_woman\torso\t_06_wa__novigrad_elf.w2ent");
		AddPart("torso_t_03_wa__novigrad_elf","characters\models\crowd_npc\novigrad_elf_woman\torso\t_03_wa__novigrad_elf.w2ent");
		AddPart("torso_t_07_wa__novigrad_elf","characters\models\crowd_npc\novigrad_elf_woman\torso\t_07_wa__novigrad_elf.w2ent");
		AddPart("torso_t1a_03_wa__novigrad_prostitute","characters\models\crowd_npc\novigrad_prostitute\torso\t1a_03_wa__novigrad_prostitute.w2ent");
		AddPart("torso_t2_04_wa__novigrad_prostitute","characters\models\crowd_npc\novigrad_prostitute\torso\t2_04_wa__novigrad_prostitute.w2ent");
		AddPart("torso_t2a_02_wa__novigrad_prostitute","characters\models\crowd_npc\novigrad_prostitute\torso\t2a_02_wa__novigrad_prostitute.w2ent");
		AddPart("torso_t_03_wa__novigrad_prostitute","characters\models\crowd_npc\novigrad_prostitute\torso\t_03_wa__novigrad_prostitute.w2ent");
		AddPart("torso_t2d_04_wa__novigrad_sorceress","characters\models\crowd_npc\novigrad_sorceress_woman\torso\t2d_04_wa__novigrad_sorceress.w2ent");
		AddPart("torso_t_03_wa__novigrad_sorceress","characters\models\crowd_npc\novigrad_sorceress_woman\torso\t_03_wa__novigrad_sorceress.w2ent");
		AddPart("torso_t3_02_wa__skellige_villager","characters\models\crowd_npc\skellige_villager_woman\torso\t3_02_wa__skellige_villager.w2ent");
		AddPart("torso_t3_03_wa__old_skellige_villager","characters\models\crowd_npc\skellige_villager_woman\torso\t3_03_wa__old_skellige_villager.w2ent");
		AddPart("torso_t3_03_wa__skellige_villager","characters\models\crowd_npc\skellige_villager_woman\torso\t3_03_wa__skellige_villager.w2ent");
		AddPart("torso_t3d_04_wa__skellige_villager","characters\models\crowd_npc\skellige_villager_woman\torso\t3d_04_wa__skellige_villager.w2ent");
		AddPart("torso_t4_01_wa__skellige_villager","characters\models\crowd_npc\skellige_villager_woman\torso\t4_01_wa__skellige_villager.w2ent");
		AddPart("torso_t4_02_wa__skellige_villager","characters\models\crowd_npc\skellige_villager_woman\torso\t4_02_wa__skellige_villager.w2ent");
		AddPart("torso_t3_02_wa__old_skellige_villager","characters\models\crowd_npc\skellige_villager_woman\torso\t3_02_wa__old_skellige_villager.w2ent");
		AddPart("torso_t4_01_wa__skellige_villager_p08","characters\models\crowd_npc\skellige_villager_woman\torso\t4_01_wa__skellige_villager_p08.w2ent");
		AddPart("torso_t4_02_wa__skellige_villager_p03","characters\models\crowd_npc\skellige_villager_woman\torso\t4_02_wa__skellige_villager_p03.w2ent");
		AddPart("torso_t4_02_wa__skellige_villager_p08","characters\models\crowd_npc\skellige_villager_woman\torso\t4_02_wa__skellige_villager_p08.w2ent");
		AddPart("torso_t3d_01_wa__skellige_warrior_woman","characters\models\crowd_npc\skellige_warrior_woman\torso\t3d_01_wa__skellige_warrior_woman.w2ent");
		AddPart("torso_t3d_03_wa__skellige_warrior_woman","characters\models\crowd_npc\skellige_warrior_woman\torso\t3d_03_wa__skellige_warrior_woman.w2ent");
		AddPart("torso_t2a_02_wa__nml_villager","characters\models\crowd_npc\nml_villager_woman\torso\t2a_02_wa__nml_villager.w2ent");
		AddPart("torso_t3d_04_wa__novigrad_prostitute","characters\models\crowd_npc\novigrad_prostitute\dress\d_04_wa__novigrad_prostitute.w2ent");
		AddPart("torso_t_02_wa__novigrad_sorceress","characters\models\crowd_npc\novigrad_sorceress_woman\torso\t_02_wa__novigrad_sorceress.w2ent");
		AddPart("torso_t2_01_wa__bride","dlc\ep1\data\characters\models\secondary_npc\bride\t2_01_wa__bride.w2ent");
		AddPart("torso_t3_04_wa__skellige_villager","characters\models\crowd_npc\skellige_villager_woman\torso\t3_04_wa__skellige_villager.w2ent");
		AddPart("torso_t_01_wa__novigrad_sorceress","characters\models\crowd_npc\novigrad_sorceress_woman\torso\t_01_wa__novigrad_sorceress.w2ent");
		AddPart("torso_t3_08_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\torso\t3_08_wa__novigrad_citizen.w2ent");
		AddPart("torso_t2_01_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\torso\t2_01_wa__novigrad_citizen.w2ent");
		AddPart("torso_t3_07_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\torso\t3_07_wa__novigrad_citizen.w2ent");
		AddPart("torso_t3_08_wa__rosa","characters\models\secondary_npc\rosa\t3_08_wa__rosa.w2ent");
		AddPart("torso_t3d_02_wa__skellige_warrior_woman","characters\models\crowd_npc\skellige_warrior_woman\torso\t3d_02_wa__skellige_warrior_woman.w2ent");
		AddPart("torso_t0a_01_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\torso\t0a_01_wa__novigrad_citizen.w2ent");
		AddPart("torso_t3a_04_wa__novigrad_citizen_clean","characters\models\crowd_npc\novigrad_citizen_woman\torso\t3a_04_wa__novigrad_citizen_clean.w2ent");
		AddPart("torso_t_04_wa__novigrad_prostitute","characters\models\crowd_npc\novigrad_prostitute\torso\t_04_wa__novigrad_prostitute.w2ent");
		AddPart("torso_t_01_wa__lingerie","characters\models\crowd_npc\lingerie\torso\t_01_wa__lingerie.w2ent");
		AddPart("torso_t_01_wa__novigrad_elf","characters\models\crowd_npc\novigrad_elf_woman\torso\t_01_wa__novigrad_elf.w2ent");
		AddPart("torso_t4_02_wa__iriss_mother","dlc\ep1\data\characters\models\secondary_npc\iris's_mother\t4_02_wa__iriss_mother.w2ent");
		AddPart("torso_t2_01_wa__novigrad_prostitute","characters\models\crowd_npc\novigrad_prostitute\torso\t2_01_wa__novigrad_prostitute.w2ent");
	}

	public function LoadParts_2()
	{
		//[items]
		AddPart("items_t_01__syanna_px","dlc\bob\data\characters\models\main_npc\syanna\t_01__syanna_px.w2ent");
		AddPart("items_i_03_wa__bob_woman_noble","dlc\bob\data\characters\models\crowd_npc\bob_citizen_woman\items\i_03_wa__bob_woman_noble.w2ent");
		AddPart("items_i_07_wa_bob_woman","dlc\bob\data\characters\models\crowd_npc\bob_citizen_woman\items\i_07_wa_bob_woman.w2ent");
		AddPart("items_i_08_wa__bob_woman_noble","dlc\bob\data\characters\models\crowd_npc\bob_citizen_woman\items\i_08_wa__bob_woman_noble.w2ent");
		AddPart("items_i_08_wa_bob_woman","dlc\bob\data\characters\models\crowd_npc\bob_citizen_woman\items\i_08_wa_bob_woman.w2ent");
		AddPart("items_i_09_wa__bob_woman_noble","dlc\bob\data\characters\models\crowd_npc\bob_citizen_woman\items\i_09_wa__bob_woman_noble.w2ent");
		AddPart("items_i_10_wa_bob_woman","dlc\bob\data\characters\models\crowd_npc\bob_citizen_woman\items\i_10_wa_bob_woman.w2ent");
		AddPart("items_i_12_wa_bob_woman","dlc\bob\data\characters\models\crowd_npc\bob_citizen_woman\items\i_12_wa_bob_woman.w2ent");
		AddPart("items_i_13_wa_bob_woman","dlc\bob\data\characters\models\crowd_npc\bob_citizen_woman\items\i_13_wa_bob_woman.w2ent");
		AddPart("items_i_14_wa__bob_woman_noble","dlc\bob\data\characters\models\crowd_npc\bob_citizen_woman\items\i_14_wa__bob_woman_noble.w2ent");
		AddPart("items_i_05_wa__bob_woman","dlc\bob\data\characters\models\crowd_npc\bob_citizen_woman\items\i_05_wa__bob_woman.w2ent");
		AddPart("items_i_09_wa_bob_woman","dlc\bob\data\characters\models\crowd_npc\bob_citizen_woman\items\i_09_wa_bob_woman.w2ent");
		AddPart("items_i_06_wa_bob_woman","dlc\bob\data\characters\models\crowd_npc\bob_citizen_woman\items\i_06_wa_bob_woman.w2ent");
		AddPart("items_i_11_wa_bob_woman","dlc\bob\data\characters\models\crowd_npc\bob_citizen_woman\items\i_11_wa_bob_woman.w2ent");
		AddPart("items_i_01_wa__sq701_forest_nymph","dlc\bob\data\characters\models\secondary_npc\sq701_forest_nymph\i_01_wa__sq701_forest_nymph.w2ent");
		AddPart("items_i_01_wa__olgierds_gang_member_woman","dlc\ep1\data\characters\models\crowd_npc\olgierds_gang_member_woman\items\i_01_wa__olgierds_gang_member_woman.w2ent");
		AddPart("items_i_02_wa__olgierds_gang_member_woman","dlc\ep1\data\characters\models\crowd_npc\olgierds_gang_member_woman\items\i_02_wa__olgierds_gang_member_woman.w2ent");
		AddPart("items_i_03_wa__olgierds_gang_member_woman","dlc\ep1\data\characters\models\crowd_npc\olgierds_gang_member_woman\items\i_03_wa__olgierds_gang_member_woman.w2ent");
		AddPart("items_i_05_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\items\i_05_wa__novigrad_citizen.w2ent");
		AddPart("items_i_06_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\items\i_06_wa__novigrad_citizen.w2ent");
		AddPart("items_i_06_wa__novigrad_citizen_trousers","characters\models\crowd_npc\novigrad_citizen_woman\items\i_06_wa__novigrad_citizen_trousers.w2ent");
		AddPart("items_i_12_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\items\i_12_wa__novigrad_citizen.w2ent");
		AddPart("items_i_28_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\items\i_28_wa__novigrad_citizen.w2ent");
		AddPart("items_i_29_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\items\i_29_wa__novigrad_citizen.w2ent");
		AddPart("items_i_30_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\items\i_30_wa__novigrad_citizen.w2ent");
		AddPart("items_i_03_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\items\i_03_wa__novigrad_citizen.w2ent");
		AddPart("items_i_13_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\items\i_13_wa__novigrad_citizen.w2ent");
		AddPart("items_i_32_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\items\i_32_wa__novigrad_citizen.w2ent");
		AddPart("items_i_34_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\items\i_34_wa__novigrad_citizen.w2ent");
		AddPart("items_i_36_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\items\i_36_wa__novigrad_citizen.w2ent");
		AddPart("items_i_26_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\items\i_26_wa__novigrad_citizen.w2ent");
		AddPart("items_i_33_wa__novigrad_citizen_copy","characters\models\crowd_npc\novigrad_citizen_woman\items\i_33_wa__novigrad_citizen_copy.w2ent");
		AddPart("items_i_25_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\items\i_25_wa__novigrad_citizen.w2ent");
		AddPart("items_i_04_wa__novigrad_prostitute","characters\models\crowd_npc\novigrad_prostitute\items\i_04_wa__novigrad_prostitute.w2ent");
		AddPart("items_i_05_wa__novigrad_prostitute","characters\models\crowd_npc\novigrad_prostitute\items\i_05_wa__novigrad_prostitute.w2ent");
		AddPart("items_i_01_wa__novigrad_sorceress","characters\models\crowd_npc\novigrad_sorceress_woman\items\i_01_wa__novigrad_sorceress.w2ent");
		AddPart("items_i_03_wa__novigrad_sorceress","characters\models\crowd_npc\novigrad_sorceress_woman\items\i_03_wa__novigrad_sorceress.w2ent");
		AddPart("items_i_07_wa__novigrad_sorceress","characters\models\crowd_npc\novigrad_sorceress_woman\items\i_07_wa__novigrad_sorceress.w2ent");
		AddPart("items_i_08_wa__novigrad_sorceress","characters\models\crowd_npc\novigrad_sorceress_woman\items\i_08_wa__novigrad_sorceress.w2ent");
		AddPart("items_i_01_wa__skellige_villager","characters\models\crowd_npc\skellige_villager_woman\items\i_01_wa__skellige_villager.w2ent");
		AddPart("items_i_05_wa__skellige_villager","characters\models\crowd_npc\skellige_villager_woman\items\i_05_wa__skellige_villager.w2ent");
		AddPart("items_i_06_wa__skellige_villager","characters\models\crowd_npc\skellige_villager_woman\items\i_06_wa__skellige_villager.w2ent");
		AddPart("items_i_08_wa__skellige_villager","characters\models\crowd_npc\skellige_villager_woman\items\i_08_wa__skellige_villager.w2ent");
		AddPart("items_i_08_wa__skellige_villager_p03","characters\models\crowd_npc\skellige_villager_woman\items\i_08_wa__skellige_villager_p03.w2ent");
		AddPart("items_i_07_wa__skellige_villager","characters\models\crowd_npc\skellige_villager_woman\items\i_07_wa__skellige_villager.w2ent");
		AddPart("items_i_08_wa__skellige_villager_p08","characters\models\crowd_npc\skellige_villager_woman\items\i_08_wa__skellige_villager_p08.w2ent");
		AddPart("items_i_01_wa__skellige_warrior","characters\models\crowd_npc\skellige_warrior_woman\items\i_01_wa__skellige_warrior.w2ent");
		AddPart("items_i_01_wa__nml_villager","characters\models\crowd_npc\nml_villager_woman\items\i_01_wa__nml_villager.w2ent");
		AddPart("items_i_02_wa__nml_villager","characters\models\crowd_npc\nml_villager_woman\items\i_02_wa__nml_villager.w2ent");
		AddPart("items_i_03_wa__nml_villager","characters\models\crowd_npc\nml_villager_woman\items\i_03_wa__nml_villager.w2ent");
		AddPart("items_i_01_wa__scoiatael_elf","characters\models\crowd_npc\scoiatael_elf_woman\items\i_01_wa__scoiatael_elf.w2ent");
		AddPart("items_i_03_wa__succubus","characters\models\crowd_npc\succubus\items\i_03_wa__succubus.w2ent");
		AddPart("items_i_01_wa__succubus","characters\models\crowd_npc\succubus\items\i_01_wa__succubus.w2ent");
		AddPart("items_i_05_wa__succubus","characters\models\crowd_npc\succubus\items\i_05_wa__succubus.w2ent");
		AddPart("items_i_06_wa__succubus","characters\models\crowd_npc\succubus\items\i_06_wa__succubus.w2ent");
		AddPart("items_i_08_wa__succubus","characters\models\crowd_npc\succubus\items\i_08_wa__succubus.w2ent");
		AddPart("items_i_09_wa__succubus","characters\models\crowd_npc\succubus\items\i_09_wa__succubus.w2ent");
		AddPart("items_i_10_wa__succubus","characters\models\crowd_npc\succubus\items\i_10_wa__succubus.w2ent");
		AddPart("items_i_12_wa__succubus","characters\models\crowd_npc\succubus\items\i_12_wa__succubus.w2ent");
		AddPart("items_i_13_wa__succubus","characters\models\crowd_npc\succubus\items\i_13_wa__succubus.w2ent");
		AddPart("items_i_02_wa__succubus","characters\models\crowd_npc\succubus\items\i_02_wa__succubus.w2ent");
		AddPart("items_RingHenrietta","characters\models\main_npc\triss\RingHenrietta.w2ent");
		AddPart("items_NecklaceHenrietta1","characters\models\main_npc\triss\NecklaceHenrietta1.w2ent");
		AddPart("items_NecklaceHenrietta","characters\models\main_npc\triss\NecklaceHenrietta.w2ent");
		AddPart("items_i_01_wa__syanna","dlc\bob\data\characters\models\main_npc\syanna\i_01_wa__syanna.w2ent");
		AddPart("items_i_02_wa__syanna","dlc\bob\data\characters\models\main_npc\syanna\i_02_wa__syanna.w2ent");
		AddPart("items_i_03_wa__syanna","dlc\bob\data\characters\models\main_npc\syanna\i_03_wa__syanna.w2ent");
		AddPart("items_i_04_wa__syanna_cloak_px","dlc\bob\data\characters\models\main_npc\syanna\i_04_wa__syanna_cloak_px.w2ent");
		AddPart("items_i_01_wa_oriana","dlc\bob\data\characters\models\main_npc\oriana\i_01_wa_oriana.w2ent");
		AddPart("items_earrings_01_wa__anna_henrietta","dlc\bob\data\characters\models\main_npc\anna_henrietta\earrings_01_wa__anna_henrietta.w2ent");
		AddPart("items_hairpin_01_wa__anna_henrietta","dlc\bob\data\characters\models\main_npc\anna_henrietta\hairpin_01_wa__anna_henrietta.w2ent");
		AddPart("items_crown_01_wa__anna_henrietta","dlc\bob\data\characters\models\main_npc\anna_henrietta\crown_01_wa__anna_henrietta.w2ent");
		AddPart("items_i_05_wa__syanna","dlc\bob\data\characters\models\main_npc\syanna\i_05_wa__syanna.w2ent");
		AddPart("items_i_01_wa__triss_dlc","dlc\dlc6\data\characters\models\main_npc\triss\i_01_wa__triss_dlc.w2ent");
		AddPart("items_i_01_wa__eveline_gallo","dlc\ep1\data\characters\models\secondary_npc\eveline_gallo\i_01_wa__eveline_gallo.w2ent");
		AddPart("items_i_02_wa__novigrad_sorceress","characters\models\crowd_npc\novigrad_sorceress_woman\items\i_02_wa__novigrad_sorceress.w2ent");
		AddPart("items_q603_item__hanselt_mask_eveline","dlc\ep1\data\items\quest_items\q603\q603_item__hanselt_mask_eveline.w2ent");
		AddPart("items_i_01_wa__shani","dlc\ep1\data\characters\models\secondary_npc\shani\i_01_wa__shani.w2ent");
		AddPart("items_i_08_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\items\i_08_wa__novigrad_citizen.w2ent");
		AddPart("items_i_02_wa__skellige_villager","characters\models\crowd_npc\skellige_villager_woman\items\i_02_wa__skellige_villager.w2ent");
		AddPart("items_corine_torso","characters\models\main_npc\triss\corine_torso.w2ent");
		AddPart("items_pendant_03_wa__yennefer","characters\models\main_npc\yennefer\pendant_03_wa__yennefer.w2ent");
		AddPart("items_i_02_wa__bride","dlc\ep1\data\characters\models\secondary_npc\bride\i_02_wa__bride.w2ent");
		AddPart("items_i_01_wa__bride","dlc\ep1\data\characters\models\secondary_npc\bride\i_01_wa__bride.w2ent");
		AddPart("items_i_03_wa__bride","dlc\ep1\data\characters\models\secondary_npc\bride\i_03_wa__bride.w2ent");
		AddPart("items_i_01_wa__cerys","characters\models\main_npc\cerys\i_01_wa__cerys.w2ent");
		AddPart("items_i_02_wa__cerys","characters\models\main_npc\cerys\i_02_wa__cerys.w2ent");
		AddPart("items_fur_01_wa__cerys","characters\models\main_npc\cerys\fur_01_wa__cerys.w2ent");
		AddPart("items_i_04_wa__skellige_villager","characters\models\crowd_npc\skellige_villager_woman\items\i_04_wa__skellige_villager.w2ent");
		AddPart("items_i_24_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\items\i_24_wa__novigrad_citizen.w2ent");
		AddPart("items_i_14_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\items\i_14_wa__novigrad_citizen.w2ent");
		AddPart("items_i_15_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\items\i_15_wa__novigrad_citizen.w2ent");
		AddPart("items_i_10_wa__novigrad_sorceress","characters\models\crowd_npc\novigrad_sorceress_woman\items\i_10_wa__novigrad_sorceress.w2ent");
		AddPart("items_i_10_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\items\i_10_wa__novigrad_citizen.w2ent");
		AddPart("items_i_06_wa__novigrad_sorceress","characters\models\crowd_npc\novigrad_sorceress_woman\items\i_06_wa__novigrad_sorceress.w2ent");
		AddPart("items_i_02_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\items\i_02_wa__novigrad_citizen.w2ent");
		AddPart("items_i_04_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\items\i_04_wa__novigrad_citizen.w2ent");
		AddPart("items_i_11_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\items\i_11_wa__novigrad_citizen.w2ent");
		AddPart("items_i_16_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\items\i_16_wa__novigrad_citizen.w2ent");
		AddPart("items_i_23_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\items\i_23_wa__novigrad_citizen.w2ent");
		AddPart("items_i_07_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\items\i_07_wa__novigrad_citizen.w2ent");
		AddPart("items_i_01_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\items\i_01_wa__novigrad_citizen.w2ent");
		AddPart("items_i_35_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\items\i_35_wa__novigrad_citizen.w2ent");
		AddPart("items_i_02_wa__eveline_gallo","dlc\ep1\data\characters\models\secondary_npc\eveline_gallo\i_02_wa__eveline_gallo.w2ent");
		AddPart("items_i_09_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\items\i_09_wa__novigrad_citizen.w2ent");
		AddPart("items_i_31_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\items\i_31_wa__novigrad_citizen.w2ent");
		AddPart("items_i_19_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\items\i_19_wa__novigrad_citizen.w2ent");
		AddPart("items_i_06_wa__novigrad_prostitute","characters\models\crowd_npc\novigrad_prostitute\items\i_06_wa__novigrad_prostitute.w2ent");
		AddPart("items_triss_necklace","characters\models\main_npc\triss\triss_necklace.w2ent");
		AddPart("items_i_03_wa__skellige_villager","characters\models\crowd_npc\skellige_villager_woman\items\i_03_wa__skellige_villager.w2ent");
		AddPart("items_i_07_wa__novigrad_prostitute","characters\models\crowd_npc\novigrad_prostitute\items\i_07_wa__novigrad_prostitute.w2ent");
		AddPart("items_i_11_wa__novigrad_sorceress","characters\models\crowd_npc\novigrad_sorceress_woman\items\i_11_wa__novigrad_sorceress.w2ent");
		AddPart("items_lara_necklace_01_wa__ciri", "items\bodyparts\ciri_items\lara_necklace_01_wa__ciri.w2ent");
		AddPart("items_wolf_medallion_01_wa__ciri", "items\bodyparts\ciri_items\wolf_medallion_01_wa__ciri.w2ent");

		//[dress]
		AddPart("dress_q701_dress_forest_nymph","dlc\bob\data\characters\models\secondary_npc\sq701_forest_nymph\q701_dress_forest_nymph.w2ent");
		AddPart("dress_d_01_wa__olgierds_gang_member_woman","dlc\ep1\data\characters\models\crowd_npc\olgierds_gang_member_woman\dress\d_01_wa__olgierds_gang_member_woman.w2ent");
		AddPart("dress_d_06_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\dress\d_06_wa__novigrad_citizen.w2ent");
		AddPart("dress_d_07_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\dress\d_07_wa__novigrad_citizen.w2ent");
		AddPart("dress_d_08_wa__old_novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\dress\d_08_wa__old_novigrad_citizen.w2ent");
		AddPart("dress_d_01_wa__novigrad_citizen_p03","characters\models\crowd_npc\novigrad_citizen_woman\dress\d_01_wa__novigrad_citizen_p03.w2ent");
		AddPart("dress_d_01_wa__novigrad_citizen_p08","characters\models\crowd_npc\novigrad_citizen_woman\dress\d_01_wa__novigrad_citizen_p08.w2ent");
		AddPart("dress_d_01_wa__old_novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\dress\d_01_wa__old_novigrad_citizen.w2ent");
		AddPart("dress_d_02_wa__old_novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\dress\d_02_wa__old_novigrad_citizen.w2ent");
		AddPart("dress_d_04_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\dress\d_04_wa__novigrad_citizen.w2ent");
		AddPart("dress_d_04_wa__old_novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\dress\d_04_wa__old_novigrad_citizen.w2ent");
		AddPart("dress_dress_d_05_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\dress\d_05_wa__novigrad_citizen.w2ent");
		AddPart("dress_d_05_wa__old_novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\dress\d_05_wa__old_novigrad_citizen.w2ent");
		AddPart("dress_d_07_wa__novigrad_citizen_p03","characters\models\crowd_npc\novigrad_citizen_woman\dress\d_07_wa__novigrad_citizen_p03.w2ent");
		AddPart("dress_d_07_wa__old_novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\dress\d_07_wa__old_novigrad_citizen.w2ent");
		AddPart("dress_d_06_wa__novigrad_citizen_p03","characters\models\crowd_npc\novigrad_citizen_woman\dress\d_06_wa__novigrad_citizen_p03.w2ent");
		AddPart("dress_d_06_wa__novigrad_citizen_p08","characters\models\crowd_npc\novigrad_citizen_woman\dress\d_06_wa__novigrad_citizen_p08.w2ent");
		AddPart("dress_d_01_wa__novigrad_elf","characters\models\crowd_npc\novigrad_elf_woman\dress\d_01_wa__novigrad_elf.w2ent");
		AddPart("dress_d_02_wa__novigrad_elf","characters\models\crowd_npc\novigrad_elf_woman\dress\d_02_wa__novigrad_elf.w2ent");
		AddPart("dress_d_02_wa__novigrad_prostitute","characters\models\crowd_npc\novigrad_prostitute\dress\d_02_wa__novigrad_prostitute.w2ent");
		AddPart("dress_d_02_wa__novigrad_sorceress","characters\models\crowd_npc\novigrad_sorceress_woman\dress\d_02_wa__novigrad_sorceress.w2ent");
		AddPart("dress_d_03_wa__novigrad_sorceress","characters\models\crowd_npc\novigrad_sorceress_woman\dress\d_03_wa__novigrad_sorceress.w2ent");
		AddPart("dress_d_04_wa__novigrad_sorceress","characters\models\crowd_npc\novigrad_sorceress_woman\dress\d_04_wa__novigrad_sorceress.w2ent");
		AddPart("dress_d_01_wa__skellige_villager_pattern2","characters\models\crowd_npc\skellige_villager_woman\dress\d_01_wa__skellige_villager_pattern2.w2ent");
		AddPart("dress_d_02_wa__skellige_villager","characters\models\crowd_npc\skellige_villager_woman\dress\d_02_wa__skellige_villager.w2ent");
		AddPart("dress_d_03_wa__old_skellige_villager","characters\models\crowd_npc\skellige_villager_woman\dress\d_03_wa__old_skellige_villager.w2ent");
		AddPart("dress_d_03_wa__skellige_villager","characters\models\crowd_npc\skellige_villager_woman\dress\d_03_wa__skellige_villager.w2ent");
		AddPart("dress_d_05_wa__skellige_villager","characters\models\crowd_npc\skellige_villager_woman\dress\d_05_wa__skellige_villager.w2ent");
		AddPart("dress_d_06_wa__skellige_villager_p1","characters\models\crowd_npc\skellige_villager_woman\dress\d_06_wa__skellige_villager_p1.w2ent");
		AddPart("dress_d_01_wa__skellige_villager_p00","characters\models\crowd_npc\skellige_villager_woman\dress\d_01_wa__skellige_villager_p00.w2ent");
		AddPart("dress_d_01_wa__skellige_villager_p03","characters\models\crowd_npc\skellige_villager_woman\dress\d_01_wa__skellige_villager_p03.w2ent");
		AddPart("dress_d_01_wa__skellige_villager_p08","characters\models\crowd_npc\skellige_villager_woman\dress\d_01_wa__skellige_villager_p08.w2ent");
		AddPart("dress_d_01_wa__skellige_villager_pattern","characters\models\crowd_npc\skellige_villager_woman\dress\d_01_wa__skellige_villager_pattern.w2ent");
		AddPart("dress_d_06_wa__skellige_villager","characters\models\crowd_npc\skellige_villager_woman\dress\d_06_wa__skellige_villager.w2ent");
		AddPart("dress_d_01_wa__nml_villager","characters\models\crowd_npc\nml_villager_woman\dress\d_01_wa__nml_villager.w2ent");
		AddPart("dress_d_01_wa__old_nml_villager","characters\models\crowd_npc\nml_villager_woman\dress\d_01_wa__old_nml_villager.w2ent");
		AddPart("dress_d_02_wa__nml_villager","characters\models\crowd_npc\nml_villager_woman\dress\d_02_wa__nml_villager.w2ent");
		AddPart("dress_d_02_wa__old_nml_villager","characters\models\crowd_npc\nml_villager_woman\dress\d_02_wa__old_nml_villager.w2ent");
		AddPart("dress_d_03_wa__nml_villager","characters\models\crowd_npc\nml_villager_woman\dress\d_03_wa__nml_villager.w2ent");
		AddPart("dress_d_04_wa__nml_villager","characters\models\crowd_npc\nml_villager_woman\dress\d_04_wa__nml_villager.w2ent");
		AddPart("dress_d_01_wa__succubus","characters\models\crowd_npc\succubus\dress\d_01_wa__succubus.w2ent");
		AddPart("dress_d_02_wa__succubus","characters\models\crowd_npc\succubus\dress\d_02_wa__succubus.w2ent");
		AddPart("dress_d_03_wa__succubus","characters\models\crowd_npc\succubus\dress\d_03_wa__succubus.w2ent");
		AddPart("dress_d_04_wa__succubus","characters\models\crowd_npc\succubus\dress\d_04_wa__succubus.w2ent");
		AddPart("dress_05_wa__anna_henrietta_black","dlc\bob\data\characters\models\main_npc\anna_henrietta\dress_05_wa__anna_henrietta_black.w2ent");
		AddPart("dress_d_02_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\dress\d_02_wa__novigrad_citizen.w2ent");
		AddPart("dress_t2las_0_wa__bath_robe","characters\models\crowd_npc\bath_robes\dress\t2las_0_wa__bath_robe.w2ent");
		AddPart("dress_d_01_wa__novigrad_sorceress","characters\models\crowd_npc\novigrad_sorceress_woman\dress\d_01_wa__novigrad_sorceress.w2ent");
		AddPart("dress_d_02_wa__old_skellige_villager","characters\models\crowd_npc\skellige_villager_woman\dress\d_02_wa__old_skellige_villager.w2ent");
		AddPart("dress_d_01_wa__rosa","characters\models\secondary_npc\rosa\d_01_wa__rosa.w2ent");
		AddPart("dress_d_01_wa__margaritta","characters\models\secondary_npc\margaritta\d_01_wa__margaritta.w2ent");
		AddPart("dress_d_07_wa__skellige_villager","characters\models\crowd_npc\skellige_villager_woman\dress\d_07_wa__skellige_villager.w2ent");
		AddPart("dress_d_01_wa__corine","characters\models\secondary_npc\corine_dreamer\d_01_wa__corine.w2ent");
		AddPart("dress_d_03_wa__novigrad_prostitute","characters\models\crowd_npc\novigrad_prostitute\dress\d_03_wa__novigrad_prostitute.w2ent");
		AddPart("dress_d_02_wa__novigrad_prostitute_px","characters\models\crowd_npc\novigrad_prostitute\dress\d_02_wa__novigrad_prostitute_px.w2ent");
		AddPart("dress_d_01_wa__novigrad_prostitute","characters\models\crowd_npc\novigrad_prostitute\dress\d_01_wa__novigrad_prostitute.w2ent");
		AddPart("dress_d_01_wa__skellige_villager","characters\models\crowd_npc\skellige_villager_woman\dress\d_01_wa__skellige_villager.w2ent");
		AddPart("dress_d_01_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\dress\d_01_wa__novigrad_citizen.w2ent");
		AddPart("dress_d_01_wa__bride","dlc\ep1\data\characters\models\secondary_npc\bride\d_01_wa__bride.w2ent");

		//[arms]
		AddPart("arms_a0_01_wa__body","characters\models\common\woman_average\body\a0_01_wa__body.w2ent");
		AddPart("arms_a0n_02_wa__body","characters\models\common\woman_average\body\a0n_02_wa__body.w2ent");
		AddPart("arms_a2g_01_wa__old_body","characters\models\common\woman_average\body\a2g_01_wa__old_body.w2ent");
		AddPart("arms_ag_01_wa__old_body","characters\models\common\woman_average\body\ag_01_wa__old_body.w2ent");
		AddPart("arms_ag_02_wa__body","characters\models\common\woman_average\body\ag_02_wa__body.w2ent");
		AddPart("arms_a0_01_wa__old_body","characters\models\common\woman_average\body\a0_01_wa__old_body.w2ent");
		AddPart("arms_a0n_01_wa__body","characters\models\common\woman_average\body\a0n_01_wa__body.w2ent");
		AddPart("arms_a0_02_wa__body","characters\models\common\woman_average\body\a0_02_wa__body.w2ent");
		AddPart("arms_g_02_wa__body_quest_bloody","characters\models\common\woman_average\body\g_02_wa__body_quest_bloody.w2ent");
		AddPart("arms_g_01_wa__body","characters\models\common\woman_average\body\g_01_wa__body.w2ent");
		AddPart("arms_a2g_01_wa__body","characters\models\common\woman_average\body\a2g_01_wa__body.w2ent");
		AddPart("arms_ag_01_wa__body","characters\models\common\woman_average\body\ag_01_wa__body.w2ent");
		AddPart("arms_a0_03_wa__body","characters\models\common\woman_average\body\a0_03_wa__body.w2ent");
		AddPart("arms_a2g_02_wa__body","characters\models\common\woman_average\body\a2g_02_wa__body.w2ent");
		AddPart("arms_a2_01_wa__body","characters\models\common\woman_average\body\a2_01_wa__body.w2ent");
		AddPart("arms_ag2_01_wa__body_filippa","characters\models\secondary_npc\filippa\ag2_01_wa__body_filippa.w2ent");
		AddPart("arms_g_01_wa__old_body","characters\models\common\woman_average\body\g_01_wa__old_body.w2ent");
		AddPart("arms_a_01_wa__bob_woman_noble","dlc\bob\data\characters\models\crowd_npc\bob_citizen_woman\arms\a_01_wa__bob_woman_noble.w2ent");
		AddPart("arms_a_02_wa__bob_woman_noble","dlc\bob\data\characters\models\crowd_npc\bob_citizen_woman\arms\a_02_wa__bob_woman_noble.w2ent");
		AddPart("arms_a_02_wa__bob_woman_noble_p01","dlc\bob\data\characters\models\crowd_npc\bob_citizen_woman\arms\a_02_wa__bob_woman_noble_p01.w2ent");
		AddPart("arms_a_03_wa__bob_woman_noble","dlc\bob\data\characters\models\crowd_npc\bob_citizen_woman\arms\a_03_wa__bob_woman_noble.w2ent");
		AddPart("arms_a_04_wa__bob_woman_noble","dlc\bob\data\characters\models\crowd_npc\bob_citizen_woman\arms\a_04_wa__bob_woman_noble.w2ent");
		AddPart("arms_a_06_wa__bob_woman_noble","dlc\bob\data\characters\models\crowd_npc\bob_citizen_woman\arms\a_06_wa__bob_woman_noble.w2ent");
		AddPart("arms_a_06_wa__bob_woman_noble_p01","dlc\bob\data\characters\models\crowd_npc\bob_citizen_woman\arms\a_06_wa__bob_woman_noble_p01.w2ent");
		AddPart("arms_a_06_wa__bob_woman_noble_p02","dlc\bob\data\characters\models\crowd_npc\bob_citizen_woman\arms\a_06_wa__bob_woman_noble_p02.w2ent");
		AddPart("arms_a_082_wa__bob_woman_noble","dlc\bob\data\characters\models\crowd_npc\bob_citizen_woman\arms\a_082_wa__bob_woman_noble.w2ent");
		AddPart("arms_a_082_wa__bob_woman_noble_p01","dlc\bob\data\characters\models\crowd_npc\bob_citizen_woman\arms\a_082_wa__bob_woman_noble_p01.w2ent");
		AddPart("arms_a_08_wa__bob_woman_noble","dlc\bob\data\characters\models\crowd_npc\bob_citizen_woman\arms\a_08_wa__bob_woman_noble.w2ent");
		AddPart("arms_a_08_wa__bob_woman_noble_p01","dlc\bob\data\characters\models\crowd_npc\bob_citizen_woman\arms\a_08_wa__bob_woman_noble_p01.w2ent");
		AddPart("arms_a_08_wa__bob_woman_noble_p02","dlc\bob\data\characters\models\crowd_npc\bob_citizen_woman\arms\a_08_wa__bob_woman_noble_p02.w2ent");
		AddPart("arms_a_01_wa__bob_woman_noble_flat","dlc\bob\data\characters\models\crowd_npc\bob_citizen_woman\arms\a_01_wa__bob_woman_noble_flat.w2ent");
		AddPart("arms_a_02_wa__bob_woman_noble_p02","dlc\bob\data\characters\models\crowd_npc\bob_citizen_woman\arms\a_02_wa__bob_woman_noble_p02.w2ent");
		AddPart("arms_a_05_wa__bob_woman_noble","dlc\bob\data\characters\models\crowd_npc\bob_citizen_woman\arms\a_05_wa__bob_woman_noble.w2ent");
		AddPart("arms_a_06_wa__bob_woman_noble_p03","dlc\bob\data\characters\models\crowd_npc\bob_citizen_woman\arms\a_06_wa__bob_woman_noble_p03.w2ent");
		AddPart("arms_a_07_wa__bob_woman_noble","dlc\bob\data\characters\models\crowd_npc\bob_citizen_woman\arms\a_07_wa__bob_woman_noble.w2ent");
		AddPart("arms_a_03_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\arms\a_03_wa__novigrad_citizen.w2ent");
		AddPart("arms_a_04_wa__novigrad_citizen_p03","characters\models\crowd_npc\novigrad_citizen_woman\arms\a_04_wa__novigrad_citizen_p03.w2ent");
		AddPart("arms_a_05_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\arms\a_05_wa__novigrad_citizen.w2ent");
		AddPart("arms_a_08_wa__novigrad_citizen_p03","characters\models\crowd_npc\novigrad_citizen_woman\arms\a_08_wa__novigrad_citizen_p03.w2ent");
		AddPart("arms_a_01_wa__novigrad_citizen_p03","characters\models\crowd_npc\novigrad_citizen_woman\arms\a_01_wa__novigrad_citizen_p03.w2ent");
		AddPart("arms_a_06_wa__novigrad_citizen_p03","characters\models\crowd_npc\novigrad_citizen_woman\arms\a_06_wa__novigrad_citizen_p03.w2ent");
		AddPart("arms_a_06_wa__novigrad_citizen_p08","characters\models\crowd_npc\novigrad_citizen_woman\arms\a_06_wa__novigrad_citizen_p08.w2ent");
		AddPart("arms_a_09_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\arms\a_09_wa__novigrad_citizen.w2ent");
		AddPart("arms_a_12_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\arms\a_12_wa__novigrad_citizen.w2ent");
		AddPart("arms_a_01_wa__novigrad_citizen_p08","characters\models\crowd_npc\novigrad_citizen_woman\arms\a_01_wa__novigrad_citizen_p08.w2ent");
		AddPart("arms_a_10_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\arms\a_10_wa__novigrad_citizen.w2ent");
		AddPart("arms_a_04_wa__novigrad_citizen_p08","characters\models\crowd_npc\novigrad_citizen_woman\arms\a_04_wa__novigrad_citizen_p08.w2ent");
		AddPart("arms_a_08_wa__novigrad_citizen_p08","characters\models\crowd_npc\novigrad_citizen_woman\arms\a_08_wa__novigrad_citizen_p08.w2ent");
		AddPart("arms_a_01_wa__novigrad_sorceress_p08","characters\models\crowd_npc\novigrad_sorceress_woman\arms\a_01_wa__novigrad_sorceress_p08.w2ent");
		AddPart("arms_a_02_wa__novigrad_sorceress","characters\models\crowd_npc\novigrad_sorceress_woman\arms\a_02_wa__novigrad_sorceress.w2ent");
		AddPart("arms_a_01_wa__skellige_villager","characters\models\crowd_npc\skellige_villager_woman\arms\a_01_wa__skellige_villager.w2ent");
		AddPart("arms_a_02_wa__skellige_villager","characters\models\crowd_npc\skellige_villager_woman\arms\a_02_wa__skellige_villager.w2ent");
		AddPart("arms_a_03_wa__skellige_villager","characters\models\crowd_npc\skellige_villager_woman\arms\a_03_wa__skellige_villager.w2ent");
		AddPart("arms_a_04_wa__skellige_villager","characters\models\crowd_npc\skellige_villager_woman\arms\a_04_wa__skellige_villager.w2ent");
		AddPart("arms_a_05_wa__skellige_villager","characters\models\crowd_npc\skellige_villager_woman\arms\a_05_wa__skellige_villager.w2ent");
		AddPart("arms_a_02_wa__skellige_villager_p08","characters\models\crowd_npc\skellige_villager_woman\arms\a_02_wa__skellige_villager_p08.w2ent");
		AddPart("arms_ag_01_wa__skellige_warrior_woman","characters\models\crowd_npc\skellige_warrior_woman\arms\ag_01_wa__skellige_warrior_woman.w2ent");
		AddPart("arms_a_02_wa__succubus","characters\models\crowd_npc\succubus\arms\a_02_wa__succubus.w2ent");
		AddPart("arms_g_05_wa__novigrad_citizen_","characters\models\crowd_npc\novigrad_citizen_woman\gloves\g_05_wa__novigrad_citizen_.w2ent");
		AddPart("arms_g_04_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\gloves\g_04_wa__novigrad_citizen.w2ent");
		AddPart("arms_g_06_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\gloves\g_06_wa__novigrad_citizen.w2ent");
		AddPart("arms_a_01_wa__novigrad_sorceress","characters\models\crowd_npc\novigrad_sorceress_woman\arms\a_01_wa__novigrad_sorceress.w2ent");
		AddPart("arms_a_03_wa__novigrad_sorceress","characters\models\crowd_npc\novigrad_sorceress_woman\arms\a_03_wa__novigrad_sorceress.w2ent");
		AddPart("arms_a_01_wa__bride","dlc\ep1\data\characters\models\secondary_npc\bride\a_01_wa__bride.w2ent");
		AddPart("arms_a_06_wa__skellige_villager","characters\models\crowd_npc\skellige_villager_woman\arms\a_06_wa__skellige_villager.w2ent");
		AddPart("arms_ag_02_wa__skellige_warrior_woman","characters\models\crowd_npc\skellige_warrior_woman\arms\ag_02_wa__skellige_warrior_woman.w2ent");
		AddPart("arms_a_04_wa__novigrad_sorceress","characters\models\crowd_npc\novigrad_sorceress_woman\arms\a_04_wa__novigrad_sorceress.w2ent");
		AddPart("arms_a_01_wa__skellige_warrior_woman","characters\models\crowd_npc\skellige_warrior_woman\arms\a_01_wa__skellige_warrior_woman.w2ent");
		AddPart("arms_a_08_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\arms\a_08_wa__novigrad_citizen.w2ent");
		AddPart("arms_a_06_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\arms\a_06_wa__novigrad_citizen.w2ent");
		AddPart("arms_g_01_wa__body","characters\models\common\woman_average\body\g_01_wa__body.w2ent");
		AddPart("arms_a_04_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\arms\a_04_wa__novigrad_citizen.w2ent");
		AddPart("arms_a_01_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\arms\a_01_wa__novigrad_citizen.w2ent");
		AddPart("arms_a_07_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\arms\a_07_wa__novigrad_citizen.w2ent");
		AddPart("arms_a_01_wa__novigrad_prostitute","characters\models\crowd_npc\novigrad_prostitute\arms\a_01_wa__novigrad_prostitute.w2ent");
		AddPart("arms_a_11_wa__novigrad_citizen","characters\models\crowd_npc\novigrad_citizen_woman\arms\a_11_wa__novigrad_citizen.w2ent");
	}

	public function Init()
	{
		doppler = GetGeraltDoppler();

		LoadParts_1();
		LoadParts_2();
		
		count = list.Size();

		LoadBodyCategory();
		LoadTorsoCategory();
		LoadDressCategory();
		LoadArmsCategory();
		LoadLegsCategory();
		LoadShoesCategory();
		LoadItemsCategory();

		categoryCount = categories.Size();

		LoadFavorites();
	}

	public function LoadFavorites()
	{
		var slotNames : array<name>;
		var slot0, slot1, slot2, slot3, slot4, slot5 : array<int>;

		favoriteSlotsCount = 6;

		categoryConfigName = 'Hidden';
		doppler = GetGeraltDoppler();

		slotNames.PushBack( 'GDSandBoxFavorite1Head' );
		slotNames.PushBack( 'GDSandBoxFavorite1Hair' );
		slotsNames.PushBack( slotNames );
		slotNames.Clear();

		slotNames.PushBack( 'GDSandBoxFavorite2Head' );
		slotNames.PushBack( 'GDSandBoxFavorite2Hair' );
		slotsNames.PushBack( slotNames );
		slotNames.Clear();

		slotNames.PushBack( 'GDSandBoxFavorite3Head' );
		slotNames.PushBack( 'GDSandBoxFavorite3Hair' );
		slotsNames.PushBack( slotNames );
		slotNames.Clear();

		slotNames.PushBack( 'GDSandBoxFavorite4Head' );
		slotNames.PushBack( 'GDSandBoxFavorite4Hair' );
		slotsNames.PushBack( slotNames );
		slotNames.Clear();

		slotNames.PushBack( 'GDSandBoxFavorite5Head' );
		slotNames.PushBack( 'GDSandBoxFavorite5Hair' );
		slotsNames.PushBack( slotNames );
		slotNames.Clear();

		slotNames.PushBack( 'GDSandBoxFavorite6Head' );
		slotNames.PushBack( 'GDSandBoxFavorite6Hair' );
		slotsNames.PushBack( slotNames );
		slotNames.Clear();

		slot0.PushBack( StringToInt(theGame.GetInGameConfigWrapper().GetVarValue( categoryConfigName, 'GDSandBoxFavorite1Head' )) );
		slot0.PushBack( StringToInt(theGame.GetInGameConfigWrapper().GetVarValue( categoryConfigName, 'GDSandBoxFavorite1Hair' )) );
		slots.PushBack( slot0 );

		slot1.PushBack( StringToInt(theGame.GetInGameConfigWrapper().GetVarValue( categoryConfigName, 'GDSandBoxFavorite2Head' )) );
		slot1.PushBack( StringToInt(theGame.GetInGameConfigWrapper().GetVarValue( categoryConfigName, 'GDSandBoxFavorite2Hair' )) );
		slots.PushBack( slot1 );

		slot2.PushBack( StringToInt(theGame.GetInGameConfigWrapper().GetVarValue( categoryConfigName, 'GDSandBoxFavorite3Head' )) );
		slot2.PushBack( StringToInt(theGame.GetInGameConfigWrapper().GetVarValue( categoryConfigName, 'GDSandBoxFavorite3Hair' )) );
		slots.PushBack( slot2 );

		slot3.PushBack( StringToInt(theGame.GetInGameConfigWrapper().GetVarValue( categoryConfigName, 'GDSandBoxFavorite4Head' )) );
		slot3.PushBack( StringToInt(theGame.GetInGameConfigWrapper().GetVarValue( categoryConfigName, 'GDSandBoxFavorite4Hair' )) );
		slots.PushBack( slot3 );

		slot4.PushBack( StringToInt(theGame.GetInGameConfigWrapper().GetVarValue( categoryConfigName, 'GDSandBoxFavorite5Head' )) );
		slot4.PushBack( StringToInt(theGame.GetInGameConfigWrapper().GetVarValue( categoryConfigName, 'GDSandBoxFavorite5Hair' )) );
		slots.PushBack( slot4 );

		slot5.PushBack( StringToInt(theGame.GetInGameConfigWrapper().GetVarValue( categoryConfigName, 'GDSandBoxFavorite6Head' )) );
		slot5.PushBack( StringToInt(theGame.GetInGameConfigWrapper().GetVarValue( categoryConfigName, 'GDSandBoxFavorite6Hair' )) );
		slots.PushBack( slot5 );
	}

	public function LoadSlotToCurrent(index: int)
	{
		var i : int;

		doppler.resources.UnloadCurrentHair();

		if ( doppler.resources.Head.IsValid(slots[ index ][0]) ) {
			doppler.resources.currentHead = slots[ index ][0];
		}

		if ( doppler.resources.Hair.IsValid(slots[ index ][1]) ) {
			doppler.resources.currentHair = slots[ index ][1];
		}

		doppler.resources.SetCurrentHead();
		doppler.resources.SetCurrentHair();

		UnloadAll();
		
		for (i = 0; i < categoryCount; i += 1) {
			categories[i].LoadSlotToCurrent(index);
		}

		LoadAll();
	}

	public function SaveCurrentToSlot(index: int)
	{
		var i : int;

		doppler = GetGeraltDoppler();

		slots[ index ][0] = doppler.resources.currentHead;
		slots[ index ][1] = doppler.resources.currentHair;

		doppler.configWrapper.SetVarValue( categoryConfigName, slotsNames[ index ][ 0 ], IntToString( slots[ index ][0] ) );
		doppler.configWrapper.SetVarValue( categoryConfigName, slotsNames[ index ][ 1 ], IntToString( slots[ index ][1] ) );

		for (i = 0; i < categoryCount; i += 1) {
			categories[i].SaveCurrentToSlot(index);
		}
	}

	public function GetCurrentBodyDescription() : string
	{
		var text : string;
		var i, ix : int;

		text = "";
		for (i = 0; i < categoryCount; i += 1)
		{
			for (ix = 0; ix < categories[i].slotsCount; ix += 1)
			{
				if ( categories[i].currents[ix] > 0 ) {
					text += "    " + categories[i].GetNameFromIndex(ix) + "<br />";
				}
			}
		}

		if (text == "") {
			text = "    Empty<br />";
		}

		return text;
	}

	public function GetFavoritedBodyDescription(slot: int) : string
	{
		var text : string;
		var i, ix : int;

		text = "";
		for (i = 0; i < categoryCount; i += 1)
		{
			for (ix = 0; ix < categories[i].favoriteSlots[slot].Size(); ix += 1)
			{
				if ( categories[i].favoriteSlots[slot][ix] > 0 ) {
					text += "    " + categories[i].names[categories[i].favoriteSlots[slot][ix]] + "<br />";
				}
			}
		}

		if (text == "") {
			text = "    Empty<br />";
		}

		return text;
	}

	public function GetFavoriteSlotDescription( slot : int ) : string
	{
		var text : string;

		doppler = GetGeraltDoppler();

		text  = "<font size='15'>";
		text += "Head: " + doppler.resources.Head.GetName(slots[ slot ][0]) + "<br />";
		text += "Hair: " + doppler.resources.Hair.GetName(slots[ slot ][1]) + "<br />";
		text += "Body:<br />";
		text += GetFavoritedBodyDescription(slot);
		text += "</font>";

		return text;
	}

	public function AddPart(partName: string, path : string)
	{
		list.PushBack((CEntityTemplate)LoadResource(path, true));
		names.PushBack(partName);
	}

	public function IsValid(index : int) : bool
	{
		return index >= 0 && index < count && categoryCount > 0;
	}

	public function LoadBodyCategory()
	{
		var beginsTemp : array<string>;
		var category : GeraltDopplerSandBoxCategory;
		var slotsNames, favoritesNames : array<name>;
		var favorites : array<array<name>>;

		slotsNames.PushBack('GDSandBoxBody1');

		beginsTemp.PushBack("body_");
		
		category = new GeraltDopplerSandBoxCategory in this;
		category.Init("Body", beginsTemp, slotsNames, false);

		favoritesNames.Clear();
		favoritesNames.PushBack('GDSandBoxFavorite1Body1');
		category.AddFavorites(favoritesNames);
		
		favoritesNames.Clear();
		favoritesNames.PushBack('GDSandBoxFavorite2Body1');
		category.AddFavorites(favoritesNames);

		favoritesNames.Clear();
		favoritesNames.PushBack('GDSandBoxFavorite3Body1');
		category.AddFavorites(favoritesNames);

		favoritesNames.Clear();
		favoritesNames.PushBack('GDSandBoxFavorite4Body1');
		category.AddFavorites(favoritesNames);

		favoritesNames.Clear();
		favoritesNames.PushBack('GDSandBoxFavorite5Body1');
		category.AddFavorites(favoritesNames);

		favoritesNames.Clear();
		favoritesNames.PushBack('GDSandBoxFavorite6Body1');
		category.AddFavorites(favoritesNames);

		categories.PushBack(category);
	}

	public function LoadTorsoCategory()
	{
		var beginsTemp : array<string>;
		var category : GeraltDopplerSandBoxCategory;
		var slotsNames, favoritesNames : array<name>;
		var favorites : array<array<name>>;

		slotsNames.PushBack('GDSandBoxTorso1');
		slotsNames.PushBack('GDSandBoxTorso2');

		beginsTemp.PushBack("torso_");
		
		category = new GeraltDopplerSandBoxCategory in this;
		category.Init("Torso", beginsTemp, slotsNames, false);

		favoritesNames.Clear();
		favoritesNames.PushBack( 'GDSandBoxFavorite1Torso1' );
		favoritesNames.PushBack( 'GDSandBoxFavorite1Torso2' );
		category.AddFavorites(favoritesNames);
		
		favoritesNames.Clear();
		favoritesNames.PushBack( 'GDSandBoxFavorite2Torso1' );
		favoritesNames.PushBack( 'GDSandBoxFavorite2Torso2' );
		category.AddFavorites(favoritesNames);

		favoritesNames.Clear();
		favoritesNames.PushBack( 'GDSandBoxFavorite3Torso1' );
		favoritesNames.PushBack( 'GDSandBoxFavorite3Torso2' );
		category.AddFavorites(favoritesNames);

		favoritesNames.Clear();
		favoritesNames.PushBack( 'GDSandBoxFavorite4Torso1' );
		favoritesNames.PushBack( 'GDSandBoxFavorite4Torso2' );
		category.AddFavorites(favoritesNames);

		favoritesNames.Clear();
		favoritesNames.PushBack( 'GDSandBoxFavorite5Torso1' );
		favoritesNames.PushBack( 'GDSandBoxFavorite5Torso2' );
		category.AddFavorites(favoritesNames);

		favoritesNames.Clear();
		favoritesNames.PushBack( 'GDSandBoxFavorite6Torso1' );
		favoritesNames.PushBack( 'GDSandBoxFavorite6Torso2' );
		category.AddFavorites(favoritesNames);

		categories.PushBack(category);
	}

	public function LoadDressCategory()
	{
		var beginsTemp : array<string>;
		var category : GeraltDopplerSandBoxCategory;
		var slotsNames, favoritesNames : array<name>;
		var favorites : array<array<name>>;

		slotsNames.PushBack('GDSandBoxDress1');
		slotsNames.PushBack('GDSandBoxDress2');

		beginsTemp.PushBack("dress_");

		category = new GeraltDopplerSandBoxCategory in this;
		category.Init("Dress", beginsTemp, slotsNames, false);

		favoritesNames.Clear();
		favoritesNames.PushBack( 'GDSandBoxFavorite1Dress1' );
		favoritesNames.PushBack( 'GDSandBoxFavorite1Dress2' );
		category.AddFavorites(favoritesNames);

		favoritesNames.Clear();
		favoritesNames.PushBack( 'GDSandBoxFavorite2Dress1' );
		favoritesNames.PushBack( 'GDSandBoxFavorite2Dress2' );
		category.AddFavorites(favoritesNames);

		favoritesNames.Clear();
		favoritesNames.PushBack( 'GDSandBoxFavorite3Dress1' );
		favoritesNames.PushBack( 'GDSandBoxFavorite3Dress2' );
		category.AddFavorites(favoritesNames);

		favoritesNames.Clear();
		favoritesNames.PushBack( 'GDSandBoxFavorite4Dress1' );
		favoritesNames.PushBack( 'GDSandBoxFavorite4Dress2' );
		category.AddFavorites(favoritesNames);

		favoritesNames.Clear();
		favoritesNames.PushBack( 'GDSandBoxFavorite5Dress1' );
		favoritesNames.PushBack( 'GDSandBoxFavorite5Dress2' );
		category.AddFavorites(favoritesNames);

		favoritesNames.Clear();
		favoritesNames.PushBack( 'GDSandBoxFavorite6Dress1' );
		favoritesNames.PushBack( 'GDSandBoxFavorite6Dress2' );
		category.AddFavorites(favoritesNames);

		categories.PushBack(category);
	}

	public function LoadArmsCategory()
	{
		var beginsTemp : array<string>;
		var category : GeraltDopplerSandBoxCategory;
		var slotsNames, favoritesNames : array<name>;
		var favorites : array<array<name>>;

		slotsNames.PushBack('GDSandBoxArms1');
		slotsNames.PushBack('GDSandBoxArms2');
		slotsNames.PushBack('GDSandBoxArms3');
		slotsNames.PushBack('GDSandBoxArms4');

		beginsTemp.PushBack("arms_");
		
		category = new GeraltDopplerSandBoxCategory in this;
		category.Init("Arms", beginsTemp, slotsNames, false);

		favoritesNames.Clear();
		favoritesNames.PushBack( 'GDSandBoxFavorite1Arms1' );
		favoritesNames.PushBack( 'GDSandBoxFavorite1Arms2' );
		favoritesNames.PushBack( 'GDSandBoxFavorite1Arms3' );
		favoritesNames.PushBack( 'GDSandBoxFavorite1Arms4' );
		category.AddFavorites(favoritesNames);

		favoritesNames.Clear();
		favoritesNames.PushBack( 'GDSandBoxFavorite2Arms1' );
		favoritesNames.PushBack( 'GDSandBoxFavorite2Arms2' );
		favoritesNames.PushBack( 'GDSandBoxFavorite2Arms3' );
		favoritesNames.PushBack( 'GDSandBoxFavorite2Arms4' );
		category.AddFavorites(favoritesNames);

		favoritesNames.Clear();
		favoritesNames.PushBack( 'GDSandBoxFavorite3Arms1' );
		favoritesNames.PushBack( 'GDSandBoxFavorite3Arms2' );
		favoritesNames.PushBack( 'GDSandBoxFavorite3Arms3' );
		favoritesNames.PushBack( 'GDSandBoxFavorite3Arms4' );
		category.AddFavorites(favoritesNames);

		favoritesNames.Clear();
		favoritesNames.PushBack( 'GDSandBoxFavorite4Arms1' );
		favoritesNames.PushBack( 'GDSandBoxFavorite4Arms2' );
		favoritesNames.PushBack( 'GDSandBoxFavorite4Arms3' );
		favoritesNames.PushBack( 'GDSandBoxFavorite4Arms4' );
		category.AddFavorites(favoritesNames);

		favoritesNames.Clear();
		favoritesNames.PushBack( 'GDSandBoxFavorite5Arms1' );
		favoritesNames.PushBack( 'GDSandBoxFavorite5Arms2' );
		favoritesNames.PushBack( 'GDSandBoxFavorite5Arms3' );
		favoritesNames.PushBack( 'GDSandBoxFavorite5Arms4' );
		category.AddFavorites(favoritesNames);

		favoritesNames.Clear();
		favoritesNames.PushBack( 'GDSandBoxFavorite6Arms1' );
		favoritesNames.PushBack( 'GDSandBoxFavorite6Arms2' );
		favoritesNames.PushBack( 'GDSandBoxFavorite6Arms3' );
		favoritesNames.PushBack( 'GDSandBoxFavorite6Arms4' );
		category.AddFavorites(favoritesNames);

		categories.PushBack(category);
	}

	public function LoadLegsCategory()
	{
		var beginsTemp : array<string>;
		var category : GeraltDopplerSandBoxCategory;
		var slotsNames, favoritesNames : array<name>;
		var favorites : array<array<name>>;

		slotsNames.PushBack('GDSandBoxLegs1');
		slotsNames.PushBack('GDSandBoxLegs2');
		slotsNames.PushBack('GDSandBoxLegs3');
		slotsNames.PushBack('GDSandBoxLegs4');
		slotsNames.PushBack('GDSandBoxLegs5');

		beginsTemp.PushBack("legs_");
	
		category = new GeraltDopplerSandBoxCategory in this;
		category.Init("Legs", beginsTemp, slotsNames, false);

		favoritesNames.Clear();
		favoritesNames.PushBack( 'GDSandBoxFavorite1Legs1' );
		favoritesNames.PushBack( 'GDSandBoxFavorite1Legs2' );
		favoritesNames.PushBack( 'GDSandBoxFavorite1Legs3' );
		favoritesNames.PushBack( 'GDSandBoxFavorite1Legs4' );
		favoritesNames.PushBack( 'GDSandBoxFavorite1Legs5' );
		category.AddFavorites(favoritesNames);

		favoritesNames.Clear();
		favoritesNames.PushBack( 'GDSandBoxFavorite2Legs1' );
		favoritesNames.PushBack( 'GDSandBoxFavorite2Legs2' );
		favoritesNames.PushBack( 'GDSandBoxFavorite2Legs3' );
		favoritesNames.PushBack( 'GDSandBoxFavorite2Legs4' );
		favoritesNames.PushBack( 'GDSandBoxFavorite2Legs5' );
		category.AddFavorites(favoritesNames);

		favoritesNames.Clear();
		favoritesNames.PushBack( 'GDSandBoxFavorite3Legs1' );
		favoritesNames.PushBack( 'GDSandBoxFavorite3Legs2' );
		favoritesNames.PushBack( 'GDSandBoxFavorite3Legs3' );
		favoritesNames.PushBack( 'GDSandBoxFavorite3Legs4' );
		favoritesNames.PushBack( 'GDSandBoxFavorite3Legs5' );
		category.AddFavorites(favoritesNames);

		favoritesNames.Clear();
		favoritesNames.PushBack( 'GDSandBoxFavorite4Legs1' );
		favoritesNames.PushBack( 'GDSandBoxFavorite4Legs2' );
		favoritesNames.PushBack( 'GDSandBoxFavorite4Legs3' );
		favoritesNames.PushBack( 'GDSandBoxFavorite4Legs4' );
		favoritesNames.PushBack( 'GDSandBoxFavorite4Legs5' );
		category.AddFavorites(favoritesNames);

		favoritesNames.Clear();
		favoritesNames.PushBack( 'GDSandBoxFavorite5Legs1' );
		favoritesNames.PushBack( 'GDSandBoxFavorite5Legs2' );
		favoritesNames.PushBack( 'GDSandBoxFavorite5Legs3' );
		favoritesNames.PushBack( 'GDSandBoxFavorite5Legs4' );
		favoritesNames.PushBack( 'GDSandBoxFavorite5Legs5' );
		category.AddFavorites(favoritesNames);

		favoritesNames.Clear();
		favoritesNames.PushBack( 'GDSandBoxFavorite6Legs1' );
		favoritesNames.PushBack( 'GDSandBoxFavorite6Legs2' );
		favoritesNames.PushBack( 'GDSandBoxFavorite6Legs3' );
		favoritesNames.PushBack( 'GDSandBoxFavorite6Legs4' );
		favoritesNames.PushBack( 'GDSandBoxFavorite6Legs5' );
		category.AddFavorites(favoritesNames);

		categories.PushBack(category);
	}

	public function LoadShoesCategory()
	{
		var beginsTemp : array<string>;
		var category : GeraltDopplerSandBoxCategory;
		var slotsNames, favoritesNames : array<name>;
		var favorites : array<array<name>>;

		slotsNames.PushBack('GDSandBoxShoes1');
		slotsNames.PushBack('GDSandBoxShoes2');

		beginsTemp.PushBack("shoes_");

		category = new GeraltDopplerSandBoxCategory in this;
		category.Init("Shoes", beginsTemp, slotsNames, false);

		favoritesNames.Clear();
		favoritesNames.PushBack( 'GDSandBoxFavorite1Shoes1' );
		favoritesNames.PushBack( 'GDSandBoxFavorite1Shoes2' );
		category.AddFavorites(favoritesNames);

		favoritesNames.Clear();
		favoritesNames.PushBack( 'GDSandBoxFavorite2Shoes1' );
		favoritesNames.PushBack( 'GDSandBoxFavorite2Shoes2' );
		category.AddFavorites(favoritesNames);

		favoritesNames.Clear();
		favoritesNames.PushBack( 'GDSandBoxFavorite3Shoes1' );
		favoritesNames.PushBack( 'GDSandBoxFavorite3Shoes2' );
		category.AddFavorites(favoritesNames);

		favoritesNames.Clear();
		favoritesNames.PushBack( 'GDSandBoxFavorite4Shoes1' );
		favoritesNames.PushBack( 'GDSandBoxFavorite4Shoes2' );
		category.AddFavorites(favoritesNames);

		favoritesNames.Clear();
		favoritesNames.PushBack( 'GDSandBoxFavorite5Shoes1' );
		favoritesNames.PushBack( 'GDSandBoxFavorite5Shoes2' );
		category.AddFavorites(favoritesNames);

		favoritesNames.Clear();
		favoritesNames.PushBack( 'GDSandBoxFavorite6Shoes1' );
		favoritesNames.PushBack( 'GDSandBoxFavorite6Shoes2' );
		category.AddFavorites(favoritesNames);

		categories.PushBack(category);
	}

	public function LoadItemsCategory()
	{
		var beginsTemp : array<string>;
		var category : GeraltDopplerSandBoxCategory;
		var slotsNames, favoritesNames : array<name>;
		var favorites : array<array<name>>;

		slotsNames.PushBack('GDSandBoxItems1');
		slotsNames.PushBack('GDSandBoxItems2');
		slotsNames.PushBack('GDSandBoxItems3');
		slotsNames.PushBack('GDSandBoxItems4');
		slotsNames.PushBack('GDSandBoxItems5');
		slotsNames.PushBack('GDSandBoxItems6');
		slotsNames.PushBack('GDSandBoxItems7');
		slotsNames.PushBack('GDSandBoxItems8');
		slotsNames.PushBack('GDSandBoxItems9');
		slotsNames.PushBack('GDSandBoxItems10');
		slotsNames.PushBack('GDSandBoxItems11');
		slotsNames.PushBack('GDSandBoxItems12');
		slotsNames.PushBack('GDSandBoxItems13');
		slotsNames.PushBack('GDSandBoxItems14');
		slotsNames.PushBack('GDSandBoxItems15');
		slotsNames.PushBack('GDSandBoxItems16');
		slotsNames.PushBack('GDSandBoxItems17');
		slotsNames.PushBack('GDSandBoxItems18');
		slotsNames.PushBack('GDSandBoxItems19');
		slotsNames.PushBack('GDSandBoxItems20');
		
		beginsTemp.PushBack("items_");
		

		category = new GeraltDopplerSandBoxCategory in this;
		category.Init("Items", beginsTemp, slotsNames, false);

		favoritesNames.Clear();
		favoritesNames.PushBack( 'GDSandBoxFavorite1Items1' );
		favoritesNames.PushBack( 'GDSandBoxFavorite1Items2' );
		favoritesNames.PushBack( 'GDSandBoxFavorite1Items3' );
		favoritesNames.PushBack( 'GDSandBoxFavorite1Items4' );
		favoritesNames.PushBack( 'GDSandBoxFavorite1Items5' );
		favoritesNames.PushBack( 'GDSandBoxFavorite1Items6' );
		favoritesNames.PushBack( 'GDSandBoxFavorite1Items7' );
		favoritesNames.PushBack( 'GDSandBoxFavorite1Items8' );
		favoritesNames.PushBack( 'GDSandBoxFavorite1Items9' );
		favoritesNames.PushBack( 'GDSandBoxFavorite1Items10' );
		favoritesNames.PushBack( 'GDSandBoxFavorite1Items11' );
		favoritesNames.PushBack( 'GDSandBoxFavorite1Items12' );
		favoritesNames.PushBack( 'GDSandBoxFavorite1Items13' );
		favoritesNames.PushBack( 'GDSandBoxFavorite1Items14' );
		favoritesNames.PushBack( 'GDSandBoxFavorite1Items15' );
		favoritesNames.PushBack( 'GDSandBoxFavorite1Items16' );
		favoritesNames.PushBack( 'GDSandBoxFavorite1Items17' );
		favoritesNames.PushBack( 'GDSandBoxFavorite1Items18' );
		favoritesNames.PushBack( 'GDSandBoxFavorite1Items19' );
		favoritesNames.PushBack( 'GDSandBoxFavorite1Items20' );
		category.AddFavorites(favoritesNames);

		favoritesNames.Clear();
		favoritesNames.PushBack( 'GDSandBoxFavorite2Items1' );
		favoritesNames.PushBack( 'GDSandBoxFavorite2Items2' );
		favoritesNames.PushBack( 'GDSandBoxFavorite2Items3' );
		favoritesNames.PushBack( 'GDSandBoxFavorite2Items4' );
		favoritesNames.PushBack( 'GDSandBoxFavorite2Items5' );
		favoritesNames.PushBack( 'GDSandBoxFavorite2Items6' );
		favoritesNames.PushBack( 'GDSandBoxFavorite2Items7' );
		favoritesNames.PushBack( 'GDSandBoxFavorite2Items8' );
		favoritesNames.PushBack( 'GDSandBoxFavorite2Items9' );
		favoritesNames.PushBack( 'GDSandBoxFavorite2Items10' );
		favoritesNames.PushBack( 'GDSandBoxFavorite2Items11' );
		favoritesNames.PushBack( 'GDSandBoxFavorite2Items12' );
		favoritesNames.PushBack( 'GDSandBoxFavorite2Items13' );
		favoritesNames.PushBack( 'GDSandBoxFavorite2Items14' );
		favoritesNames.PushBack( 'GDSandBoxFavorite2Items15' );
		favoritesNames.PushBack( 'GDSandBoxFavorite2Items16' );
		favoritesNames.PushBack( 'GDSandBoxFavorite2Items17' );
		favoritesNames.PushBack( 'GDSandBoxFavorite2Items18' );
		favoritesNames.PushBack( 'GDSandBoxFavorite2Items19' );
		favoritesNames.PushBack( 'GDSandBoxFavorite2Items20' );
		category.AddFavorites(favoritesNames);

		favoritesNames.Clear();
		favoritesNames.PushBack( 'GDSandBoxFavorite3Items1' );
		favoritesNames.PushBack( 'GDSandBoxFavorite3Items2' );
		favoritesNames.PushBack( 'GDSandBoxFavorite3Items3' );
		favoritesNames.PushBack( 'GDSandBoxFavorite3Items4' );
		favoritesNames.PushBack( 'GDSandBoxFavorite3Items5' );
		favoritesNames.PushBack( 'GDSandBoxFavorite3Items6' );
		favoritesNames.PushBack( 'GDSandBoxFavorite3Items7' );
		favoritesNames.PushBack( 'GDSandBoxFavorite3Items8' );
		favoritesNames.PushBack( 'GDSandBoxFavorite3Items9' );
		favoritesNames.PushBack( 'GDSandBoxFavorite3Items10' );
		favoritesNames.PushBack( 'GDSandBoxFavorite3Items11' );
		favoritesNames.PushBack( 'GDSandBoxFavorite3Items12' );
		favoritesNames.PushBack( 'GDSandBoxFavorite3Items13' );
		favoritesNames.PushBack( 'GDSandBoxFavorite3Items14' );
		favoritesNames.PushBack( 'GDSandBoxFavorite3Items15' );
		favoritesNames.PushBack( 'GDSandBoxFavorite3Items16' );
		favoritesNames.PushBack( 'GDSandBoxFavorite3Items17' );
		favoritesNames.PushBack( 'GDSandBoxFavorite3Items18' );
		favoritesNames.PushBack( 'GDSandBoxFavorite3Items19' );
		favoritesNames.PushBack( 'GDSandBoxFavorite3Items20' );
		category.AddFavorites(favoritesNames);

		favoritesNames.Clear();
		favoritesNames.PushBack( 'GDSandBoxFavorite4Items1' );
		favoritesNames.PushBack( 'GDSandBoxFavorite4Items2' );
		favoritesNames.PushBack( 'GDSandBoxFavorite4Items3' );
		favoritesNames.PushBack( 'GDSandBoxFavorite4Items4' );
		favoritesNames.PushBack( 'GDSandBoxFavorite4Items5' );
		favoritesNames.PushBack( 'GDSandBoxFavorite4Items6' );
		favoritesNames.PushBack( 'GDSandBoxFavorite4Items7' );
		favoritesNames.PushBack( 'GDSandBoxFavorite4Items8' );
		favoritesNames.PushBack( 'GDSandBoxFavorite4Items9' );
		favoritesNames.PushBack( 'GDSandBoxFavorite4Items10' );
		favoritesNames.PushBack( 'GDSandBoxFavorite4Items11' );
		favoritesNames.PushBack( 'GDSandBoxFavorite4Items12' );
		favoritesNames.PushBack( 'GDSandBoxFavorite4Items13' );
		favoritesNames.PushBack( 'GDSandBoxFavorite4Items14' );
		favoritesNames.PushBack( 'GDSandBoxFavorite4Items15' );
		favoritesNames.PushBack( 'GDSandBoxFavorite4Items16' );
		favoritesNames.PushBack( 'GDSandBoxFavorite4Items17' );
		favoritesNames.PushBack( 'GDSandBoxFavorite4Items18' );
		favoritesNames.PushBack( 'GDSandBoxFavorite4Items19' );
		favoritesNames.PushBack( 'GDSandBoxFavorite4Items20' );
		category.AddFavorites(favoritesNames);

		favoritesNames.Clear();
		favoritesNames.PushBack( 'GDSandBoxFavorite5Items1' );
		favoritesNames.PushBack( 'GDSandBoxFavorite5Items2' );
		favoritesNames.PushBack( 'GDSandBoxFavorite5Items3' );
		favoritesNames.PushBack( 'GDSandBoxFavorite5Items4' );
		favoritesNames.PushBack( 'GDSandBoxFavorite5Items5' );
		favoritesNames.PushBack( 'GDSandBoxFavorite5Items6' );
		favoritesNames.PushBack( 'GDSandBoxFavorite5Items7' );
		favoritesNames.PushBack( 'GDSandBoxFavorite5Items8' );
		favoritesNames.PushBack( 'GDSandBoxFavorite5Items9' );
		favoritesNames.PushBack( 'GDSandBoxFavorite5Items10' );
		favoritesNames.PushBack( 'GDSandBoxFavorite5Items11' );
		favoritesNames.PushBack( 'GDSandBoxFavorite5Items12' );
		favoritesNames.PushBack( 'GDSandBoxFavorite5Items13' );
		favoritesNames.PushBack( 'GDSandBoxFavorite5Items14' );
		favoritesNames.PushBack( 'GDSandBoxFavorite5Items15' );
		favoritesNames.PushBack( 'GDSandBoxFavorite5Items16' );
		favoritesNames.PushBack( 'GDSandBoxFavorite5Items17' );
		favoritesNames.PushBack( 'GDSandBoxFavorite5Items18' );
		favoritesNames.PushBack( 'GDSandBoxFavorite5Items19' );
		favoritesNames.PushBack( 'GDSandBoxFavorite5Items20' );
		category.AddFavorites(favoritesNames);

		favoritesNames.Clear();
		favoritesNames.PushBack( 'GDSandBoxFavorite6Items1' );
		favoritesNames.PushBack( 'GDSandBoxFavorite6Items2' );
		favoritesNames.PushBack( 'GDSandBoxFavorite6Items3' );
		favoritesNames.PushBack( 'GDSandBoxFavorite6Items4' );
		favoritesNames.PushBack( 'GDSandBoxFavorite6Items5' );
		favoritesNames.PushBack( 'GDSandBoxFavorite6Items6' );
		favoritesNames.PushBack( 'GDSandBoxFavorite6Items7' );
		favoritesNames.PushBack( 'GDSandBoxFavorite6Items8' );
		favoritesNames.PushBack( 'GDSandBoxFavorite6Items9' );
		favoritesNames.PushBack( 'GDSandBoxFavorite6Items10' );
		favoritesNames.PushBack( 'GDSandBoxFavorite6Items11' );
		favoritesNames.PushBack( 'GDSandBoxFavorite6Items12' );
		favoritesNames.PushBack( 'GDSandBoxFavorite6Items13' );
		favoritesNames.PushBack( 'GDSandBoxFavorite6Items14' );
		favoritesNames.PushBack( 'GDSandBoxFavorite6Items15' );
		favoritesNames.PushBack( 'GDSandBoxFavorite6Items16' );
		favoritesNames.PushBack( 'GDSandBoxFavorite6Items17' );
		favoritesNames.PushBack( 'GDSandBoxFavorite6Items18' );
		favoritesNames.PushBack( 'GDSandBoxFavorite6Items19' );
		favoritesNames.PushBack( 'GDSandBoxFavorite6Items20' );
		category.AddFavorites(favoritesNames);

		categories.PushBack(category);
	}

	public function LoadAll()
	{
		var i, catSize, slotsSize : int;

		catSize = categories.Size();

		for (i = 0; i < catSize; i += 1) {
			categories[i].LoadAll();
		}
	}

	public function UnloadAll()
	{
		var i, catSize, slotsSize : int;

		catSize = categories.Size();

		for (i = 0; i < catSize; i += 1) {
			categories[i].UnloadAll();
		}
	}

	public function LoadFromConfig()
	{
		var i, catSize, slotsSize : int;

		catSize = categories.Size();

		for (i = 0; i < catSize; i += 1) {
			categories[i].LoadFromConfig();
		}
	}
}
