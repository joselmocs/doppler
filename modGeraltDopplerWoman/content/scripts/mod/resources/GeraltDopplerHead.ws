class GeraltDopplerHead
{
	public var list : array<name>;
	public var names : array<string>;
	public var count : int;

	public function Init()
	{
		IncludeHead("Annabelle", 'anabelle');
		IncludeHead("Anna Strenger", 'anna');
		IncludeHead("Anna Henrietta", 'annahenrietta');
		IncludeHead("Bride", 'bride');
		IncludeHead("Cerys", 'cerys');
		IncludeHead("Ciri", 'ciri');
		IncludeHead("Corrine", 'corine');
		IncludeHead("Edna var Attre", 'edna');
		IncludeHead("Eveline Gallo", 'eveline');
		IncludeHead("Felicia Cori", 'felicia');
		IncludeHead("Philippa Eilhart", 'filippa');
		IncludeHead("Fringilla Vigo", 'fringilla');
		IncludeHead("Irina", 'irina');
		IncludeHead("Iris", 'iris');
		IncludeHead("Iris's Mother", 'irismother');
		IncludeHead("Keira Metz", 'keira');
		IncludeHead("Birna Bran", 'ladybran');
		IncludeHead("Mary Louisa", 'luiza');
		IncludeHead("Margarita Laux-Antille", 'margaritta');
		IncludeHead("Nobleman's Daughter", 'noblemansdaughter');
		IncludeHead("Orianna", 'oriana');
		IncludeHead("Priscilla", 'priscilla');
		IncludeHead("Shani", 'shani');
		IncludeHead("Sheala de Tancarville", 'sheala');
		IncludeHead("Syanna", 'syanna');
		IncludeHead("Tamara Strenger", 'tamara');
		IncludeHead("Triss Merigold", 'triss');
		IncludeHead("Ves", 'ves');
		IncludeHead("Vespula", 'vespula');
		IncludeHead("Vivienne de Tabris", 'vivienne');
		IncludeHead("Yennefer of Vengerberg", 'yennefer');
		IncludeHead("Succbubs Black", 'succubusblack');
		IncludeHead("Succbubs Red", 'succubusred');
		IncludeHead("Succbubs White", 'succubuswhite');
		IncludeHead("Brewess", 'brewess');
		IncludeHead("Skellige", 'skellige1');
		IncludeHead("Novigrad1", 'novigrad1');
		IncludeHead("Novigrad2", 'novigrad2');
		IncludeHead("Novigrad3", 'novigrad3');
		IncludeHead("Novigrad4", 'novigrad4');
		IncludeHead("Novigrad5", 'novigrad5');
		IncludeHead("Novigrad6", 'novigrad6');
		IncludeHead("Novigrad7", 'novigrad7');
		IncludeHead("Novigrad8", 'novigrad8');
		IncludeHead("Novigrad9", 'novigrad9');
		IncludeHead("Novigrad10", 'novigrad10');
		IncludeHead("Novigrad11", 'novigrad11');
		IncludeHead("Novigrad12", 'novigrad12');
		IncludeHead("Novigrad13", 'novigrad13');
		IncludeHead("Novigrad14", 'novigrad14');
		IncludeHead("Novigrad15", 'novigrad15');
		IncludeHead("Novigrad16", 'novigrad16');
		IncludeHead("Novigrad17", 'novigrad17');
		IncludeHead("Novigrad18", 'novigrad18');
		IncludeHead("Novigrad19", 'novigrad19');
		IncludeHead("Novigrad20", 'novigrad20');
		IncludeHead("Novigrad21", 'novigrad21');
		IncludeHead("Novigrad22", 'novigrad22');
		IncludeHead("Novigrad23", 'novigrad23');
		IncludeHead("Novigrad24", 'novigrad24');
		IncludeHead("Novigrad25", 'novigrad25');
		IncludeHead("Novigrad26", 'novigrad26');
		IncludeHead("Novigrad27", 'novigrad27');
		IncludeHead("Novigrad28", 'novigrad28');
		IncludeHead("Novigrad29", 'novigrad29');
		IncludeHead("Novigrad30", 'novigrad30');
		IncludeHead("Novigrad31", 'novigrad31');
		IncludeHead("Novigrad32", 'novigrad32');
		IncludeHead("Novigrad33", 'novigrad33');
		IncludeHead("Novigrad34", 'novigrad34');
		IncludeHead("Novigrad35", 'novigrad35');
		IncludeHead("Novigrad36", 'novigrad36');
		IncludeHead("Novigrad37", 'novigrad37');
		IncludeHead("Novigrad38", 'novigrad38');
		IncludeHead("Novigrad39", 'novigrad39');
		IncludeHead("Novigrad40", 'novigrad40');
		IncludeHead("Novigrad41", 'novigrad41');
		IncludeHead("Novigrad42", 'novigrad42');
		IncludeHead("Novigrad43", 'novigrad43');
		IncludeHead("Elf1", 'elf1');
		IncludeHead("Elf2", 'elf2');
		IncludeHead("Elf3", 'elf3');
		IncludeHead("Elf4", 'elf4');
		IncludeHead("Elf5", 'elf5');
		IncludeHead("Elf6", 'elf6');
		IncludeHead("Elf7", 'elf7');
		IncludeHead("Elf8", 'elf8');
		IncludeHead("Elf9", 'elf9');
		IncludeHead("Elf10", 'elf10');
		IncludeHead("dlc1", 'dlc1');
		IncludeHead("dlc2", 'dlc2');
		IncludeHead("dlc3", 'dlc3');
		IncludeHead("dlc4", 'dlc4');
		IncludeHead("dlc5", 'dlc5');
		IncludeHead("dlc6", 'dlc6');
		IncludeHead("dlc7", 'dlc7');
		IncludeHead("dlc8", 'dlc8');
				
		count = list.Size();
	}

	public function IncludeHead(headName : string, head : name)
	{
		names.PushBack(headName);
		list.PushBack(head);
	}

	public function GetName(index: int) : string
	{
		return names[index];
	}

	public function IsValid(index : int) : bool
	{
		return index >= 0 && index < count;
	}
}