class modMenuTest
{
	public var initialized : bool; default initialized = false;
	public var menuPage : NotifyMenu;

	public function Init()
	{
		if ( !initialized )
		{
			theInput.RegisterListener( this, 'DopplerGeraltOnUp', 'DopplerGeraltOnUp' );
			theInput.RegisterListener( this, 'DopplerGeraltOnDown', 'DopplerGeraltOnDown' );
			theInput.RegisterListener( this, 'DopplerGeraltOnLeft', 'DopplerGeraltOnLeft' );
			theInput.RegisterListener( this, 'DopplerGeraltOnRight', 'DopplerGeraltOnRight' );
			theInput.RegisterListener( this, 'OnDopplerModDelete', 'DopplerModOnDelete' );

			menuPage = new notifyMenuSubMenusTest1 in this;
			menuPage.SetRoot();
			menuPage.Init();
		}
	}

	public final function OnDopplerModDelete( action : SInputAction )
	{
		if ( IsPressed( action ) )
		{
			menuPage.OnUp();
		}
	}

	public final function DopplerGeraltOnUp( action : SInputAction )
	{
		if ( IsPressed( action ) )
		{
			menuPage.OnUp();
		}
	}

	public final function DopplerGeraltOnDown( action : SInputAction )
	{
		if ( IsPressed( action ) )
		{
			menuPage.OnDown();
		}
	}

	public final function DopplerGeraltOnLeft( action : SInputAction )
	{
		if ( IsPressed( action ) )
		{
			menuPage.OnLeft();
		}
	}

	public final function DopplerGeraltOnRight( action : SInputAction )
	{
		if ( IsPressed( action ) )
		{
			menuPage.OnRight();
		}
	}
}



class notifyMenuSubMenusTest1 extends NotifyMenuSubMenus
{
	public function Init()
	{
		SetTitle("Root SubPageTest 1");

		AddSubMenu(new notifyMenuHelpTest1 in this);
		AddSubMenu(new notifyMenuOptionsTest1 in this);
		AddSubMenu(new notifyMenuSubMenusTest2 in this);
	}
}

class notifyMenuOptionsTest1 extends NotifyMenuOptions
{
	public function Init()
	{
		SetTitle("Options");

		AddOption("option 1");
		AddOption("options 2");
	}
}

class notifyMenuHelpTest1 extends NotifyMenu
{
	public function Init()
	{
		SetTitle("HelpTest 1");

		AddContent("conteudo de ajuda<br />");
		AddContent("more content");
	}
}

class notifyMenuSubMenusTest2 extends NotifyMenuSubMenus
{
	public function Init()
	{
		SetTitle("SubPagesTest 2");

		AddSubMenu(new notifyMenuHelpTest2 in this);
		AddSubMenu(new notifyMenuSubMenusTest3 in this);
	}
}

class notifyMenuHelpTest2 extends NotifyMenu
{
	public function Init()
	{
		SetTitle("HelpTest 2");

		AddContent("conteudo de ajuda subPage 1 bla bla");
	}
}

class notifyMenuSubMenusTest3 extends NotifyMenuSubMenus
{
	public function Init()
	{
		SetTitle("SubPagesTest 3");
		AddSubMenu(new notifyMenuHelpTest3 in this);
		AddSubMenu(new notifyMenuHelpTest4 in this);
	}
}

class notifyMenuHelpTest3 extends NotifyMenu
{
	public function Init()
	{
		SetTitle("HelpTest 3");
		AddContent("conteudo de ajuda bla bla");
	}
}


class notifyMenuHelpTest4 extends NotifyMenu
{
	public function Init()
	{
		SetTitle("HelpTest 4");
		AddContent("conteudo de ajuda<br />");
		AddContent("huehuehue");
	}
}
