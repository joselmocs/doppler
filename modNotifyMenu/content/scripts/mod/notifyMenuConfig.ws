class NotifyMenuConfig
{
	const var globalFont : string; default globalFont = "<font size='16'>";
	const var highlightFont : string; default highlightFont = "<font color='#844211'>";
	const var alternativeFont : string; default alternativeFont = "<font size='15'>";
	const var legendSeparatorText : string; default legendSeparatorText = "_______________________";
	const var oppenedMenuDuration : float; default oppenedMenuDuration = 60000;
}
