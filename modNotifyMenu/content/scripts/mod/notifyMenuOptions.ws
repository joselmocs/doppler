class NotifyMenuOptions extends NotifyMenu
{
	public var options : array<string>;
	private var highlighted : int; default highlighted = 0;
	private var actionName : string; default actionName = "select";
	private var isSelectable : bool; default isSelectable = false;

	public function AddOption( title : string )
	{
		options.PushBack( title );
	}

	public function SetActionName( action : string )
	{
		actionName = action;
	}

	public function SetIsSelectable( selectable : bool )
	{
		isSelectable = selectable;
	}

	public function GetHighlighted() : int
	{
		return highlighted;
	}

	public function GetHighlightedName() : string
	{
		return options[ highlighted ];
	}

	public function GetOptionsSize() : int
	{
		return options.Size();
	}

	public function SetHighlightedIndex( index : int )
	{
		highlighted = index;
	}

	public function GetActionsLabel() : string
	{
		var actionsLabel : string;

		actionsLabel = "";

		if ( isSelectable ) {
			actionsLabel += GetConfig().highlightFont + "RIGHT</font> to " + actionName + " ";
			actionsLabel += GetConfig().highlightFont + GetHighlightedName() + "</font><br />";
		}
		actionsLabel += GetConfig().highlightFont + "UP/DOWN</font> to change between options<br />";
		actionsLabel += super.GetActionsLabel();

		return actionsLabel;
	}

	public function ClearOptions()
	{
		options.Clear();
	}

	public function PreRefreshContent()	{ }
	public function RefreshContent()
	{
		var i, optionsSize : int;

		Close();
		ClearContent();
		PreRefreshContent();

		optionsSize = options.Size();

		for ( i = 0; i < optionsSize; i += 1)
		{
			if ( GetHighlighted() == i ) {
				AddContent( GetConfig().highlightFont + "# " + options[ i ] + "</font>");
			}
			else {
				AddContent( options[ i ] );
			}

			if ( i < optionsSize - 1 ) {
				AddContent( "<br />" );
			}
		}

		Open();
	}

	public function OnUp()
	{
		if ( !isActive ) {
			return;
		}

		if ( highlighted - 1 >= 0 ) {
			highlighted = highlighted - 1;
		}
		else {
			highlighted = options.Size() - 1;
		}

		RefreshContent();
	}

	public function OnDown()
	{
		if ( !isActive ) {
			return;
		}

		if ( highlighted + 1 < options.Size() ) {
			highlighted = highlighted + 1;
		}
		else {
			highlighted = 0;
		}

		RefreshContent();
	}
}
