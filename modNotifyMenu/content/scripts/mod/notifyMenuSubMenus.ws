class NotifyMenuSubMenus extends NotifyMenuOptions
{
	public var menus : array<NotifyMenu>;
	public var openedMenuIndex : int; default openedMenuIndex = -1;

	public function Init()
	{
		SetIsSelectable( true );
	}

	public function AddSubMenu( menu : NotifyMenu )
	{
		menu.Init();
		menu.SetReference( this );
		menus.PushBack( menu );

		AddOption( menu.GetTitle() );
	}

	public function PreRefreshContent()
	{
		super.PreRefreshContent();
	}

	public function ClearOptions()
	{
		menus.Clear();
		super.ClearOptions();
	}

	public function CanClose() : bool
	{
		return !HasOppenedPage();
	}

	public function Close()
	{
		openedMenuIndex = -1;
		super.Close();
	}

	public function GetHighlightedName() : string
	{
		return menus[ GetHighlighted() ].GetTitle();
	}

	public function HasOppenedPage() : bool
	{
		return openedMenuIndex > -1;
	}

	public function GetOpenedPage() : NotifyMenu
	{
		return menus[ openedMenuIndex ];
	}

	public function OnUp()
	{
		if ( HasOppenedPage() ) {
			GetOpenedPage().OnUp();
		}
		else {
			super.OnUp();
		}
	}

	public function OnDown()
	{
		if ( HasOppenedPage() ) {
			GetOpenedPage().OnDown();
		}
		else {
			super.OnDown();
		}
	}

	public function OnLeft()
	{
		if ( HasOppenedPage() )
		{
			if ( !GetOpenedPage().CanClose() )
			{
				GetOpenedPage().OnLeft();
				return;
			}
			else {
				GetOpenedPage().Close();
				RefreshContent();
			}
		}
		else {
			return super.OnLeft();
		}
	}

	public function OnRight()
	{
		if ( isRoot && !isActive && !HasOppenedPage() ) {
			super.OnRight();
			return;
		}

		if ( HasOppenedPage() ) {
			GetOpenedPage().OnRight();
		}
		else {
			Close();
			openedMenuIndex = GetHighlighted();
			GetOpenedPage().RefreshContent();
		}
	}
}
