class NotifyMenu
{
	public var config : NotifyMenuConfig;
	public var title : string;
	public var subTitle : string;
	public var parentMenu : NotifyMenu;
	public var content : string; default content = "";
	public var isActive : bool; default isActive = false;
	public var isRoot : bool; default isRoot = false;

	public function Init() {}
	public function SetRoot()
	{
		isRoot = true;
		config = new NotifyMenuConfig in this;
	}

	public function SetReference( page : NotifyMenu )
	{
		parentMenu = page;
	}

	public function GetRoot() : NotifyMenu
	{
		if ( isRoot ) {
			return this;
		}

		if ( parentMenu.isRoot ) {
			return parentMenu;
		}

		return parentMenu.GetRoot();
	}

	public function GetConfig() : NotifyMenuConfig
	{
		return GetRoot().config;
		if ( !isRoot ) {
			return parentMenu.GetConfig();
		}

		return config;
	}

	public function SetTitle( _title : string )
	{
		title = _title;
	}

	public function GetTitle() : string
	{
		return title;
	}

	public function GetDescription() : string
	{
		return "";
	}

	public function SetSubTitle( _subTitle : string )
	{
		subTitle = _subTitle;
	}

	public function GetSubTitle() : string
	{
		return subTitle;
	}

	public function GetFullTitle() : string
	{
		if ( parentMenu ) {
			return parentMenu.GetFullTitle() + " : " + GetConfig().highlightFont + GetTitle() + "</font>";
		}

		if ( isRoot ) {
			return GetTitle();
		}

		return GetConfig().highlightFont + GetTitle() + "</font>";
	}

	public function RefreshContent()
	{
		Close();
		Open();
	}

	public function ClearContent()
	{
		content = "";
	}

	public function AddContent( text : string )
	{
		content += text;
	}

	public function HasOppenedPage() : bool
	{
		return false;
	}

	public function GetActionsLabel() : string
	{
		var actionsLabel, leftAction : string;

		if ( isRoot ) {
			leftAction = "close";
		}
		else {
			leftAction = "back";
		}

		actionsLabel += GetConfig().highlightFont + "LEFT</font> to " + leftAction + "<br />";

		return actionsLabel;
	}

	public function Open()
	{
		var notificationText, leftAction : string;

		notificationText  = GetConfig().globalFont;
		notificationText += GetFullTitle() + "<br /><br />";
		notificationText += content;
		notificationText += "<br />" + GetConfig().legendSeparatorText + "<br />";
		notificationText += GetActionsLabel();
		notificationText += "</font>";

		theGame.GetGuiManager().ShowNotification( notificationText, GetConfig().oppenedMenuDuration );
		isActive = true;
	}

	public function CanClose() : bool
	{
		return true;
	}

	public function Close()
	{
		theGame.GetGuiManager().ClearNotificationsQueue();
		isActive = false;
	}

	public function CloseAll()
	{
		if ( parentMenu ) {
			parentMenu.CloseAll();
		}
		Close();
	}

	public function OnLeft()
	{
		Close();
	}

	public function OnRight() {
		if ( isRoot ) {
			RefreshContent();
		}
	}

	public function OnUp() { }
	public function OnDown() { }
}
